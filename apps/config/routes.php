<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'welcome';
$route['terminada'] = 'Terminada';
$route['uvuvwevwevweonyetenyevweugwemubwemosas'] = 'Update';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['apiregister'] = 'Oauth_reg';
$route['apilogin'] = 'Oauth_login';
$route['logout'] = 'Logof';
$route['call'] = 'Outbound';
$route['calls'] = 'Outbound_1';
$route['chanspy'] = 'Outbound/chanspy';
$route['command'] = 'End_call';
$route['concise'] = 'End_call/command';

//$route['daily-data'] = 'Outbound/get_daily_data';
//$route['daily-data-transfer'] = 'Outbound/get_daily_data_transfer';
$route['daily-data'] = 'End_call/view';
$route['daily-data-transfer'] = 'End_call/view_transfer';

$route['daily-data-by-date'] = 'Outbound/get_daily_data_bydate';
$route['daily-aux'] = 'Outbound/get_daily_aux';
$route['report-inbound'] = 'Outbound/read_json';
//$route['Outbound/get_dataviewinterval'] = 'Outbound/read_json';
$route['summary-daily'] = 'Outbound/sumarydailyreport';
$route['daily-report'] = 'Outbound/daily_report';
$route['daily-act'] = 'Outbound/get_all_daily_activity';

$route['asterisk'] = 'Phone';
$route['logoffall'] = 'Phone/logoffallbycron';
$route['end_call'] = 'welcome/end_call';
$route['off'] = 'end_call';
$route['extract'] = 'Outbound/get_data_e';
$route['extract/doc'] = 'Outbound/doc';
$route['register'] = 'phone/Phone/register';

$route['login'] = 'phone/Phone/login';
$route['log-out'] = 'Phone/logout';

$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
