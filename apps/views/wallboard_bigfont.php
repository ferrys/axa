<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>AXA WALLBOARD</title>
    <link rel="icon" href="https://axasales.valdo-intl.com/API/asset/images/favicon.jpg" type="image/jpg" />
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" />
	<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="https://axasales.valdo-intl.com/API/asset/wallboard/css/style.css" rel="stylesheet" />
	<link href="https://axasales.valdo-intl.com/API/asset/wallboard/css/style-responsive.css" rel="stylesheet" />
	<link href="https://axasales.valdo-intl.com/API/asset/wallboard/css/themes/default.css" rel="stylesheet" id="style_color" />
	<!--<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />-->
	<!--<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />-->
	<!--<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css" />-->
	<!--<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />-->
	<!--<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css"  />-->
	<!--<link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />-->
    <!-- <link href="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css" />-->
    
	
    <script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>	
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->	
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>		
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<![endif]-->	
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/breakpoints/breakpoints.js" type="text/javascript"></script>	
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery.blockui.js" type="text/javascript"></script>	
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery.cookie.js" type="text/javascript"></script>
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>	
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery.peity.min.js" type="text/javascript"></script>	
	<script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>	
    <script src="https://axasales.valdo-intl.com/API/asset/wallboard/js/jq/jquery.jclock.js" type="text/javascript"></script>
    <script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
    <script src="https://axasales.valdo-intl.com/API/asset/wallboard/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	
    <script src="https://axasales.valdo-intl.com/API/asset/wallboard/scripts/app.js" type="text/javascript"></script>
	
	<script>
		jQuery(document).ready(function() {		
			App.init(); // initlayout and core plugins
		});
	</script>
    <script type="text/javascript" src="https://axasales.valdo-intl.com/API/asset/wallboard/js/jq/jquery.autocomplete.js"></script>
    <script type="text/javascript" src="https://axasales.valdo-intl.com/API/asset/wallboard/js/cc/wallboard.js"></script>
    
    <style>
        h1 {text-shadow: 1px 1px 1px Gray; font-size:3.5em; margin-top:30px; }
        strong { text-shadow: 1px 1px 1px Gray; font-size:1.8em; }
    </style>
	<?php echo $html['css'] ?> 
</head>
<body class="fixed-top">
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="brand" href="#"><i>AXA AFI</i></a>
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="arrow"></span>
				</a>          			
			</div>
		</div>
	</div>
	<div id="container" class="row-fluid">
		<div id="sidebar" class="nav-collapse collapse">
			<div class="sidebar-toggler hidden-phone"></div>     	
		</div>
		<div id="body">
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">		
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="https://axasales.valdo-intl.com/API/asterisk">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="wallboard">Wallboard</a></li>
							<li class="pull-right dashboard-report-li">
								<div class="dashboard-report-range-container no-text-shadow" data-placement="top"><i class="icon-time icon-large"></i><span id="jclock"></span></div>
							</li>
						</ul>
					</div>
				</div>
				<div id="page" class="dashboard">
                	<div class="span12">
                    	<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab_1_2"><i class="icon-bar-chart"></i> Wallboard</a></li>
                            </ul>
                            <div class="tab-content">
								<div class="row-fluid">
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-axa fade in">
											 <center><img src="<?=base_url().'asset/wallboard/phone_status/call.png';?>" width="100" height="200"/><h5><strong><font size="4">TOTAL CALL</font></font></strong></h5><h1 id="total">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-queue fade in">
											 <center><img src="<?=base_url().'asset/wallboard/phone_status/queue.png';?>" width="100" height="200"/><h5><strong><font size="4">CALL QUEUE</font></strong></h5><h1 id="queueing">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-talk fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/talking_google.png';?>" width="100" height="200"/><h5><strong><font size="4">ANSWERED CALL</font></strong></h5><h1 id="answered">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-abandone fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/abandone.png';?>" width="100" height="200"/><h5><strong><font size="4">ABANDONED CALL</font></strong></h5><h1 id="abandon">...</h1></center>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-idle fade in">
											 <center><img src="<?=base_url().'asset/wallboard/phone_status/idle.png';?>" width="100" height="200"/><h5><strong><font size="4">AGENT IDLE</font></strong></h5><h1 id="idle2">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-break1 fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/break.png';?>" width="100" height="200"/><h5><strong><font size="4">AGENT NOT ACTIVE</font></strong></h5><h1 id="not_active">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-inbound fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/received.png';?>" width="100" height="200"/><h5><strong><font size="4">AGENT INBOUND</font></strong></h5><h1 id="agent_inbound">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-abd fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/abd_queue.png';?>" width="100" height="200"/><h5><strong><font size="4">ABANDONED IN QUEUE</font></strong></h5><h1 id="abd_queue">...</h1></center>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-received fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/received2.png';?>" width="100" height="200"/><h5><strong><font size="4">AGENT TALKING</font></strong></h5><h1 id="talking">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-acd1 fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/acd1.png';?>" width="100" height="200"/><h5><strong><font size="4">ANSWERED >20 SECS</font></strong></h5><h1 id="acd1">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-acd2 fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/acd1.png';?>" width="100" height="200"/><h5><strong><font size="4">ANSWERED <20 SECS</font></strong></h5><h1 id="acd2">...</h1></center>
										</div>
									</div>
									<div class="responsive" data-tablet="span3" data-desktop="span3" >
										<div class="circle-stat alert alert-block alert-outbound fade in">
											<center><img src="<?=base_url().'asset/wallboard/phone_status/outbond.png';?>" width="100" height="200"/><h5><strong><font size="4">AGENT OUTBOUND</font></strong></h5><h1 id="agent_outbound">...</h1></center>
										</div>
									</div>
								</div>
                            </div>
                    	</div>
                    </div>
                </div>
			</div>	
		</div>
	</div>
	<div class="responsive" >
		<?php echo $html['js'] ?>
	</div>
</body>
</html>
