<!DOCTYPE html>   
<html lang="en">   
    <head>   
        <meta charset="utf-8">   
        <title>AXA Monitoring</title>   
        <meta name="description" content="Bootstrap.">  
        <?php  $html['css']?>
         <?php  $html['js']?>
        <script> setInterval(function ()
        {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>monitoring",
                datatype: "html",
                success: function (data)
                {
                 
        //table.ajax.reload(null, false);             
            $("#example2").load(" #example2");
                }
            });
        }, 5000);</script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    </head>  
    <body  style="margin:20px auto">  
        <div class="container">
            <div class="row header" style="text-align:center;color:green">
                <h3>AXA Agent Monitoring</h3>
            </div>
            <?php
            $is_logged_in = $this->session->userdata('is_logged_in');
            if (!isset($is_logged_in) || $is_logged_in != true) {
                ?>
                <section>
                    <?php
                    if (validation_errors()) {
                        validation_errors('<p class="alert alert-danger" align="center">', '</p>');
                    }
                    ?>
                    <div class="row header" style="text-align:center;color:orangered">
                        <h3>Login Required</h3></div>
                </section>
                <?php
            } else {
                if ($this->session->userdata('sip_no') == 703 OR $this->session->userdata('sip_no') == 801) {
                    ?>
            <div id="example">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>SIP</th>
                                <th>Login Sebagai</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody >
                            <?php
                            for ($i =0; $i < count($sip); $i++) {
                                
                                 if ($sip[$i]['active'] == 0) {
                        $status = '<span class="label label-danger">OFFLINE</span>';
                        $du ='';
                        $h ='';
                        $d = 'disabled';
                        $whis='---------'; 
                        $spy ='---------';
                        $hold ='';
                    }
                  else if ($sip[$i]['active'] == 1) {
                        $status = '<span class="label label-primary">IDLE</span>';
                        $d = 'disabled';
                       $datetime1 = strtotime($sip[$i]['time']);
                        $datetime2 = strtotime(date("Y-m-d H:i:s"));
                        $interval  = $datetime2 - $datetime1;
                        $du = '<span class="label label-default">'.gmdate('H:i:s', $interval).'</span>';
                        $h ='';
                        $whis='---------'; 
                        $spy ='---------';
                        $hold ='';
                    } 
                    

                    else if ($sip[$i]['active'] == 2) {
                        $status = '<span class="label label-warning">AUX</span>';
                        $d = 'disabled';
                        $h ='';
                        $datetime1 = strtotime($sip[$i]['time']);
                        $datetime2 = strtotime(date("Y-m-d H:i:s"));
                        $interval  = $datetime2 - $datetime1;
                        $du = '<span class="label label-warning">'.gmdate('H:i:s', $interval).'</span>';
                        $whis='---------'; 
                        $spy ='---------';
                        $hold ='';
                    }
                     else if ($sip[$i]['active'] == 3) {
                        
                        if($sip[$i]['onhold'] == 1){
                            $s=  base_url().'monitoring/agentspy?ext=';
                            $w= base_url().'monitoring/agentwhisper?ext=';
                            $h ='<span class="label label-warning">HOLD</span>';
                            $spy = 'spy('."'".$s.$sip[$i]['sip_no']."'".')';
                            $whis = 'spy('."'".$w.$sip[$i]['sip_no']."'".')';
                            $datetime1 = strtotime($sip[$i]['time']);
                            $datetime2 = strtotime(date("Y-m-d H:i:s"));
                            $interval  = $datetime2 - $datetime1;
                             if($interval>1200){$class='class="label label-warning"';}else{$class='class="label label-success"';}
                             $du = '<span '.$class.'>'.gmdate('H:i:s', $interval).'</span>';
                             $hold = '<span class="label label-warning">HOLD</span>';
                            $d = '';
                            $status = '<span class="label label-success">ONLINE</span>';
                        }
                        else{$s=  base_url().'monitoring/agentspy?ext=';
                            $w= base_url().'monitoring/agentwhisper?ext=';
                            $h ='';
                            $spy = 'spy('."'".$s.$sip[$i]['sip_no']."'".')';
                            $whis = 'spy('."'".$w.$sip[$i]['sip_no']."'".')';
                            $datetime1 = strtotime($sip[$i]['time']);
                            $datetime2 = strtotime(date("Y-m-d H:i:s"));
                            $interval  = $datetime2 - $datetime1;
                            if($interval>1200)
                            {$class='class="label label-warning"';}else{$class='class="label label-success"';}
                             $du = '<span '.$class.'>'.gmdate('H:i:s', $interval).'</span>';
                             $hold ='';
                            $d = '';
                            $status = '<span class="label label-success">ONLINE</span>';}
                        
                    }
                                
                                
                            
                                 
                                ?>
                                <tr >
                                    <td><?php echo $sip[$i]['sip_no'] ?></td> 
                                    <td><?php echo $sip[$i]['role'] ?></td>
                                    <td  style="width: 170px;"> 
                                        <?php echo $status ?>
                                        <!--<span hidden="" class="label label-info">Idle</span><span hidden class="label label-danger">Offline</span><span hidden class="label label-warning">Aux</span>-->
                                    </td>
                                    <td  style="width: 170px;">
                                        <a <?php echo $d ?> type="button" class="btn btn-success" onclick=".$spy.">Spying</a>
                                        <a <?php echo $d ?> type="button" class="btn btn-info" onclick=".$whis.">Whisper</a>
                                    </td>
                                </tr> <?php } ?>
                        </tbody>
                         </table></div>
                    <?php
                } else {
                    echo ' <div class="row header" style="text-align:center;color:orangered">
                        <h3>privilege user</h3></div>';
                }
            }
            ?>             
        </div>
    </body>  
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
        
        function spy(url)
        {
            var request = new XMLHttpRequest();
            request.open("GET", url, true);
            request.send(null);
        }
    </script>
</html>  