<!DOCTYPE html>   
<html lang="en">   
    <head>   
        <meta charset="utf-8">   
        <title>AXA Monitoring</title>   
        <meta name="description" content="Bootstrap.">  
  <?php echo $html['css']?>
       <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
       
       
  </head>  
    <body  style="margin:20px auto">
       
        <?php
            $is_logged_in = $this->session->userdata('is_logged_in');
            if (!isset($is_logged_in) || $is_logged_in != true) {
                ?>
         <div class="row header" style="text-align:center;color:orangered">
                        <h3>Login Required</h3></div>
        <?php
            } else {
                if ($this->session->userdata('sip_no') == 703 OR $this->session->userdata('sip_no') == 702 OR $this->session->userdata('sip_no') == 801 OR $this->session->userdata('sip_no') == 802 OR $this->session->userdata('sip_no') == 803 OR $this->session->userdata('sip_no') == 804 ) {
                    ?>
        <div class="container"> 
            
            <a style=" text-decoration: none;" href="<?php echo base_url() ?>phone/logout_as"><b>Logout</b> |</a>
            <a target="_blank" style=" text-decoration: none;" href="<?php echo base_url() ?>asterisk"><b>Home</b> |</a>
            <a target="_blank" style=" text-decoration: none;" href="https://axasales.valdo-intl.com/API/wallboard"><b>Wallboard</b></a>
            <div class="row header" style="text-align:center;color:green">
                <h3>AXA AGENT MONITORING</h3>
                
            </div>
           
            <div class="box-body">   
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <?php if ($this->session->userdata('sip_no') == 703 || $this->session->userdata('sip_no') == 704 ){?>
            <h3 id="load" >
                <p ><b>STORAGE : </b><b style="color: #000099;">TOTAL : <?php echo $totalSpace ?> |</b>
                    <b style="color: #339933;">FREE : <?php echo $freeSpace ?> |</b>
                    <b style="color: #cc0000;">USED : <?php echo $used ?> GB</b>
                </p>
             
                <p style="color: #663300;">RAM USAGE: <?php echo $this->benchmark->memory_usage();?></p>
                <p style="color: #ff8000;">Script Load : {elapsed_time}/Second </p></h3>
                    <?php } ?>
       
                <tr>
                    <th>No</th>
                    <th>SIP</th>
                    <th>Username</th>
                    <th>Login Role</th>
                    <th>Status</th>
                    <th>Duration</th>
                    <th style="width:175px;">Action</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
           
        </table>
      

          <?php if ($this->session->userdata('sip_no') == 703 OR $this->session->userdata('sip_no') == 704  ){?><div class="w3-container">

</div>

<?php } ?>
    </div>
            
</div>
    <?php
            } 
            
 else {echo ' <div class="row header" style="text-align:center;color:orangered">
                        <h3><img src='. base_url().'asset/monitoring/under-maintenance.png></a></h3></div>';}
                }
                    ?>

<script src="<?php echo base_url('asset/jquery/jquery-2.1.4.min.js')?>"></script>
<script src="<?php echo base_url('asset/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('asset/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('asset/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('asset/bootstrap/js/bootstrap-datepicker.min.js')?>"></script>

<script type="text/javascript">
    var save_method;
    var table;
    $(document).ready(function () {
        table = $('#table').DataTable({
            "processing": false,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('monitoring/ajax_list') ?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": [-1],
                    "orderable": true,
                },
            ],
        });
        $('.datepicker').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            todayHighlight: true,
            orientation: "top auto",
            todayBtn: true,
            todayHighlight: true,
        });

        $("input").change(function () {
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        
        $("textarea").change(function () {
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        
        $("select").change(function () {
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });
        

                
function logout()
        {
            $('#Logout').text('logout...');
            $('#Logout').attr('disabled', true);
            $.ajax({
                url: "<?php echo site_url('phone/logout') ?>",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#div').load(" #div");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#Logout').text('Logout');
                    $('#Logout').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $('#div').load(" #div");
//                    alert('No Internet Connection');
//                    $('#btnLogin').text('Login');
//                    $('#btnLogin').attr('disabled', false);
                }
            });
        }
         function spy(url)
        {
            var request = new XMLHttpRequest();
            request.open("GET", url, true);
            request.send(null);
        }
 
            setInterval(function ()
        {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>monitoring",
                datatype: "html",
                success: function (data)
                {
                   
//                     $("#loading").load(" #loading");
//                     $("#loading1").load(" #loading1");
//                     $("#loading2").load(" #loading2");
//                     $("#loading3").load(" #loading3");
//                      $("#loading4").load(" #loading4");
                    table.ajax.reload(null, false);             
                }
            });
        }, 2123);
        
        setInterval(function ()
        {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>monitoring",
                datatype: "html",
                success: function (data)
                {
                    $("#load").load(" #load");    
                }
            });
        }, 2002000);

</script>
           
       
    </body>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111137321-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111137321-1');
</script>

    
</html>  