<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Asterisk</title>
        <?php echo $html['css'] ?>        
    </head>
    <body>
        <div class="container container-table">
            <div class="row vertical-center-row">
                <div class="text-center col-md-4 col-md-offset-4"> 
                    <section>
                        <form action="#" method="POST">
                            <input placeholder="name" class="form-control"><br>
                            <input placeholder="sip" class="form-control"><br>
                            <input placeholder="password" class="form-control"><br>
                            <input placeholder="retype password" class="form-control"><br>
                            <button type="button" class="btn btn-primary">REGISTER</button>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>
    <?php echo $html['js'] ?>
</html>