<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Asterisk</title>
        <?php echo $html['css'] ?>  
        <style>
            #parent{display: flex;}
            #narrow {width: 100px;}#wide {flex:1;}
            .status-available{color:#2FC332;}
            .status-not-available{color:#D60202;}
        </style>
    </head>
    <?php echo $this->session->userdata('password') ?>
<!--       <script>
         setInterval(function ()
         {
             $.ajax({
                 type: "patch",
                 url: "https://axasales.valdo-intl.com/restserver/terminada",
                 datatype: "json",
                 success: function (data)
                 {
                     $("#activity").load(" #activity");
                 }
             });
         }, 5000);
         $.ajax({
                 type: "post",
                 url: "https://axasales.valdo-intl.com/restserver",
                 datatype: "json",
                 success: function (data)
                 {
                     $("#activity").load(" #activity");
                 }
             });
         }, 5000);
     </script>-->
    <body>
        <div id="div" class="container container-table">
            <div class="row vertical-center-row">
                <div  class="text-center col-md-4 col-md-offset-4"> <h4>VALDO ASTERISK</h4></div></div>
       <div class="row vertical-center-row">
                <div  class="text-center col-md-4 col-md-offset-4"> 
            <?php if ($this->session->userdata('sip_no') == 703 OR $this->session->userdata('sip_no') == 702 OR $this->session->userdata('sip_no') == 801 OR $this->session->userdata('sip_no') == 804 OR $this->session->userdata('sip_no') == 802 OR $this->session->userdata('sip_no') == 803) { ?>
             <!--<a id="Logout" type="button" onclick="myFunction()" style=" text-decoration: none;" href="#"><b>Logout</b> | </a>-->
             <a target="_blank"  style=" text-decoration: none;" href="<?php echo base_url() ?>monitoring"><b>Monitoring</b> |</a>
             <a target="_blank" style=" text-decoration: none;" href="https://axasales.valdo-intl.com/API/wallboard"><b>Wallboard</b>|</a>
             <a id="LogoutA" onclick="AsteriskLogout()" style="cursor: pointer;text-decoration: none;"><b>Logout All</b></a><?php } ?>
             </div></div>
             <script>
            setInterval(function ()
            {
                $.ajax({
                    type: "post",
                    url: "<?php echo base_url() ?>asterisk",
                    datatype: "html",
                    success: function (data)
                    {
                        $("#refresh").load(" #refresh");
                        
                    }
                });
            }, 5000);    
        </script>
            <div class="row vertical-center-row">
                <div  class="text-center col-md-4 col-md-offset-4"> 
                    <?php $is_logged_in = $this->session->userdata('is_logged_in');
                    if (!isset($is_logged_in) || $is_logged_in != true) { ?>
                        <section>
                        <?php if (validation_errors()) {
                             validation_errors('<p class="alert alert-danger" align="center">', '</p>');
                        } ?>
                            
                            <form id="form-login" action="#" method="post">
                                <span id="sip-availability-status"></span>      
                                <input placeholder="SIP"  name="sip_no"  id="sip_no" class="form-control"><br>
                                <input placeholder="Password" type="password"  name="password" id="password"  class="form-control"><br>
                                <select name="role" required="" class="form-control">
                                    <option value="">--Pilih--</option>
                                    <option value="2">Outbound</option>
                                    <option value="1">Inbound</option>
                                   
                                </select><br>
                                <button type="submit" id="btnLogin" onclick="login()" class="btn btn-primary btn-block btn-flat">Login</button>
                            </form>
                        </section>
                        <?php
                    } else {
                        
                        ?>
                  
                    
                        <div id="istirahat">
                           
                            <form id="form" method="POST">
                                <br><br><div  class="dropdown"> <?php if ($aux[0]['active'] != 2) { ?>
                                    <select name="aux"  onchange="save()" class="form-control-static">
                                            <option value="">--Pilih AUX--</option>
                                            <option value="1">Makan Siang</option>
                                            <option value="2">Sholat</option>
                                            <option value="3">Toilet</option>
                                            <option value="4">Briefing</option>
                                        </select> <br><br>
                                     <input disabled placeholder="SIP" value="<?php echo $this->session->userdata('sip_no'); ?>" class="form-control">
                            <?php 
                             $this->load->model(array('mphone', 'm_cstmr'));
                                $role = $this->m_cstmr->get_data_agent($this->session->userdata('id_agent'));
                                
                            ?>
                            <input disabled  value="<?php echo $role[0]['role'] ?>" class="form-control">
                                     <br><input id="Logout" type="button" onclick="myFunction()" class="btn" style="background-color: goldenrod;" value="LOGOUT"><br><br>
                                    <?php } else { ?>
                                    <button type="button" id="btnUnpause" onclick="unpause()" class="btn btn-warning btn-block btn-flat">UNPAUSE</button>
                                    <?php } ?>
                                </div>
                            </form><br>
                        </div> 
                        <?php
                    }
                    ?>                                   

                </div></div></div>
        <br>
        <br>
        <br>
        <br>
        <br>
    </body>
    <script>
        var save_method;
        function save()
        {
            $('#auxstart').text('Loading...');
            $('#auxstart').attr('disabled', true);

            $.ajax({
                url: "<?php echo site_url('phone/ajax_update_aux') ?>",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#istirahat').load(" #istirahat");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#auxstart').text('Loading');
                    $('#auxstart').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
//                    alert('No Internet Connection');
//                    $('#btnLogin').text('Login');
//                    $('#btnLogin').attr('disabled', false);
 $('#istirahat').load(" #istirahat");
                }
            });
        }

        function unpause()
        {
            $('#btnUnpause').text('Wait...');
            $('#btnUnpause').attr('disabled', true);
            $.ajax({
                url: "<?php echo site_url('phone/ajax_update_unpause') ?>",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#istirahat').load(" #istirahat");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#btnUnpause').text('Wait...');
                    $('#btnUnpause').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $('#istirahat').load(" #istirahat");
//                    alert('No Internet Connection');
//                    $('#btnLogin').text('Login');
//                    $('#btnLogin').attr('disabled', false);
                }
            });
        }
        //https://axasales.valdo-intl.com/API/logoffall?key_token=208ce7bcc76f0619c86c4e33c3b8c75989
        
        function AsteriskLogout() {
            var r = confirm("Logout Semua Agent!");
            if (r === true) {
                logoutas();
            } else {
                
            }
        }
        function logoutas()
        {
            $('#LogoutA').text('logout...');
            $('#LogoutA').attr('disabled', true);
            $.ajax({
                url: "https://axasales.valdo-intl.com/API/logoffall?key_token=208ce7bcc76f0619c86c4e33c3b8c75989",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#div').load(" #div");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#LogoutA').text('Logout');
                    $('#LogoutA').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $('#div').load(" #div");
//                    alert('No Internet Connection');
//                    $('#btnLogin').text('Login');
//                    $('#btnLogin').attr('disabled', false);
                }
            });
        }
        
        function myFunction() {
            var r = confirm("Yakin Logout!");
            if (r == true) {
                logout();
            } else {
                
            }
        }


        function logout()
        {
            $('#Logout').text('logout...');
            $('#Logout').attr('disabled', true);
            $.ajax({
                url: "<?php echo site_url('phone/logout') ?>",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#div').load(" #div");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#Logout').text('Logout');
                    $('#Logout').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $('#div').load(" #div");
//                    alert('No Internet Connection');
//                    $('#btnLogin').text('Login');
//                    $('#btnLogin').attr('disabled', false);
                }
            });
        }
        function login()
        {
            $('#btnLogin').text('Login...');
            $('#btnLogin').attr('disabled', true);
            $.ajax({
                url: "<?php echo site_url('phone/ceklogin') ?>",
                type: "POST",
                data: $('#form-login').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#div').load(" #div");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#btnLogin').text('Login');
                    $('#btnLogin').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                     $('#div').load(" #div");
//                    alert('No Internet Connection');
//                    $('#btnLogin').text('Login');
//                    $('#btnLogin').attr('disabled', false);
                }
            });
        }

    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111137321-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111137321-1');
</script>

    <?php echo $html['js'] ?>
</html>