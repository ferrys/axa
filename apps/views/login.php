<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Asterisk</title>
        <?php echo $html['css'] ?>        
    </head>
    <body>
        <div class="container container-table">
            <div class="row vertical-center-row">
                <div class="text-center col-md-4 col-md-offset-4"> <br>
                    <section> <?php if (validation_errors()) {echo validation_errors('<p class="alert alert-danger" align="center">', '</p>');} ?>

                          <form action="<?php base_url()?>oauth" method="post">
                              <input placeholder="SIP" required="" name="sip_no" class="form-control"><br>
                              <!--<input placeholder="email" required="" name="email" class="form-control"><br>-->
                              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </body>
    <?php echo $html['js'] ?>
</html>