<!DOCTYPE html>   
<html lang="en">   
    <head>   
        <meta charset="utf-8">   
        <title>Report History Incoming Call</title>   
        <meta name="description" content="Bootstrap.">  
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <?php echo  $html['css']?> 
       
		
    </head>  
    <body  style="margin:20px auto">

		
		<!--<script src="<?php echo base_url('asset/wallboard/js/jquery-1.8.0.min.js')?>"></script>-->
		<script src="<?php echo base_url('asset/wallboard/js/fungsi.js')?>"></script>
		<!--<script src="<?php echo base_url('asset/wallboard/js/jquery.easyui.min.js')?>"></script>-->
		<!--<script src="<?php echo base_url('asset/wallboard/js/jquery.ui.datepicker.js')?>"></script>-->
		<div>
			<h3>Report <span class="label label-success">Incoming Call</span></h3>
		</div>
		<form action="<?php echo base_url() ?>report_historyIncoming/report_historyCall">
			<div id="parent">
				<div id="wide" class="input-group col-md-3">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>
					<input type="text" id="reservation" name="range" placeholder="Pilih Tanggal" required class="form-control pull-left">
				</div>
			
				<button type="submit" value="submit" title="Download"><i class="fa fa-print"></i></button>
			
			</div>
			  
		</form>
<!--                <form action="<?php echo base_url() ?>/report_tele" >
		<div class="container">
			<div class="control-group fm-req">
				<p class="infonya"></p>
				<label class="control-label">Periode </label>
				<div class="controls">
					<input id="tgl1" name="tgl1" type="text" class="form-control"></input> s/d 
                                        <input id="tgl2" name="tgl2" type="text" class="form-control"></input> 
				</div><br>
				<div class="controls">
                                    <button  type="submit" class="btn btn-success">Download</button></div>
			</div>
			
		</div>
                    </form>-->
 <?php echo  $html['js']?>

		
	<script>
  $.widget.bridge('uibutton', $.ui.button);
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD/MM/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
       {ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) 
        {
          $('#daterange-btn span').html(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
        }
    );
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();
    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
		
    </body>

    
</html>  