<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Asterisk</title>
        <?php echo $html['css'] ?>  
        <style>
            #parent{display: flex;}
            #narrow {width: 100px;}#wide {flex:1;}
            .status-available{color:#2FC332;}
            .status-not-available{color:#D60202;}
        </style>
       </head>
<!--       <script>
            setInterval(function ()
            {
                $.ajax({
                    type: "patch",
                    url: "https://axasales.valdo-intl.com/restserver/terminada",
                    datatype: "json",
                    success: function (data)
                    {
                        $("#activity").load(" #activity");
                    }
                });
            }, 5000);
            $.ajax({
                    type: "post",
                    url: "https://axasales.valdo-intl.com/restserver",
                    datatype: "json",
                    success: function (data)
                    {
                        $("#activity").load(" #activity");
                    }
                });
            }, 5000);
        </script>-->
    <body><br>
        <div class="container container-table">
            <div id="div" class="row vertical-center-row">
                <div class="text-center col-md-4 col-md-offset-4"> 
               <br /><br />
                    <br>
                    <?php $is_logged_in = $this->session->userdata('is_logged_in');if (!isset($is_logged_in) || $is_logged_in != true) {?>
                    <section> <?php if (validation_errors()) {echo validation_errors('<p class="alert alert-danger" align="center">', '</p>');} ?>

                          <form id="form-login" action="#" method="post">
                         <span id="sip-availability-status"></span>      
                         <input placeholder="SIP"  name="sip_no" required class="form-control"><br>
                              <!--<input placeholder="email" required="" name="email" class="form-control"><br>-->
                              <button type="submit" id="btnLogin" onclick="login()" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </form>
                    </section>
                    <?php
                    } 
                    else { ?>
                  <div id="istirahat">
                      <h2>SIP</h2><input disabled placeholder="SIP"  value="<?php echo $this->session->userdata('sip_no'); ?>" class="form-control">
                      
                          <form id="form" method="POST">
                    <br><br><div  class="dropdown"> <?php
                    
                    if ($aux[0]['status_agent'] == 0) { ?>
                            <select name="aux" onchange="save()" class="form-control-static">
                                <option value="">--Pilih AUX--</option>
                                <option value="1">Makan Siang</option>
                                <option value="2">Sholat</option>
                                <option value="3">DLL</option>
                            </select>  
                        <?php } else { ?>
                            <input type="button" onclick="unpause()" class="btn" style="background-color: yellow;" value="UNPAUSE">
                        <?php } ?>
                    </div></form>
                      <input type="button" onclick="logout()" class="btn" style="background-color: goldenrod;" value="LOGOUT">

                    </div>
                                 <?php
                    } 
?>
                </div></div></div>
        <br>
        <br>
        <br>
        <br>
        <br>
    </body>
    <script>
        var save_method;
        function save()
        {
            $('#btnSave').text('saving...');
            $('#btnSave').attr('disabled', true);

            $.ajax({
                url: "<?php echo site_url('phone/ajax_update_aux') ?>",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#istirahat').load(" #istirahat");
                    } else
         
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#btnSave').text('save');
                    $('#btnSave').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save');
                    $('#btnSave').attr('disabled', false);
                }
            });
        }
        
        function unpause()
        {
            $('#btnSave').text('saving...');
            $('#btnSave').attr('disabled', true);
            $.ajax({
                url: "<?php echo site_url('phone/ajax_update_unpause') ?>",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#istirahat').load(" #istirahat");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#btnSave').text('save');
                    $('#btnSave').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save');
                    $('#btnSave').attr('disabled', false);
                }
            });
        }
        
        
        function logout()
        {
            $('#btnSave').text('saving...');
            $('#btnSave').attr('disabled', true);
            $.ajax({
                url: "<?php echo site_url('phone/logout') ?>",
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#div').load(" #div");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#btnSave').text('save');
                    $('#btnSave').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnSave').text('save');
                    $('#btnSave').attr('disabled', false);
                }
            });
        }
         function login()
        {
            $('#btnLogin').text('Login...');
            $('#btnLogin').attr('disabled', true);
            $.ajax({
                url: "<?php echo site_url('phone/ceklogin') ?>",
                type: "POST",
                data: $('#form-login').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    if (data.status)
                    {
                        $('#div').load(" #div");
                    } else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                        }
                    }
                    $('#btnLogin').text('Login');
                    $('#btnLogin').attr('disabled', false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#btnLogin').text('Login');
                    $('#btnLogin').attr('disabled', false);
                }
            });
        }
      //  function checkSipAvailability() {
//                jQuery.ajax({
//                    url: "<?php echo site_url('phone/check_sip_exists') ?>",
//                    data: 'sip_no=' + $("#sip_no").val(),
//                    type: "POST",
//                    success: function (data) {
//                        $("#sip-availability-status").html(data);
//                    },
//                    error: function () {}
//                });
//            }
    </script>
    <?php echo $html['js'] ?>
</html>