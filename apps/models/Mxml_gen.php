<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mxml_gen extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('factory');
        $this->load->model('Mloadxml');
    }
    
    function getlogcti(){
        $xmlFile = $this->config->item('path_xml').'/xml/log_cti.xml';
        $xmlObj = simplexml_load_string($this->Mloadxml->load($xmlFile));
        
        $totalxml = count($xmlObj->datacontent);
        

        $data = array();
        for($i = 0; $i < $totalxml; $i++){
            $data[$i]['ID_VALDO'] = (String) $xmlObj->datacontent[$i]->ID_VALDO;
            $data[$i]['SIP'] = (String) $xmlObj->datacontent[$i]->SIP;
            $data[$i]['DESTINATION_NUMBER'] = (String) $xmlObj->datacontent[$i]->DESTINATION_NUMBER;
            $data[$i]['DARI'] = (String) $xmlObj->datacontent[$i]->DARI;
            $data[$i]['START_DIALING_DATE'] = (String) $xmlObj->datacontent[$i]->START_DIALING_DATE;
            $data[$i]['START_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->START_CALL_DATE;
            $data[$i]['END_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->END_CALL_DATE;
            $data[$i]['DURATION_DIAL'] = (String) $xmlObj->datacontent[$i]->DURATION_DIAL;
            $data[$i]['DURATION_CALL'] = (String) $xmlObj->datacontent[$i]->DURATION_CALL;
            $data[$i]['DURATION_WAIT'] = (String) $xmlObj->datacontent[$i]->DURATION_WAIT;
            $data[$i]['TRANSFER_FROM'] = (String) $xmlObj->datacontent[$i]->TRANSFER_FROM;
            $data[$i]['TRANSFER_TO'] = (String) $xmlObj->datacontent[$i]->TRANSFER_TO;
            $data[$i]['RECORDING_FILE'] = (String) $xmlObj->datacontent[$i]->RECORDING_FILE;
            $data[$i]['STATUS'] = (String) $xmlObj->datacontent[$i]->STATUS;
        }
        
        return $data;

    }
    
     function getmonitoring(){
        $xmlFile = $this->config->item('path_xml').'/xml/monitoring.xml';
        $xmlObj = simplexml_load_string($this->Mloadxml->load($xmlFile));
        
        $totalxml = count($xmlObj->datacontent);
        

        $data = array();
        for($i = 0; $i < $totalxml; $i++){
            $data[$i]['id_agent'] = (String) $xmlObj->datacontent[$i]->id_agent;
            $data[$i]['username'] = (String) $xmlObj->datacontent[$i]->username;
            $data[$i]['sip_no'] = (String) $xmlObj->datacontent[$i]->sip_no;
            $data[$i]['email'] = (String) $xmlObj->datacontent[$i]->email;
            $data[$i]['ective'] = (String) $xmlObj->datacontent[$i]->ective;
            $data[$i]['onhold'] = (String) $xmlObj->datacontent[$i]->onhold;
            $data[$i]['role'] = (String) $xmlObj->datacontent[$i]->role;
          
        }
        
        return $data;

    }
    
    function key_sfdc(){
        $xmlFile = $this->config->item('path_xml').'/xml/token.xml';
        $xmlObj = simplexml_load_string($this->Mloadxml->load($xmlFile));
        
        $totalxml = count($xmlObj->datacontent);
        

        $data = array();
        for($i = 0; $i < $totalxml; $i++){
            $data[$i]['TOKEN_PRODUCTION'] = (String) $xmlObj->datacontent[$i]->TOKEN_PRODUCTION;
            
        }
        
        return $data;

    }
    
    function getlogctitransfer(){
        $xmlFile = $this->config->item('path_xml').'/xml/log_cti_transfer.xml';
        $xmlObj = simplexml_load_string($this->Mloadxml->load($xmlFile));
        
        $totalxml = count($xmlObj->datacontent);
        

        $data = array();
        for($i = 0; $i < $totalxml; $i++){
            $data[$i]['ID_VALDO'] = (String) $xmlObj->datacontent[$i]->ID_VALDO;
            $data[$i]['SIP'] = (String) $xmlObj->datacontent[$i]->SIP;
            $data[$i]['DESTINATION_NUMBER'] = (String) $xmlObj->datacontent[$i]->DESTINATION_NUMBER;
            $data[$i]['DARI'] = (String) $xmlObj->datacontent[$i]->DARI;
            $data[$i]['START_DIALING_DATE'] = (String) $xmlObj->datacontent[$i]->START_DIALING_DATE;
            $data[$i]['START_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->START_CALL_DATE;
            $data[$i]['END_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->END_CALL_DATE;
            $data[$i]['DURATION_DIAL'] = (String) $xmlObj->datacontent[$i]->DURATION_DIAL;
            $data[$i]['DURATION_CALL'] = (String) $xmlObj->datacontent[$i]->DURATION_CALL;
            $data[$i]['DURATION_WAIT'] = (String) $xmlObj->datacontent[$i]->DURATION_WAIT;
            $data[$i]['TRANSFER_FROM'] = (String) $xmlObj->datacontent[$i]->TRANSFER_FROM;
            $data[$i]['TRANSFER_TO'] = (String) $xmlObj->datacontent[$i]->TRANSFER_TO;
            $data[$i]['RECORDING_FILE'] = (String) $xmlObj->datacontent[$i]->RECORDING_FILE;
            $data[$i]['STATUS'] = (String) $xmlObj->datacontent[$i]->STATUS;
        }
        
        return $data;

    }
    
    function getdailydata(){
        $xmlFile = $this->config->item('path_xml').'/xml/dailydata/'.str_replace('/', '', $_GET['date']).'.xml';
        $xmlObj = simplexml_load_string($this->Mloadxml->load($xmlFile));
        
        $totalxml = count($xmlObj->datacontent);
        

        $data = array();
        for($i = 0; $i < $totalxml; $i++){
           /*  $data[$i]['ID_VALDO'] = (String) $xmlObj->datacontent[$i]->ID_VALDO;
            $data[$i]['SIP'] = (String) $xmlObj->datacontent[$i]->SIP;
            $data[$i]['DESTINATION_NUMBER'] = (String) $xmlObj->datacontent[$i]->DESTINATION_NUMBER;
            $data[$i]['DARI'] = (String) $xmlObj->datacontent[$i]->DARI;
            $data[$i]['START_DIALING_DATE'] = (String) $xmlObj->datacontent[$i]->START_DIALING_DATE;
            $data[$i]['START_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->START_CALL_DATE;
            $data[$i]['END_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->END_CALL_DATE;
            $data[$i]['DURATION_DIAL'] = (String) $xmlObj->datacontent[$i]->DURATION_DIAL;
            $data[$i]['DURATION_CALL'] = (String) $xmlObj->datacontent[$i]->DURATION_CALL;
            $data[$i]['DURATION_WAIT'] = (String) $xmlObj->datacontent[$i]->DURATION_WAIT;
            $data[$i]['RECORDING_FILE'] = (String) $xmlObj->datacontent[$i]->RECORDING_FILE;
            $data[$i]['STATUS'] = (String) $xmlObj->datacontent[$i]->STATUS; */
			$data[$i]['ID_VALDO'] = (String) $xmlObj->datacontent[$i]->ID_VALDO;
            $data[$i]['SIP'] = (String) $xmlObj->datacontent[$i]->SIP;
            $data[$i]['DESTINATION_NUMBER'] = (String) $xmlObj->datacontent[$i]->DESTINATION_NUMBER;
            $data[$i]['RECORDING_FILE'] = (String) $xmlObj->datacontent[$i]->RECORDING_FILE;
            $data[$i]['START_DIALING_DATE'] = (String) $xmlObj->datacontent[$i]->START_DIALING_DATE;
            $data[$i]['START_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->START_CALL_DATE;
            $data[$i]['END_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->END_CALL_DATE;
            $data[$i]['DURATION_WAIT'] = (String) $xmlObj->datacontent[$i]->DURATION_WAIT;
            $data[$i]['DURATION_CALL'] = (String) $xmlObj->datacontent[$i]->DURATION_CALL;
            $data[$i]['DURATION_DIAL'] = (String) $xmlObj->datacontent[$i]->DURATION_DIAL;
            $data[$i]['STATUS'] = (String) $xmlObj->datacontent[$i]->STATUS;
            $data[$i]['TRANSFER_FROM'] = (String) $xmlObj->datacontent[$i]->DARI;
            $data[$i]['TRANSFER_TO'] = '';
        }
        
        return $data;

    }
    
    function getdailydatatransfer(){
        $xmlFile = $this->config->item('path_xml').'/xml/dailydata/transfer/'.str_replace('/', '', $_GET['date']).'.xml';
        $xmlObj = simplexml_load_string($this->Mloadxml->load($xmlFile));
        
        $totalxml = count($xmlObj->datacontent);
        

        $data = array();
        for($i = 0; $i < $totalxml; $i++){
            $data[$i]['ID_VALDO'] = (String) $xmlObj->datacontent[$i]->ID_VALDO;
            $data[$i]['SIP'] = (String) $xmlObj->datacontent[$i]->SIP;
            $data[$i]['DESTINATION_NUMBER'] = (String) $xmlObj->datacontent[$i]->DESTINATION_NUMBER;
            $data[$i]['DARI'] = (String) $xmlObj->datacontent[$i]->DARI;
            $data[$i]['START_DIALING_DATE'] = (String) $xmlObj->datacontent[$i]->START_DIALING_DATE;
            $data[$i]['START_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->START_CALL_DATE;
            $data[$i]['END_CALL_DATE'] = (String) $xmlObj->datacontent[$i]->END_CALL_DATE;
            $data[$i]['DURATION_DIAL'] = (String) $xmlObj->datacontent[$i]->DURATION_DIAL;
            $data[$i]['DURATION_CALL'] = (String) $xmlObj->datacontent[$i]->DURATION_CALL;
            $data[$i]['DURATION_WAIT'] = (String) $xmlObj->datacontent[$i]->DURATION_WAIT;
            $data[$i]['RECORDING_FILE'] = (String) $xmlObj->datacontent[$i]->RECORDING_FILE;
            $data[$i]['STATUS'] = (String) $xmlObj->datacontent[$i]->STATUS;
        }
        
        return $data;

    }
   
    function daily_data_transfer_xml() {
        $data = $this->get_daily_data_transfer();
        $total = count($data);
        $time = date('Y-m-d H:i:s');
        $xmlfolder = $this->config->item('path_xml') . '/xml/';


        $xmlfile = "daily_data.xml";
        $filePath = $xmlfolder . $xmlfile;
        if (count($data) > 0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            for ($i = 0; $i < $total; $i++) {
                xmlwriter_start_element($wrt, "datacontent");

                xmlwriter_start_element($wrt, "ID_VALDO");
                xmlwriter_text($wrt, $data[$i]['ID_VALDO']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "SIP");
                xmlwriter_text($wrt, $data[$i]['SIP']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "PREV_DUR");
                xmlwriter_text($wrt, $data[$i]['PREV_DUR']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DESTINATION_NUMBER");
                xmlwriter_text($wrt, $data[$i]['DESTINATION_NUMBER']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DARI");
                xmlwriter_text($wrt, $data[$i]['DARI']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "START_DIALING_DATE");
                xmlwriter_text($wrt, $data[$i]['START_DIALING_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "START_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['START_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "END_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['END_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "DURATION_DIAL");
                xmlwriter_text($wrt, $data[$i]['DURATION_DIAL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_CALL");
                xmlwriter_text($wrt, $data[$i]['DURATION_CALL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_WAIT");
                xmlwriter_text($wrt, $data[$i]['DURATION_WAIT']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "RECORDING_FILE");
                xmlwriter_text($wrt, $data[$i]['RECORDING_FILE']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "STATUS");
                xmlwriter_text($wrt, $data[$i]['STATUS']);
                xmlwriter_end_element($wrt);

                xmlwriter_end_element($wrt);
            }

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "dailydata");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
    
   function daily_data_xml() {
        $data = $this->get_daily_data();
        $total = count($data);
        $time = date('Y-m-d H:i:s');
        $xmlfolder = $this->config->item('path_xml') . '/xml/';


        $xmlfile = "daily_data.xml";
        $filePath = $xmlfolder . $xmlfile;
        if (count($data) > 0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            for ($i = 0; $i < $total; $i++) {
                xmlwriter_start_element($wrt, "datacontent");

                xmlwriter_start_element($wrt, "ID_VALDO");
                xmlwriter_text($wrt, $data[$i]['ID_VALDO']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "SIP");
                xmlwriter_text($wrt, $data[$i]['SIP']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "PREV_DUR");
                xmlwriter_text($wrt, $data[$i]['PREV_DUR']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DESTINATION_NUMBER");
                xmlwriter_text($wrt, $data[$i]['DESTINATION_NUMBER']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DARI");
                xmlwriter_text($wrt, $data[$i]['DARI']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "START_DIALING_DATE");
                xmlwriter_text($wrt, $data[$i]['START_DIALING_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "START_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['START_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "END_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['END_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "DURATION_DIAL");
                xmlwriter_text($wrt, $data[$i]['DURATION_DIAL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_CALL");
                xmlwriter_text($wrt, $data[$i]['DURATION_CALL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_WAIT");
                xmlwriter_text($wrt, $data[$i]['DURATION_WAIT']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "RECORDING_FILE");
                xmlwriter_text($wrt, $data[$i]['RECORDING_FILE']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "STATUS");
                xmlwriter_text($wrt, $data[$i]['STATUS']);
                xmlwriter_end_element($wrt);

                xmlwriter_end_element($wrt);
            }

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "dailydata");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
    
  
    function deleteXMLPreview($channel_id, $content_id) {

        $xmlfile = $this->config->item('path_xml') . '/' . $channel_id . '/' . $content_id . ".xml";

        unlink($xmlfile);
    }
    
    function get_daily_data() {
         date_default_timezone_set('asia/jakarta');
        $where = "WHERE DATE_FORMAT(calldate, '%Y/%m/%d')='" .date('Y/m/d'). "'";
        
       
        $tanggal = date('Y/m/d');
       
        
        $sql = "SELECT
        
        uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN src
        WHEN dcontext = 'ext-queues' AND lastapp = 'Queue'  THEN SUBSTR(dstchannel,5,3) 
        WHEN dcontext = 'from-internal-xfer' THEN dst
        ELSE dst END) AS SIP,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration
        ELSE dst END) AS PREV_DUR,
        SUBSTRING(dst, 4)
         AS DESTINATION_NUMBER,src AS DARI, 

		(CASE  
          WHEN dcontext = 'from-internal' THEN calldate
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue 	
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN calldate
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN DATE_ADD(datetime_init, INTERVAL (duration_cc) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN duration
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN (duration_cc + duration_wait_cc - 1)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN billsec
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN duration_cc
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN disposition = 'ANSWERED' AND dcontext='from-internal' THEN (duration-billsec)
           WHEN disposition = 'ANSWERED' AND dcontext='ext-queues' THEN (duration_wait_cc - 1)
           WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN (duration-billsec)
           ELSE 0 END) AS DURATION_WAIT, (CASE 
        WHEN CHAR_LENGTH(recordingfile) > 75 THEN 
        replace(recordingfile, '/var/spool/asterisk/monitor/" . $tanggal . "', 'https://axasales.valdo-intl.com/recording/" . $tanggal . "')
        ELSE CONCAT('https://axasales.valdo-intl.com/recording/" . $tanggal . "/',recordingfile) END) AS RECORDING_FILE,
       
       (CASE 
        WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log   
        FROM cdr $where  AND recordingfile !='' AND dcontext !='from-internal-xfer' AND disposition!='FAILED' AND lastapp !='Transferred Call'";

        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        
        
        $query->free_result();
        $this->db->close();
    }
    
    
    function get_data_call_in_terminada() {
        $sql = "SELECT 
        uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN src
        WHEN dcontext = 'ext-queues' AND lastapp = 'Queue'  THEN SUBSTR(dstchannel,5,3) 
        WHEN dcontext = 'from-internal-xfer' THEN dst
        ELSE dst END) AS SIP,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration
        ELSE dst END) AS PREV_DUR,
        SUBSTRING(dst, 4)
         AS DESTINATION_NUMBER,src AS DARI, 
        recordingfile as RECORDING_FILE,

		(CASE  
          WHEN dcontext = 'from-internal' THEN calldate
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue 	
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN calldate
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN DATE_ADD(datetime_init, INTERVAL (duration_cc) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN duration
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN (duration_cc + duration_wait_cc )
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN billsec
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN duration_cc
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN disposition = 'ANSWERED' AND dcontext='from-internal' THEN (duration-billsec)
           WHEN disposition = 'ANSWERED' AND dcontext='ext-queues' THEN (duration_wait_cc)
           WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN (duration-billsec)
           ELSE 0 END) AS DURATION_WAIT,
       (CASE 
        WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log  
        FROM cdr WHERE disposition!='FAILED' AND lastapp !='Transferred Call' AND id_user!='' AND dcontext!='from-zaptel' AND dcontext!='from-internal-xfer' AND date(calldate) = CURDATE() AND status_log ='0' AND recordingfile !=''";
        //date(calldate) = '2017-12-06'
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        $query->free_result();
        $this->db->close();
    
        }
    
    
     function get_data_call_in_terminada_t() {
        $sql = "SELECT 
        
        t1.uniqueid AS ID_VALDO,
        (CASE  
        WHEN t1.dcontext = 'from-internal' THEN t1.src
        WHEN t1.dcontext = 'ext-queues' AND t1.lastapp = 'Queue'  THEN SUBSTR(t1.dstchannel,5,3) 
        WHEN t1.dcontext = 'from-internal-xfer' THEN t1.dst
        ELSE t1.dst END) AS SIP,
        (CASE  
        WHEN t1.dcontext = 'from-internal' THEN 0
        WHEN t1.dcontext = 'ext-queues' THEN 0
        WHEN t1.dcontext = 'from-internal-xfer' THEN t1.duration
        ELSE t1.dst END) AS PREV_DUR,
        SUBSTRING(t1.dst, 4)
         AS DESTINATION_NUMBER,t1.src AS DARI, 
        t1.recordingfile as RECORDING_FILE,

		(CASE  
          WHEN t1.dcontext = 'from-internal' THEN t1.calldate
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.date_time_entry_queue 	
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN t1.dcontext = 'from-internal' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration-t1.billsec) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.datetime_init
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  t1.dcontext = 'from-internal' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN DATE_ADD(t1.datetime_init, INTERVAL (t1.duration_cc) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal' THEN t1.duration
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN (t1.duration_cc + t1.duration_wait_cc - 1)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN ((DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)-DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)))
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal' THEN t1.billsec
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.duration_cc
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN ((DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)-DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)))
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext='from-internal' THEN 0
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext='ext-queues' THEN 0
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN 0
           ELSE 0 END) AS DURATION_WAIT,
       (CASE 
        WHEN t1.dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN t1.dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(t1.dcontext = 'from-internal-xfer', t1.src, '') AS TRANSFER_FROM ,
        IF(t1.dcontext = 'from-internal-xfer', t1.dst, '') AS TRANSFER_TO,t1.status_log  
        
       
      
        FROM cdr t1
          INNER JOIN 
        cdr t2 on t1.recordingfile = t2.recordingfile
        WHERE t1.disposition!='FAILED' AND t1.lastapp !='Transferred Call' AND t1.dcontext='from-internal-xfer' AND t2.dcontext = 'ext-queues' AND date(t1.calldate) = CURDATE() AND t1.status_log ='0' AND t2.status_log ='1' AND t1.recordingfile !=''
        ";
        //date(calldate) = '2017-12-06'
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        $query->free_result();
        $this->db->close();
        }
    
    
    function get_daily_data_transfer() {
          date_default_timezone_set('asia/jakarta');
//        $where = "WHERE DATE_FORMAT(calldate, '%Y/%m/%d')='2018/04/30'";
//        $tanggal = "2018/04/30";
        $where = "WHERE DATE_FORMAT(calldate, '%Y/%m/%d')='" .date('Y/m/d'). "'";
       
        $tanggal = date('Y/m/d');
       
        
        $sql = "SELECT
        
        uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN src
        WHEN dcontext = 'ext-queues' AND lastapp = 'Queue'  THEN SUBSTR(dstchannel,5,3) 
        WHEN dcontext = 'from-internal-xfer' THEN dst
        ELSE dst END) AS SIP,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration
        ELSE dst END) AS PREV_DUR,
        SUBSTRING(dst, 4)
         AS DESTINATION_NUMBER,src AS DARI, 

		(CASE  
          WHEN dcontext = 'from-internal' THEN calldate
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue 	
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN calldate
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN DATE_ADD(datetime_init, INTERVAL (duration_cc) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN duration
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN (duration_cc + duration_wait_cc - 1)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN billsec
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN duration_cc
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN disposition = 'ANSWERED' AND dcontext='from-internal' THEN (duration-billsec)
           WHEN disposition = 'ANSWERED' AND dcontext='ext-queues' THEN (duration_wait_cc - 1)
           WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN (duration-billsec)
           ELSE 0 END) AS DURATION_WAIT, (CASE 
        WHEN CHAR_LENGTH(recordingfile) > 75 THEN 
        replace(recordingfile, '/var/spool/asterisk/monitor/" . $tanggal . "', 'https://axasales.valdo-intl.com/recording/" . $tanggal . "')
        ELSE CONCAT('https://axasales.valdo-intl.com/recording/" . $tanggal . "/',recordingfile) END) AS RECORDING_FILE,
       
       (CASE 
        WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log   
        FROM cdr $where  AND recordingfile !='' AND dcontext ='from-internal-xfer' AND disposition !='FAILED' AND lastapp ='Transferred Call'";

        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        $query->free_result();
        $this->db->close();
    }

}
