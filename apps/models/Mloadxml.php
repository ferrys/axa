<?php

class Mloadxml extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function load($xmlFile) {
        if (is_file($xmlFile)) {
            $strXml = file_get_contents($xmlFile);
            $strXml = utf8_encode($strXml);
        } else {
            $strXml = $this->config->item('xml_blank');
        }
        return $strXml;
    }

}

/* End of file */