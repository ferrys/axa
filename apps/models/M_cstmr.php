<?php

class M_cstmr extends CI_Model {

    var $call_center;

    function __construct() {
        $this->errors = "";
        $this->load->database();
        $this->call_center = $this->load->database('call_center', true);
    }
    
     function optimasi()
     {
        $sql = 'REPAIR TABLE cdr,agent_login';
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
     }
     
     function optimasi_agent()
     {
        $sql = 'REPAIR TABLE agent';
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
     }
     
     function repair_agent()
     {
         $sql = 'OPTIMIZE TABLE agent';
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
     }
     
     function repair()
     {
         $sql = 'OPTIMIZE TABLE cdr,agent_login';
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
     }
            
     function get_hold($id) {
        $sql = "SELECT * from td_hold_duration WHERE uniqueid=".$id."";
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }
    
    function get_data_call_in() {
        $sql = "SELECT a.*,b.* from call_entry AS a INNER JOIN agent as b ON b.id = a.id_agent WHERE a.status ='activa' and a.status_log ='0'";
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }
    
    function get_data_inbound() {
        $sql = "SELECT a.datetime_init AS START_TIME, a.datetime_end AS DATE_TIME_END,a.callerid AS CALLER, a.hangup_side AS HANGUP_SIDE, b.number AS EXT, b.name AS USERNAME
               from call_entry AS a
               INNER JOIN agent AS b ON a.id_agent = b.id
               WHERE a.hangup_side ='COMPLETECALLER' OR a.hangup_side ='COMPLETEAGENT' ";
        //a.datetime_init !='null'  AND a.status='terminada' AND a.id_agent !='NULL' AND
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }

    function sumarydailyreport() {
        $sql = "SELECT 'Call Offered' AS descr, COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003')
                THEN uniqueid
                END ) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT 'Answered Call Distribution' AS descr, COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel != ''
                THEN uniqueid
                END ) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT 'Answered Call Distribution (<20sec)' AS descr, COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel != '' AND duration_wait_cc < '20'
                THEN uniqueid
                END ) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT 'Abandon' AS descr, COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel = ''
                THEN uniqueid
                END ) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT '- Less than 5 second' AS descr, COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel = '' and duration_wait_cc <= '5'
                THEN uniqueid
                END ) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT '- More than 5 second' AS descr, COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel = '' and duration_wait_cc > '5'
                THEN uniqueid
                END ) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT '% Abandon' AS descr, ROUND(COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel = ''
                THEN uniqueid
                END )/ COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003')
                THEN uniqueid
                END )*100, 2) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT 'AHT' AS descr,SEC_TO_TIME(floor(AVG(duration_cc))) AS total FROM cdr ce
                WHERE lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel != '' AND DATE(ce.date_time_entry_queue) = '".$_GET['date']."' 
                UNION ALL
                SELECT 'Service Level' AS descr, ROUND(COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003') and dstchannel != '' AND duration_wait_cc < '20'
                THEN uniqueid
                END )/ COUNT(
                CASE WHEN lastapp='Queue' and dst IN('5000','5002','5003')
                THEN uniqueid
                END )*100, 2) AS total
                FROM cdr ce
                WHERE DATE( ce.date_time_entry_queue ) = '".$_GET['date']."'
                UNION ALL
                SELECT 'People Standby' AS descr,COUNT(DISTINCT CASE WHEN disposition LIKE 'ANSWERED' AND channel !='' AND lastapp LIKE 'Queue' THEN SUBSTRING(dstchannel, 1, 7) END) AS total FROM cdr ce
                WHERE DATE(ce.calldate) = '".$_GET['date']."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
	   
		// $query->free_result();
    }

    function get_data_interval() {
        $sql = "SELECT a.tgl, a.jam, CASE WHEN a.answered IS NOT NULL THEN a.answered ELSE 0 END
        + a.abandoned AS coff, 
        CASE WHEN a.answered IS NOT NULL THEN a.answered ELSE 0 END AS acd,acd_kurang5s,acd_5sd20,acd_kurang20s,acd_kurang30s,acd_up20s, a.abandoned,abandoned_5s,abandoned_20s,abandoned_up20s, AVG_abandone, AVG_speed,AVG_acd, 
        concat(round(acd_kurang20s/(a.answered+ a.abandoned)*100,2),'%') as SLA20S,
        concat(round(acd_kurang30s/(a.answered+ a.abandoned)*100,2),'%') as SLA30S,
        concat(round(acd_kurang20s/(a.answered)*100,2),'%') as SLA20SACD
        FROM (
        SELECT DATE(ce.datetime_entry_queue) AS tgl,
        CASE WHEN TIME(ce.datetime_entry_queue) >= '07:00:00' AND TIME(ce.datetime_entry_queue) < '07:15:00'
        THEN '07:00:00 - 07:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '07:15:00' AND TIME(ce.datetime_entry_queue) < '07:30:00'
        THEN '07:15:00 - 07:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '07:30:00' AND TIME(ce.datetime_entry_queue) < '07:45:00'
        THEN '07:30:00 - 07:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '07:45:00' AND TIME(ce.datetime_entry_queue) < '08:00:00'
        THEN '07:45:00 - 08:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '08:00:00' AND TIME(ce.datetime_entry_queue) < '08:15:00'
        THEN '08:00:00 - 08:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '08:15:00' AND TIME(ce.datetime_entry_queue) < '08:30:00'
        THEN '08:15:00 - 08:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '08:30:00' AND TIME(ce.datetime_entry_queue) < '08:45:00'
        THEN '08:30:00 - 08:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '08:45:00' AND TIME(ce.datetime_entry_queue) < '09:00:00'
        THEN '08:45:00 - 09:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '09:00:00' AND TIME(ce.datetime_entry_queue) < '09:15:00'
        THEN '09:00:00 - 09:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '09:15:00' AND TIME(ce.datetime_entry_queue) < '09:30:00'
        THEN '09:15:00 - 09:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '09:30:00' AND TIME(ce.datetime_entry_queue) < '09:45:00'
        THEN '09:30:00 - 09:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '09:45:00' AND TIME(ce.datetime_entry_queue) < '10:00:00'
        THEN '09:45:00 - 10:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '10:00:00' AND TIME(ce.datetime_entry_queue) < '10:15:00'
        THEN '10:00:00 - 10:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '10:15:00' AND TIME(ce.datetime_entry_queue) < '10:30:00'
        THEN '10:15:00 - 10:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '10:30:00' AND TIME(ce.datetime_entry_queue) < '10:45:00'
        THEN '10:30:00 - 10:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '10:45:00' AND TIME(ce.datetime_entry_queue) < '11:00:00'
        THEN '10:45:00 - 11:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '11:00:00' AND TIME(ce.datetime_entry_queue) < '11:15:00'
        THEN '11:00:00 - 11:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '11:15:00' AND TIME(ce.datetime_entry_queue) < '11:30:00'
        THEN '11:15:00 - 11:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '11:30:00' AND TIME(ce.datetime_entry_queue) < '11:45:00'
        THEN '11:30:00 - 11:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '11:45:00' AND TIME(ce.datetime_entry_queue) < '12:00:00'
        THEN '11:45:00 - 12:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '12:00:00' AND TIME(ce.datetime_entry_queue) < '12:15:00'
        THEN '12:00:00 - 12:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '12:15:00' AND TIME(ce.datetime_entry_queue) < '12:30:00'
        THEN '12:15:00 - 12:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '12:30:00' AND TIME(ce.datetime_entry_queue) < '12:45:00'
        THEN '12:30:00 - 12:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '12:45:00' AND TIME(ce.datetime_entry_queue) < '13:00:00'
        THEN '12:45:00 - 13:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '13:00:00' AND TIME(ce.datetime_entry_queue) < '13:15:00'
        THEN '13:00:00 - 13:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '13:15:00' AND TIME(ce.datetime_entry_queue) < '13:30:00'
        THEN '13:15:00 - 13:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '13:30:00' AND TIME(ce.datetime_entry_queue) < '13:45:00'
        THEN '13:30:00 - 13:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '13:45:00' AND TIME(ce.datetime_entry_queue) < '14:00:00'
        THEN '13:45:00 - 14:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '14:00:00' AND TIME(ce.datetime_entry_queue) < '14:15:00'
        THEN '14:00:00 - 14:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '14:15:00' AND TIME(ce.datetime_entry_queue) < '14:30:00'
        THEN '14:15:00 - 14:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '14:30:00' AND TIME(ce.datetime_entry_queue) < '14:45:00'
        THEN '14:30:00 - 14:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '14:45:00' AND TIME(ce.datetime_entry_queue) < '15:00:00'
        THEN '14:45:00 - 15:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '15:00:00' AND TIME(ce.datetime_entry_queue) < '15:15:00'
        THEN '15:00:00 - 15:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '15:15:00' AND TIME(ce.datetime_entry_queue) < '15:30:00'
        THEN '15:15:00 - 15:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '15:30:00' AND TIME(ce.datetime_entry_queue) < '15:45:00'
        THEN '15:30:00 - 15:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '15:45:00' AND TIME(ce.datetime_entry_queue) < '16:00:00'
        THEN '15:45:00 - 16:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '16:00:00' AND TIME(ce.datetime_entry_queue) < '16:15:00'
        THEN '16:00:00 - 16:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '16:15:00' AND TIME(ce.datetime_entry_queue) < '16:30:00'
        THEN '16:15:00 - 16:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '16:30:00' AND TIME(ce.datetime_entry_queue) < '16:45:00'
        THEN '16:30:00 - 16:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '16:45:00' AND TIME(ce.datetime_entry_queue) < '17:00:00'
        THEN '16:45:00 - 17:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '17:00:00' AND TIME(ce.datetime_entry_queue) < '17:15:00'
        THEN '17:00:00 - 17:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '17:15:00' AND TIME(ce.datetime_entry_queue) < '17:30:00'
        THEN '17:15:00 - 17:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '17:30:00' AND TIME(ce.datetime_entry_queue) < '17:45:00'
        THEN '17:30:00 - 17:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '17:45:00' AND TIME(ce.datetime_entry_queue) < '18:00:00'
        THEN '17:45:00 - 18:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '18:00:00' AND TIME(ce.datetime_entry_queue) < '18:15:00'
        THEN '18:00:00 - 18:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '18:15:00' AND TIME(ce.datetime_entry_queue) < '18:30:00'
        THEN '18:15:00 - 18:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '18:30:00' AND TIME(ce.datetime_entry_queue) < '18:45:00'
        THEN '18:30:00 - 18:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '18:45:00' AND TIME(ce.datetime_entry_queue) < '19:00:00'
        THEN '18:45:00 - 19:00:00'
        WHEN TIME(ce.datetime_entry_queue) >= '19:00:00' AND TIME(ce.datetime_entry_queue) < '19:15:00'
        THEN '19:00:00 - 19:15:00'
        WHEN TIME(ce.datetime_entry_queue) >= '19:15:00' AND TIME(ce.datetime_entry_queue) < '19:30:00'
        THEN '19:15:00 - 19:30:00'
        WHEN TIME(ce.datetime_entry_queue) >= '19:30:00' AND TIME(ce.datetime_entry_queue) < '19:45:00'
        THEN '19:30:00 - 19:45:00'
        WHEN TIME(ce.datetime_entry_queue) >= '19:45:00' AND TIME(ce.datetime_entry_queue) < '20:00:00'
        THEN '19:45:00 - 20:00:00'
         END AS jam,
        COUNT(CASE WHEN (`status` = 'abandonada' OR `status` = 'terminada' OR `status` = 'en-cola' OR `status` = 'fin-monitoreo') THEN id END) AS entered,
        COUNT(CASE WHEN `status` = 'terminada' THEN id END) AS answered,
        COUNT(CASE WHEN `status` = 'terminada' AND ce.duration < 5 THEN id END) AS acd_kurang5s,
        COUNT(CASE WHEN `status` = 'terminada' AND ce.duration >= 5 AND ce.duration <= 20 THEN id END) AS acd_5sd20,
        COUNT(CASE WHEN `status` = 'terminada' AND ce.duration <= 20 THEN id END) AS acd_kurang20s,
        COUNT(CASE WHEN `status` = 'terminada' AND ce.duration <= 30 THEN id END) AS acd_kurang30s,
        COUNT(CASE WHEN `status` = 'terminada' AND ce.duration > 20 THEN id END) AS acd_up20s,
        COUNT(CASE WHEN `status` = 'abandonada' THEN id END) AS abandoned,
        COUNT(CASE WHEN `status` = 'abandonada' AND ce.duration_wait <= 5 THEN id END) AS abandoned_5s,
        COUNT(CASE WHEN `status` = 'abandonada' AND ce.duration_wait <= 20 THEN id END) AS abandoned_20s,
        COUNT(CASE WHEN `status` = 'abandonada' AND ce.duration_wait > 20 THEN id END) AS abandoned_up20s,
        (SELECT SEC_TO_TIME(floor(AVG(duration_wait))) FROM call_center.call_entry ce WHERE DATE(ce.datetime_entry_queue) BETWEEN '".$_GET['start']."' AND '".$_GET['start']."' AND `status` = 'abandonada') AS AVG_abandone,
        (SELECT SEC_TO_TIME(floor(AVG(datetime_init-datetime_entry_queue))) FROM call_center.call_entry ce WHERE DATE(ce.datetime_entry_queue) BETWEEN '".$_GET['start']."' AND '".$_GET['start']."' AND `status` = 'terminada') AS AVG_speed,
        (SELECT SEC_TO_TIME(floor(AVG(duration))) FROM call_center.call_entry ce WHERE DATE(ce.datetime_entry_queue) BETWEEN '".$_GET['start']."' AND '".$_GET['start']."' AND `status` = 'terminada') AS AVG_acd
        FROM call_center.call_entry ce WHERE DATE(ce.datetime_entry_queue) BETWEEN '".$_GET['start']."' AND '".$_GET['start']."'
        GROUP BY tgl, jam order by jam limit 1,100) a";
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }

//        WHEN TIME(ce.datetime_entry_queue) >= '20:00:00' AND TIME(ce.datetime_entry_queue) < '20:15:00'
//        THEN '20:00:00 - 20:15:00'
//        WHEN TIME(ce.datetime_entry_queue) >= '20:15:00' AND TIME(ce.datetime_entry_queue) < '20:30:00'
//        THEN '20:15:00 - 20:30:00'
//        WHEN TIME(ce.datetime_entry_queue) >= '20:30:00' AND TIME(ce.datetime_entry_queue) < '20:45:00'
//        THEN '20:30:00 - 20:45:00'
//        WHEN TIME(ce.datetime_entry_queue) >= '20:45:00' AND TIME(ce.datetime_entry_queue) < '21:00:00'
//        THEN '20:45:00 - 21:00:00'
    function get_id_user() {
        $sql = "SELECT uniqueid,id_agent FROM call_entry";
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }

//   function update_c($id) {
//        $sql = 'UPDATE cdr SET id_user=' . $cname . ' WHERE uniqueid IN('.$id.')';
//        $this->db->query($sql);
//        return TRUE;
//    }

    function get_daily_data() {
        if ($_GET['date'] == "") {
            $where = "";
        } else {
            $where = "WHERE DATE_FORMAT(calldate, '%Y/%m/%d')='" . $_GET['date'] . "'";
        }
        date_default_timezone_set('asia/jakarta');
        $tanggal = $_GET['date'];
        //$tanggal =date("Y").'/'.date("m").'/'.date("d");
//        $sql = "SELECT 
//        uniqueid AS ID_VALDO, 
//        
//       (CASE 
//        WHEN dcontext = 'from-internal' THEN SUBSTR(channel,5,3) 
//        WHEN dcontext = 'ext-queues' THEN  SUBSTR(dstchannel,5,3) 
//        ELSE SUBSTR(dstchannel,5,3)  END) AS SIP, 
//        SUBSTR(dst,4) AS DESTINATION_NUMBER, 
//       (CASE 
//        WHEN CHAR_LENGTH(recordingfile) > 60 THEN 
//        replace(recordingfile, '/var/spool/asterisk/monitor/" . $tanggal . "', 'https://axasales.valdo-intl.com/recording/" . $tanggal . "')
//        ELSE CONCAT('https://axasales.valdo-intl.com/recording/" . $tanggal . "/',recordingfile) END) AS RECORDING_FILE,
//        calldate AS START_DIALING_DATE,
//        DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND) AS START_CALL_DATE,
//        DATE_ADD(calldate, INTERVAL (duration) SECOND) AS END_CALL_DATE,
//       (duration-billsec) AS DURATION_WAIT,
//        billsec AS DURATION_CALL,
//        duration AS DURATION_DIAL,
//        
//       (CASE 
//        WHEN dcontext = 'from-internal' THEN 'OUTBOUND' 
//        WHEN dcontext = 'ext-queues' THEN  'INBOUND' 
//        ELSE 'TRANSFER' END) AS STATUS,
//        
//        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM,
//        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO
//        FROM cdr $where ORDER BY calldate DESC";
        
        
        $sql = "SELECT
        
        uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN src
        WHEN dcontext = 'ext-queues' AND lastapp = 'Queue'  THEN SUBSTR(dstchannel,5,3) 
        WHEN dcontext = 'from-internal-xfer' THEN dst
        ELSE dst END) AS SIP,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration
        ELSE dst END) AS PREV_DUR,
        SUBSTRING(dst, 4)
         AS DESTINATION_NUMBER,src AS DARI, 

		(CASE  
          WHEN dcontext = 'from-internal' THEN calldate
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue 	
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN calldate
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN DATE_ADD(datetime_init, INTERVAL (duration_cc) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN duration
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN (duration_cc + duration_wait_cc - 1)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN billsec
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN duration_cc
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN disposition = 'ANSWERED' AND dcontext='from-internal' THEN (duration-billsec)
           WHEN disposition = 'ANSWERED' AND dcontext='ext-queues' THEN (duration_wait_cc - 1)
           WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN (duration-billsec)
           ELSE 0 END) AS DURATION_WAIT, (CASE 
        WHEN CHAR_LENGTH(recordingfile) > 75 THEN 
        replace(recordingfile, '/var/spool/asterisk/monitor/" . $tanggal . "', 'https://axasales.valdo-intl.com/recording/" . $tanggal . "')
        ELSE CONCAT('https://axasales.valdo-intl.com/recording/" . $tanggal . "/',recordingfile) END) AS RECORDING_FILE,
       
       (CASE 
        WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log   
        FROM cdr $where  AND recordingfile !='' AND dcontext !='from-internal-xfer' AND disposition!='FAILED' AND lastapp !='Transferred Call'";

        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }
    
    
    function get_daily_data_transfer() {
        if ($_GET['date'] == "") {
            $where = "";
        } else {
            $where = "WHERE DATE_FORMAT(t1.calldate, '%Y/%m/%d')='" . $_GET['date'] . "'";
        }
        date_default_timezone_set('asia/jakarta');
        $tanggal = $_GET['date'];
        
        $sql = "SELECT 
        
        t1.uniqueid AS ID_VALDO,
        (CASE  
        WHEN t1.dcontext = 'from-internal' THEN t1.src
        WHEN t1.dcontext = 'ext-queues' AND t1.lastapp = 'Queue'  THEN SUBSTR(t1.dstchannel,5,3) 
        WHEN t1.dcontext = 'from-internal-xfer' THEN t1.dst
        ELSE t1.dst END) AS SIP,
        (CASE  
        WHEN t1.dcontext = 'from-internal' THEN 0
        WHEN t1.dcontext = 'ext-queues' THEN 0
        WHEN t1.dcontext = 'from-internal-xfer' THEN t1.duration
        ELSE t1.dst END) AS PREV_DUR,
        SUBSTRING(t1.dst, 4)
         AS DESTINATION_NUMBER,t1.src AS DARI, 
         (CASE 
        WHEN CHAR_LENGTH(t1.recordingfile) > 75 THEN 
        replace(t1.recordingfile, '/var/spool/asterisk/monitor/" . $tanggal . "', 'https://axasales.valdo-intl.com/recording/" . $tanggal . "')
        ELSE CONCAT('https://axasales.valdo-intl.com/recording/" . $tanggal . "/',t1.recordingfile) END) AS RECORDING_FILE,
       

		(CASE  
          WHEN t1.dcontext = 'from-internal' THEN t1.calldate
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.date_time_entry_queue 	
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN t1.dcontext = 'from-internal' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration-t1.billsec) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.datetime_init
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  t1.dcontext = 'from-internal' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN DATE_ADD(t1.datetime_init, INTERVAL (t1.duration_cc) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal' THEN t1.duration
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN (t1.duration_cc + t1.duration_wait_cc - 1)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN (DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)-DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND))
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal' THEN t1.billsec
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.duration_cc
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN (DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)-DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND))
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext='from-internal' THEN 0
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext='ext-queues' THEN 0
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN 0
           ELSE 0 END) AS DURATION_WAIT,
       (CASE 
        WHEN t1.dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN t1.dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(t1.dcontext = 'from-internal-xfer', t1.src, '') AS TRANSFER_FROM ,
        IF(t1.dcontext = 'from-internal-xfer', t1.dst, '') AS TRANSFER_TO,t1.status_log  
        
       
      
FROM cdr t1
  INNER JOIN 
cdr t2 on t1.recordingfile = t2.recordingfile
 $where AND t1.disposition!='FAILED' AND t2.duration_cc IS NOT NULL AND t1.lastapp !='Transferred Call' AND t1.dcontext='from-internal-xfer' AND t2.dcontext = 'ext-queues'  AND t1.recordingfile !=''";

        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }

    function get_daily_aux() {
        if ($_GET['date'] == "") {
            $where = "NULL";
        } else {
            $where = "WHERE DATE_FORMAT(agent_activity_view.LOGIN_TIME,'%d/%m/%Y')='" . $_GET['date'] . "'";
        }

        $sql = "SELECT DATE_FORMAT(agent_activity_view.LOGIN_TIME,'%d/%m/%Y') AS DATE,agent_activity_view.SIP, cdr_view_bak.SERVICE_TIME, cdr_view_bak.AVERAGE_TALK_TIME, agent_aux_counter_view.AUX_SECOND AS AUX,  agent_activity_view.LOGIN_TIME FROM  agent_activity_view
        LEFT JOIN  cdr_view_bak ON  agent_activity_view.SIP =  cdr_view_bak.SIP
        LEFT JOIN  agent_aux_counter_view ON  cdr_view_bak.SIP =  agent_aux_counter_view.SIP
        $where GROUP BY agent_activity_view.SIP";
//        $sql = "SELECT a.sip_no AS SIP,TIME_FORMAT(SEC_TO_TIME(SUM(a.aux_time)),'%H:%i:%s') AS AUX, SUM(c.billsec) AS TOTAL_CALL,SUM(c.duration) AS TOTAL_DIAL,
//        TIME_FORMAT(SEC_TO_TIME(SUM(c.billsec)),'%H:%i:%s')AS SERVICE_TIME,
//        TIME_FORMAT(SEC_TO_TIME(FLOOR(SUM(c.billsec)/count(c.cnum))),'%H:%i:%s')AS AVERAGE_TALK_TIME
//        FROM cdr AS c 
//        INNER JOIN agent_aux_counter AS a ON a.sip_no =  c.cnum
//        WHERE date(a.stamp) = CURDATE()
//        GROUP BY sip_no//INNER JOIN agent_activity_view AS c ON b.DATE_AUX = DATE_FORMAT( c.LOGIN_TIME,  '%d/%m/%Y' ) $where
//        ";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }

    function get_daily_agent_activity() {
//        if($_GET['date']=="WHERE 1")
//        {$where ="NULL";}
//        else {
        $where = "WHERE DATE_FORMAT(agent_activity.time,'%d/%m/%Y')='" . $_GET['date'] . "'";

//     }

        $sql = "SELECT sip_no, time FROM agent_activity " . $where . " AND activity ='AGENT LOGIN OK' ORDER BY time DESC
            UNION
            SELECT sip_no, time FROM agent_activity  " . $where . " AND activity ='AGENT LOGOFF OK' ORDER BY time ASC
            ";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }
    function getdurationcalls()
    {
        $sql = "SELECT id_agent,duration_wait,datetime_entry_queue,datetime_init,duration,uniqueid,datetime_end from call_entry where status_log = '1' AND  status ='terminada'";
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }
	
	function getAbandonecall()
    {
        $sql = "SELECT id_agent,duration_wait,datetime_entry_queue,datetime_init,duration,uniqueid,datetime_end from call_entry where date(datetime_entry_queue) = curdate() AND status = 'abandonada'";
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }
    
    function getdurationcall()
    {
        $sql = "SELECT id_agent,duration_wait,datetime_entry_queue,datetime_init,duration,uniqueid,datetime_end from call_entry where date(datetime_end) = CURDATE() AND status_log='1' AND status = 'terminada' LIMIT 10 ";
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }

    function getdurationcallmanual()
    {
        $sql = "SELECT id_agent,duration_wait,datetime_entry_queue,datetime_init,duration,uniqueid,datetime_end from call_entry where date(datetime_end) = '2018/12/06' AND status = 'terminada'";
 		 /* $sql = "SELECT de.id_agent, de.duration_wait, de.datetime_entry_queue, de.datetime_init, de.duration, de.uniqueid, de.datetime_end
				FROM call_center.call_entry de
				WHERE date( de.datetime_end )
				BETWEEN '2018-05-17'
				AND '2018-05-30'
				AND de.status = 'terminada'
				AND de.uniqueid
				IN (

				SELECT ce.uniqueid
				FROM asteriskcdrdb.cdr ce
				WHERE DATE( ce.date_time_entry_queue )
				BETWEEN '2018-05-17'
				AND '2018-05-30'
				AND ce.lastapp = 'Queue'
				AND ce.dstchannel != ''
				AND ce.`datetime_init` = '0000-00-00 00:00:00'
				)"; */
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();

    }
	
	function getdurationcallAll()
    {
       $sql = "SELECT id_agent,duration_wait,datetime_entry_queue,datetime_init,duration,uniqueid,datetime_end from call_entry where date(datetime_end) = CURDATE() AND status = 'terminada'";
 		 /* $sql = "SELECT de.id_agent, de.duration_wait, de.datetime_entry_queue, de.datetime_init, de.duration, de.uniqueid, de.datetime_end
				FROM call_center.call_entry de
				WHERE date( de.datetime_end )
				BETWEEN '2018-05-17'
				AND '2018-05-30'
				AND de.status = 'terminada'
				AND de.uniqueid
				IN (

				SELECT ce.uniqueid
				FROM asteriskcdrdb.cdr ce
				WHERE DATE( ce.date_time_entry_queue )
				BETWEEN '2018-05-17'
				AND '2018-05-30'
				AND ce.lastapp = 'Queue'
				AND ce.dstchannel != ''
				AND ce.`datetime_init` = '0000-00-00 00:00:00'
				)"; */
        $query = $this->call_center->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->call_center->close();
    }
	
    function updateanu($data,$id) {
        $sql = "UPDATE cdr SET id_user='$data' WHERE dcontext!='ext-queues' AND clid='$id' AND clid!='' ";
        $this->db->query($sql);
        return TRUE;
    }
    function getdurationcall2()
    {
        $sql = "SELECT * from agent";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
       $this->db->close();
    }

    function get_daily_aux_by_agent($id) {
        $sql = "SELECT * FROM agent_activity WHERE id_agent ='$id' ORDER BY time DESC";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
    }

    function get_data_call_in_terminada() {
        $sql = "SELECT 
        uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN src
        WHEN dcontext = 'ext-queues' AND lastapp = 'Queue'  THEN SUBSTR(dstchannel,5,3) 
        WHEN dcontext = 'from-internal-xfer' THEN dst
        ELSE dst END) AS SIP,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration
        ELSE dst END) AS PREV_DUR,
        SUBSTRING(dst, 4)
         AS DESTINATION_NUMBER,src AS DARI, 
        recordingfile as RECORDING_FILE,

		(CASE  
          WHEN dcontext = 'from-internal' THEN calldate
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue 	
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN calldate
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN DATE_ADD(datetime_init, INTERVAL (duration_cc) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN duration
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN (duration_cc + duration_wait_cc - 1)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN billsec
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN duration_cc
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN disposition = 'ANSWERED' AND dcontext='from-internal' THEN (duration-billsec)
           WHEN disposition = 'ANSWERED' AND dcontext='ext-queues' THEN (duration_wait_cc - 1)
           WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN (duration-billsec)
           ELSE 0 END) AS DURATION_WAIT,
       (CASE 
        WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log  
        FROM cdr WHERE disposition!='FAILED' AND date(calldate) ='2018-10-03' AND recordingfile !='' AND status_log=0 ";
        //date(calldate) = '2017-12-06' <-- ganti tanggalnya manual kaya gini men,
		//crontabnya matiin dulu
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
        //this->db->close();
       // DATE_ADD(calldate, INTERVAL (duration) SECOND) AS END_CALL_DATE,
//        (CASE  
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
//        WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
//        ELSE calldate END) AS START_DIALING_DATE,
//        (CASE  
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN DATE_ADD(calldate, INTERVAL (duration) SECOND)
//        WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN DATE_ADD(calldate, INTERVAL (duration) SECOND)
//       // ELSE calldate END) AS START_CALL_DATE,
    
        
        }
        
        function get_data_call_in_terminada_t() {
        $sql = "SELECT 
        
        t1.uniqueid AS ID_VALDO,
        (CASE  
        WHEN t1.dcontext = 'from-internal' THEN t1.src
        WHEN t1.dcontext = 'ext-queues' AND t1.lastapp = 'Queue'  THEN SUBSTR(t1.dstchannel,5,3) 
        WHEN t1.dcontext = 'from-internal-xfer' THEN t1.dst
        ELSE t1.dst END) AS SIP,
        (CASE  
        WHEN t1.dcontext = 'from-internal' THEN 0
        WHEN t1.dcontext = 'ext-queues' THEN 0
        WHEN t1.dcontext = 'from-internal-xfer' THEN t1.duration
        ELSE t1.dst END) AS PREV_DUR,
        SUBSTRING(t1.dst, 4)
         AS DESTINATION_NUMBER,t1.src AS DARI, 
        t1.recordingfile as RECORDING_FILE,

		(CASE  
          WHEN t1.dcontext = 'from-internal' THEN t1.calldate
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.date_time_entry_queue 	
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN t1.dcontext = 'from-internal' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration-t1.billsec) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.datetime_init
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  t1.dcontext = 'from-internal' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN DATE_ADD(t1.datetime_init, INTERVAL (t1.duration_cc) SECOND)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN  DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal' THEN t1.duration
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN (t1.duration_cc + t1.duration_wait_cc - 1)
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN ((DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)-DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)))
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal' THEN t1.billsec
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'ext-queues' THEN t1.duration_cc
         WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN ((DATE_ADD(t1.calldate, INTERVAL (t1.duration) SECOND)-DATE_ADD(t2.datetime_init, INTERVAL (t2.duration_cc) SECOND)))
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext='from-internal' THEN 0
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext='ext-queues' THEN 0
           WHEN t1.disposition = 'ANSWERED' AND t1.dcontext = 'from-internal-xfer' THEN 0
           ELSE 0 END) AS DURATION_WAIT,
       (CASE 
        WHEN t1.dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN t1.dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(t1.dcontext = 'from-internal-xfer', t1.src, '') AS TRANSFER_FROM ,
        IF(t1.dcontext = 'from-internal-xfer', t1.dst, '') AS TRANSFER_TO,t1.status_log  
        
       
      
FROM cdr t1
  INNER JOIN 
cdr t2 on t1.recordingfile = t2.recordingfile
WHERE t1.disposition!='FAILED' AND t1.lastapp !='Transferred Call' AND t1.dcontext='from-internal-xfer' AND t2.dcontext = 'ext-queues' AND date(t1.calldate) = CURDATE() AND t1.status_log ='0' AND t2.status_log ='1' AND t1.recordingfile !=''
";
        //date(calldate) = '2017-12-06'
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
        //this->db->close();
       // DATE_ADD(calldate, INTERVAL (duration) SECOND) AS END_CALL_DATE,
//        (CASE  
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
//        WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
//        ELSE calldate END) AS START_DIALING_DATE,
//        (CASE  
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN DATE_ADD(calldate, INTERVAL (duration) SECOND)
//        WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
//        WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN DATE_ADD(calldate, INTERVAL (duration) SECOND)
//       // ELSE calldate END) AS START_CALL_DATE,
    
        
        }
        
        
        function get_data_call_in_terminada_production() {
        $sql = "SELECT 
        uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN src
        WHEN dcontext = 'ext-queues' AND lastapp = 'Queue'  THEN SUBSTR(dstchannel,5,3) 
        WHEN dcontext = 'from-internal-xfer' THEN dst
        ELSE dst END) AS SIP,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration
        ELSE dst END) AS PREV_DUR,
        SUBSTRING(dst, 4)
         AS DESTINATION_NUMBER ,src AS DARI, 
        recordingfile as RECORDING_FILE,
        calldate AS START_DIALING_DATE,
        DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND) AS START_CALL_DATE,
        DATE_ADD(calldate, INTERVAL (duration) SECOND) AS END_CALL_DATE,
        billsec AS DURATION_CALL,duration AS DURATION_DIAL,
       (CASE 
        WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log  
        FROM cdr WHERE disposition!='FAILED' AND lastapp !='Transferred Call' AND date(calldate) = CURDATE() AND recordingfile !=''";
        //date(calldate) = '2017-12-06'
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
        //this->db->close();
    
        
        }
        
        
        function get_data_call_in_terminada_transfer() {
        $sql = "uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration_cc
        ELSE dst END) AS PREV_DUR
        FROM cdr WHERE disposition!='FAILED' AND lastapp !='Transferred Call' AND date(calldate) = CURDATE() AND status_log ='1' AND recordingfile !=''";
        //date(calldate) = '2017-12-06'
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
        $this->db->close();
        //this->db->close();
    
        
        }
        
                

    function get_data_call_in_terminada_inbound() {
        $sql = "SELECT 
        uniqueid AS ID_VALDO,
        (CASE  
        WHEN dcontext = 'from-internal' THEN src
        WHEN dcontext = 'ext-queues' AND lastapp = 'Queue'  THEN SUBSTR(dstchannel,5,3) 
        WHEN dcontext = 'from-internal-xfer' THEN dst
        ELSE dst END) AS SIP,
        (CASE  
        WHEN dcontext = 'from-internal' THEN 0
        WHEN dcontext = 'ext-queues' THEN 0
        WHEN dcontext = 'from-internal-xfer' THEN duration
        ELSE dst END) AS PREV_DUR,
        SUBSTRING(dst, 4)
         AS DESTINATION_NUMBER,src AS DARI, 
        recordingfile as RECORDING_FILE,

		(CASE  
          WHEN dcontext = 'from-internal' THEN calldate
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN date_time_entry_queue 	
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN calldate
        ELSE 0 END) AS START_DIALING_DATE
        ,
		(CASE  
          WHEN dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN datetime_init
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND)
        ELSE 0 END) AS START_CALL_DATE
        ,
		(CASE  
          WHEN  dcontext = 'from-internal' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN DATE_ADD(datetime_init, INTERVAL (duration_cc) SECOND)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN  DATE_ADD(calldate, INTERVAL (duration) SECOND)
        ELSE 0 END) AS END_CALL_DATE
        ,
         (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN duration
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN (duration_cc + duration_wait_cc)
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS DURATION_DIAL
        ,
        (CASE  
          WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal' THEN billsec
         WHEN disposition = 'ANSWERED' AND dcontext = 'ext-queues' THEN duration_cc
         WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN billsec
        ELSE 0 END) AS  DURATION_CALL
        ,
         (CASE  
           WHEN disposition = 'ANSWERED' AND dcontext='from-internal' THEN (duration-billsec)
           WHEN disposition = 'ANSWERED' AND dcontext='ext-queues' THEN (duration_wait_cc)
           WHEN disposition = 'ANSWERED' AND dcontext = 'from-internal-xfer' THEN (duration-billsec)
           ELSE 0 END) AS DURATION_WAIT,
       (CASE 
        WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  
        WHEN dcontext = 'ext-queues' THEN  'INBOUND'
        ELSE 'TRANSFER' END) AS STATUS,
        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log  
        FROM cdr WHERE disposition!='FAILED' AND lastapp !='Transferred Call' AND dcontext='ext-queues' AND dcontext!='from-internal-xfer' AND date(calldate) = CURDATE()AND uniqueid='1520992782.6823'  ";
        //date(calldate) = '2017-12-06'
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //this->db->close();
        //
        $this->db->close();
    }
    
//    function log_cti() {
//        $sql = "SELECT 
//        uniqueid AS ID_VALDO, SUBSTR(channel,5,3) AS SIP, dst AS DESTINATION_NUMBER,src AS DARI, 
//        recordingfile as RECORDING_FILE,
//        calldate AS START_DIALING_DATE,
//        DATE_ADD(calldate, INTERVAL (duration-billsec) SECOND) AS START_CALL_DATE,
//        DATE_ADD(calldate, INTERVAL (duration) SECOND) AS END_CALL_DATE,
//        billsec AS DURATION_CALL,duration AS DURATION_DIAL,
//       (CASE WHEN dcontext = 'from-internal' THEN 'OUTBOUND'  WHEN dcontext = 'ext-queues' THEN  'INBOUND'
//        ELSE 'TRANSFER' END) AS STATUS,
//        IF(dcontext = 'from-internal-xfer', src, '') AS TRANSFER_FROM ,
//        IF(dcontext = 'from-internal-xfer', dst, '') AS TRANSFER_TO,status_log  
//        FROM cdr WHERE recordingfile !=''";
//        //date(calldate) = '2017-12-06'
//        $query = $this->db->query($sql);
//        $data = array();
//        if ($query !== FALSE && $query->num_rows() > 0) {
//            $data = $query->result_array();
//        }
//        return $data;
//        //this->db->close();
//        
//        
//    }

    function get_data_endcall($id) {
        $sql = 'SELECT * FROM cdr WHERE uniqueid =' . $id;
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }
    
    function getallcdr() {
        $sql = 'SELECT * FROM cdr limit 2';
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

    function get_data() {
        $sql = "SELECT SUBSTR(channel,5,3) as sip__c, src as From__c, status_log,uniqueid  FROM cdr WHERE status_log = 0 AND LENGTH(src) < 4";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
    }

    function sip_dup($sip) {
        $sql = "SELECT * FROM agent WHERE sip_no='" . $sip . "'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
    }

    function get_data_agent($id_agent) {
        $sql = "SELECT * FROM agent WHERE id_agent ='" . $id_agent . "'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
    }

    function get_data_user($sip) {
        $sql = "SELECT * FROM agent WHERE sip ='" . $sip . "'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
    }

    function session_data($sip, $password) {
        $sql = "SELECT * FROM agent WHERE sip_no ='" . $sip . "' AND password ='" . $password . "'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        //
    }

    function validate($sip) {
        $hasil = $this->session_data($sip);
        $total = count($hasil);
        for ($i = 0; $i < $total; $i++) {
            if ($total > 0) {
                $data = array(
                    'id_agent' => $hasil[$i]['id_agent'],
                    'username' => $hasil[$i]['username'],
                    'SIP' => $hasil[$i]['SIP'],
                    'queue' => $hasil[$i]['queue'],
                    'email' => $hasil[$i]['email'],
                    'is_logged_in' => TRUE
                );
                $this->session->set_userdata($data);
                return true;
            }
        }
    }
}