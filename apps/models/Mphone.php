<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mphone extends CI_Model {

    var $table = 'agent';
    var $column_order = array('sip_no','role','active','username',   null);
    var $column_search = array('sip_no','role','active','username');
    var $order = array('id_agent' => 'desc');

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        $this->db->from($this->table);
        $this->db->where_not_in('sip_no', array('8888','823','833','801','802','803','804','701','702','703','704','816','817','818','819'));
        //$this->db->where('sip_no !=', $this->session->userdata('sip_no'));
        $this->db->order_by("sip_no","ASC");
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } 
                else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->close();
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);   
        $query = $this->db->get();
        return $query->result();
        $this->db->close();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
        $this->db->close();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
        $this->db->close();
    }

    public function get_by_id($id) {
        $this->db->from($this->table);
        $this->db->where('id_agent', $id);
        $query = $this->db->get();
        return $query->row();
        $this->db->close();
    }

    public function save($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
        $this->db->close();
    }

    public function update($where, $data) {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
        $this->db->close();
    }
    
    public function update_hold($where, $data) {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
        $this->db->close();
    }

    public function delete_by_id($id) {
        $this->db->where('id_agent', $id);
        $this->db->delete($this->table);
        $this->db->close();
    }
    
    function get_status_aux($id) {
        $sql = "SELECT * FROM agent WHERE id_agent ='".$id."'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        $this->db->close();
    }
    
   function user($sip) {
        $sql = 'SELECT * FROM agent WHERE sip_no = "' . $sip . '"';
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        $this->db->close();
    }
    
    function input_daily_aux($agent_id,$aux_time) {
        $sql = "INSERT INTO agent_aux_counter (sip_no, aux_time) VALUES('".$agent_id."', '".$aux_time."')";
        $this->db->query($sql);
        return TRUE;
        $this->db->close();
    }
    
    function validate($sip) {
        $hasil = $this->user($sip);
        $total = count($hasil);
        for ($i = 0; $i < $total; $i++) {
            if ($total > 0) {
                $data = array(
                    'id_agent' => $hasil[$i]['id_agent'],
                    'username' => $hasil[$i]['username'],
                    'sip_no' => $hasil[$i]['sip_no'],
                    'queue' => $hasil[$i]['queue'],
                    'email' => $hasil[$i]['email'],
                    'is_logged_in' => true
                );
                $this->session->set_userdata($data);
                return true;
            }
        }
    }
    function logoffallbycron() {
        $sql = "UPDATE agent SET active='0' WHERE 1";
        $this->db->query($sql);
        return TRUE;
        $this->db->close();
    }
    function get_agent_sip() {
        $sql = "SELECT sip_no,active,role FROM agent ORDER BY active DESC";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        $this->db->close();
    }
     function get_agent_sip_online() {
        $sql = "SELECT sip_no FROM agent WHERE active ='3'";
        $query = $this->db->query($sql);
        $data = array();
        if ($query !== FALSE && $query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
        $this->db->close();
    }
}