<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/Diskstatus.php';
class Monitoring extends CI_Controller {

    
    function __construct() {
        parent:: __construct();
        $this->load->library(array('asmanager', 'session', 'form_validation'));
        $this->load->model(array('mphone', 'm_cstmr'));
    }

    function index() {
        
        $is_logged_in = $this->session->userdata('is_logged_in');
            if (!isset($is_logged_in) || $is_logged_in != true) {
                redirect('asterisk');
            }
        $a = array();
        //$a['sip']= $this->mphone->get_agent_sip();
//        $listv= $this->mphone->get_agent_sip();
        $a['html']['css'] = add_css('/monitoring/bootstrap.min.css');
        $a['html']['css'] .= add_css('/monitoring/datatable.bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['js'] = add_js('/monitoring/jquery.js');
        $a['html']['js'] .= add_js('/monitoring/datatable.bootstrap.js');
        $a['html']['js'] .= add_js('/monitoring/jquery.datatable.js');
        
        if ($this->asmanager->connect()) {
            $asresponse = $this->asmanager->command('sip show inuse');
            $asdata = $asresponse['data'];
            $a['list'] = explode("\n", $asdata);
            for ($z = 0; $z < count($a['list']); $z++) {
                $arr = explode(' ', trim($a['list'][$z]));

                // if(substr($arr[23], 0, 1)==1){
                $si[] = array(
                    'sip_no' => $arr[0],
                    'active' => $arr[23]);
//            / }
//            for($z=0;$z < count($si);$z++)
//            {
//                if(substr($arr[23], 0, 1)==1)
//                {$active='3';}
//                $active='1';
//                $this->mphone->update(array('sip_no' => $si[$z]['sip_no']), array('active'=>$active));
//            }
            //var_dump($arr[0]);
            //var_dump(substr($arr[23], 0, 1));
            }
            date_default_timezone_set('asia/jakarta');
            for ($z = 0; $z < count($si); $z++) {
               // echo $si[$z]['sip_no'].'-'.$si[$z]['active'].'<br>';
                $cek = $this->mphone->user($si[$z]['sip_no']);
                if ($cek[0]['active'] == 1 OR $cek[0]['active'] == 3) {
                    if (substr($si[$z]['active'], 0, 1) == 0) {
                        $active = '1';
                    } else if (substr($si[$z]['active'], 0, 1) == 1){
                        $active = '3';
                    }
                    if($cek[0]['active'] != $active){$this->mphone->update(array('sip_no' => $si[$z]['sip_no']), array('active' => $active,'time' =>  date('Y-m-d H:i:s') ));}
                    if($cek[0]['onhold'] != substr($si[$z]['active'], 4, 1)){$this->mphone->update_hold(array('sip_no' => $si[$z]['sip_no']), array('onhold'=>substr($si[$z]['active'], 4, 1),'hold_time' =>  date('Y-m-d H:i:s')));}
                }
            }
            $a['sip'] = $this->mphone->get_agent_sip();
            
            $coreshowchannel = $this->asmanager->command("core show channels");
            $queueshow = $this->asmanager->command("queue show");
            $concise= $this->asmanager->command("core show channels concise");
            $showsipinuse= $this->asmanager->command(" sip show inuse");
            $a['coreshowchannel'] =explode("\n", $coreshowchannel['data']);
            $a['queueshow'] =explode("\n", $queueshow['data']);
            $a['coreshowconcise'] =explode("\n", $concise['data']);
            $a['showsipinuse'] =explode("\n", $showsipinuse['data']);
            
             try {
                $diskStatus = new DiskStatus('/');

                $a['freeSpace'] = $diskStatus->freeSpace();
                $a['totalSpace'] = $diskStatus->totalSpace();
                $a['barWidth'] = ($diskStatus->usedSpace()/100) * 560;
                $a['used']= $a['totalSpace']-$a['freeSpace'];
              } catch (Exception $e) {
                    echo 'Error ('.$e->getMessage().')';
                    exit();
             }
            
            $this->load->view('monitoring_axa', $a);
        }
    }

    function monitoring_new()
    {
         $a = array();
        //$a['sip']= $this->mphone->get_agent_sip();
//        $listv= $this->mphone->get_agent_sip();
        $a['html']['css'] = add_css('/monitoring/bootstrap.min.css');
        $a['html']['css'] .= add_css('/monitoring/datatable.bootstrap.min.css');
        $a['html']['css'] .= add_css('/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['js'] = add_js('/monitoring/jquery.js');
        $a['html']['js'] .= add_js('/monitoring/datatable.bootstrap.js');
        $a['html']['js'] .= add_js('/monitoring/jquery.datatable.js');
        
        if ($this->asmanager->connect()) {
            $asresponse = $this->asmanager->command('sip show inuse');
            $asdata = $asresponse['data'];
            $a['list'] = explode("\n", $asdata);
            for ($z = 0; $z < count($a['list']); $z++) {
                $arr = explode(' ', trim($a['list'][$z]));

                // if(substr($arr[23], 0, 1)==1){
                $si[] = array(
                    'sip_no' => $arr[0],
                    'active' => $arr[23],
                    'onhold' => $arr[27]
                        );
//            / }
//            for($z=0;$z < count($si);$z++)
//            {
//                if(substr($arr[23], 0, 1)==1)
//                {$active='3';}
//                $active='1';
//                $this->mphone->update(array('sip_no' => $si[$z]['sip_no']), array('active'=>$active));
//            }
                //var_dump($arr[0]);
                //var_dump(substr($arr[23], 0, 1));
            }
            
            
            for ($z = 0; $z < count($si); $z++) {
                var_dump(substr($si[$z]['active'], 4, 1));
            echo 'debug';
               // echo $si[$z]['sip_no'].'-'.$si[$z]['active'].'<br>';
                $cek = $this->mphone->user($si[$z]['sip_no']);
                if ($cek[0]['active'] == 1 OR $cek[0]['active'] == 3) {
                    if (substr($si[$z]['active'], 0, 1) == 0) {
                        $active = '1';
                    } else if (substr($si[$z]['active'], 0, 1) == 1){
                        $active = '3';
                    }
                    $this->mphone->update(
                            array('sip_no' => $si[$z]['sip_no']), 
                            array('active' => $active,
                                   'onhold'=> substr($si[$z]['onhold'], 0, 1)));
                }
            }
            $a['sip'] = $this->mphone->get_agent_sip();
    
            $this->load->view('monitoring', $a);
        }
    }

    public function ajax_list()
	{
//                 for ($z = 0; $z < count($a['list']); $z++) {
//                $arr = explode(' ', trim($a['list'][$z]));
//                $si[] = array(
//                    'sip_no' => $arr[0],
//                    'active' => $arr[23]);
//            }
//
//            for ($z = 0; $z < count($si); $z++) {
//               // echo $si[$z]['sip_no'].'-'.$si[$z]['active'].'<br>';
//                $cek = $this->mphone->user($si[$z]['sip_no']);
//                if ($cek[0]['active'] == 1 OR $cek[0]['active'] == 3) {
//                    if (substr($si[$z]['active'], 0, 1) == 0) {
//                        $active = '1';
//                    } else if (substr($si[$z]['active'], 0, 1) == 1){
//                        $active = '3';
//                    }
//                    $this->mphone->update(array('sip_no' => $si[$z]['sip_no']), array('active' => $active));
//                }
//            }
                $list = $this->mphone->get_datatables();
                date_default_timezone_set('Asia/Jakarta');
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $admin) {
                    if ($admin->active == 0) {
                        $status = '<span class="label label-danger">OFFLINE</span>';
                        $du ='';
                        $h ='';
                        $d = 'disabled';
                        $whis='---------'; 
                        $spy ='---------';
                        $hold ='';
                    }
                  else if ($admin->active == 1) {
                        $status = '<span class="label label-primary">IDLE</span>';
                        $d = 'disabled';
                       $datetime1 = strtotime($admin->time);
                        $datetime2 = strtotime(date("Y-m-d H:i:s"));
                        $interval  = $datetime2 - $datetime1;
                        $du = '<span class="label label-default">'.gmdate('H:i:s', $interval).'</span>';
                        $h ='';
                        $whis='---------'; 
                        $spy ='---------';
                        $hold ='';
                    } 
                    

                    else if ($admin->active == 2) {
                        $status = '<span class="label label-warning">AUX</span>';
                        $d = 'disabled';
                        $h ='';
                        $datetime1 = strtotime($admin->time);
                        $datetime2 = strtotime(date("Y-m-d H:i:s"));
                        $interval  = $datetime2 - $datetime1;
                        $du = '<span class="label label-warning">'.gmdate('H:i:s', $interval).'</span>';
                        $whis='---------'; 
                        $spy ='---------';
                        $hold ='';
                    }
                     else if ($admin->active == 3) {
                        
                        if($admin->onhold == 1){
                            $s=  base_url().'monitoring/agentspy?ext=';
                            $w= base_url().'monitoring/agentwhisper?ext=';
                            $h ='<span class="label label-warning">HOLD</span>';
                            $spy = 'spy('."'".$s.$admin->sip_no."'".')';
                            $whis = 'spy('."'".$w.$admin->sip_no."'".')';
                            $datetime1 = strtotime($admin->time);
                            $datetime2 = strtotime(date("Y-m-d H:i:s"));
                            $interval  = $datetime2 - $datetime1;
                             if($interval>1200){$class='class="label label-warning"';}else{$class='class="label label-success"';}
                             $du = '<span '.$class.'>'.gmdate('H:i:s', $interval).'</span>';
                             $hold = '<span class="label label-warning">HOLD</span>';
                            $d = '';
                            $status = '<span class="label label-success">ONLINE</span>';
                        }
                        else{$s=  base_url().'monitoring/agentspy?ext=';
                            $w= base_url().'monitoring/agentwhisper?ext=';
                            $h ='';
                            $spy = 'spy('."'".$s.$admin->sip_no."'".')';
                            $whis = 'spy('."'".$w.$admin->sip_no."'".')';
                            $datetime1 = strtotime($admin->time);
                            $datetime2 = strtotime(date("Y-m-d H:i:s"));
                            $interval  = $datetime2 - $datetime1;
                            if($interval>1200)
                            {$class='class="label label-warning"';}else{$class='class="label label-success"';}
                             $du = '<span '.$class.'>'.gmdate('H:i:s', $interval).'</span>';
                             $hold ='';
                            $d = '';
                            $status = '<span class="label label-success">ONLINE</span>';}
                        
                    }
			$no++;
			$row = array();
                        $row[] = $no;
			$row[] = $admin->sip_no;
                        $row[] ='<span>'.$admin->username.'</span>';
			$row[] = $admin->role;
                        $row[] = $status;
                        $row[] = $du.' '.$hold;
			
			$row[] = '<a '.$d.' type="button" data-toggle="tooltip" href="javascript:void(0)" class="btn btn-success" onclick="'.$spy.'">Spying</a>
                                  <a '.$d.' type="button" data-toggle="tooltip" href="javascript:void(0)" class="btn btn-info" onclick="'.$whis.'")">Whisper</a>
                            ';  
			$data[] = $row;
		}
		$output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mphone->count_all(),
                "recordsFiltered" => $this->mphone->count_filtered(),
                "data" => $data,
				);
		echo json_encode($output);
		
	}

    function agentspy() {

        //$ext = $_GET['ext'];
        chanspy($this->session->userdata('sip_no'), $_GET['ext']);
        //$interface = 'SIP/' . $ext;
//        if ($this->asmanager->connect()) {
//            $this->asmanager->Events('off');
//            $peer = $this->asmanager->Originate('SIP/' . $this->session->userdata('sip_no'), NULL, NULL, NULL, 'ChanSpy', 'SIP/' . $ext . ',q', NULL, "SPYING AGENT");
//            $status = $peer['Response'];
//            if ($status == 'Success') {
//                $activity = "SPY AGENT OK";
//                $notify = "Spy Agent : [" . $_GET['ext'] . "] Success, Pick Up Headset. Hangup After Listening";
//            } else {
//                $activity = "SPY AGENT ERROR";
//                $error_msg = $peer['Message'];
//                $notify = "Failed Spy Agent : [$error_msg" . "] Make Sure Ext $ext Is Online !!!";
//            }
//			$data1['agent_id'] = $id;
//			$data1['interface'] = $interface;
//			$data1['queue'] = $this->queueAsterisk;
//			$data1['activity'] = $activity;
//			$data1['data'] = 'Ext Supervisor : '.$ext;
            //$this->db = $this->load->database('asteriskcdr',true);
            //$this->master->simpan('agent_activity',$data1);
          //  echo $notify;
     //   }
    }

    function agentwhisper() {
        //$data = $this->allfunct->securePost();
        //$ext = $_GET['ext'];
        whispering($this->session->userdata('sip_no'), $_GET['ext']);
        //$id = $data['id'];
//        $interface = 'SIP/' . $ext;
//        if ($this->asmanager->connect()) {
//            $this->asmanager->Events('off');
//            $peer = $this->asmanager->Originate('SIP/' . $this->session->userdata('sip_no'), NULL, NULL, NULL, 'ChanSpy', 'SIP/' . $ext . ",wq", NULL, "WHISPERING AGENT");
//            $status = $peer['Response'];
//            if ($status == 'Success') {
//                $activity = "WHISPER AGENT OK";
//                $notify = "Whisper Agent : [" . $ext . "] Success, Pick Up Headset And Speak. Hangup After Listening";
//            } else {
//                $activity = "WHISPER AGENT ERROR";
//                $error_msg = $peer['Message'];
//                $notify = "Failed Whisper Agent : [$error_msg" . "] Make Sure Ext $ext Is Online !!!";
//            }
////			$data1['agent_id'] = $id;
////			$data1['interface'] = $interface;
////			$data1['queue'] = $this->queueAsterisk;
////			$data1['activity'] = $activity;
////			$data1['data'] = 'Ext Supervisor : ADMIN';
////			$this->db = $this->load->database('asteriskcdr',true);
////			$this->master->simpan('agent_activity',$data1);
//            echo $notify;
//        }
    }

}
