<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	require(APPPATH."/third_party/PHPExcel.php");
class Report_tele extends CI_Controller {

    
    function __construct() {
        parent:: __construct();
        $this->load->library(array('asmanager', 'session', 'form_validation'));
        $this->load->model(array('mphone', 'm_cstmr'));
    }

    function index() {
        
        $a = array();
         $a['html']['css'] = add_css('/assets/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('/assets/bootstrap/css/font-awesome.min.css');
        $a['html']['css'] .= add_css('/assets/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/assets/plugins/daterangepicker/daterangepicker-bs3.css');
        $a['html']['css'] .= add_css('/assets/plugins/datepicker/datepicker3.css');
        $a['html']['css'] .= add_css('/assets/plugins/colorpicker/bootstrap-colorpicker.min.css');
        $a['html']['css'] .= add_css('/assets/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['css'] .= add_css('/assets/plugins/datatables/dataTables.bootstrap.css');
        $a['html']['css'] .= add_css('/assets/plugins/select2/select2.min.css');
        $a['html']['css'] .= add_css('/assets/dist/css/AdminLTE.css');
        $a['html']['css'] .= add_css('/assets/dist/css/skins/_all-skins.min.css');

        $a['html']['js'] = add_js('/assets/plugins/jQuery/jQuery-2.2.0.min.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/jquery-ui.min.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/bootstrap.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/select2/select2.full.min.js');

        $a['html']['js'] .= add_js('/assets/plugins/input-mask/jquery.inputmask.js');
        $a['html']['js'] .= add_js('/assets/plugins/input-mask/jquery.inputmask.date.extensions.js');
        $a['html']['js'] .= add_js('/assets/plugins/input-mask/jquery.inputmask.extensions.js');
        $a['html']['js'] .= add_js('/assets/plugins/datatables/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/moment.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/datatables/dataTables.bootstrap.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/slimScroll/jquery.slimscroll.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/assets/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/assets/dist/js/demo.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/bootstrap-confirm-delete.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/test.js');
        $a['html']['js'] .= add_js('/assets/plugins/datepicker/bootstrap-datepicker.js');
        $a['html']['js'] .= add_js('/assets/plugins/daterangepicker/daterangepicker.js');
        $a['html']['js'] .= add_js('/assets/plugins/colorpicker/bootstrap-colorpicker.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/timepicker/bootstrap-timepicker.min.js');
        
        $x = $_GET['range'];
        $tang = explode("-",preg_replace('/\s/', '', $x));
        
        $date1 = date('Y-m-d', strtotime($tang[0]));
        $date2 = date('Y-m-d', strtotime($tang[1]));
        
        //echo $date1;
       // echo '<br>';
       // echo $date2;
        $this->load->view('report_tele', $a);
    }

    function report_telesales()
    {
		$tgl1 = $_GET['range'];
		$tang = explode("-",preg_replace('/\s/', '', $tgl1));
		$date1 = date('Y-m-d', strtotime($tang[0]));
		$date2 = date('Y-m-d', strtotime($tang[1]));
		
		$data = array();
		$cRet = '<b>REPORT OUTBOUND</b><br/><br/>';
		$cRet .= 'Date : '.date("M d, Y").'<br/>';
		$cRet .= 'Start Time : '.$date1.' - End Time '.$date2.'<br/><br/>';
		$cRet .= '<table border="1" cellpadding="2" cellspacing="4">';
    	$cRet .= '<thead>';
    	$cRet .= '<tr>';
    	$cRet .= '<th>No</th>';
		$cRet .= '<th>Date</th>';
		$cRet .= '<th>Duration</th>';
    	$cRet .= '<th>Agent Name</th>';
		$cRet .= '<th>Dialed From</th>';
    	$cRet .= '<th>Dialed To</th>';
    	$cRet .= '</tr>';
    	$cRet .= '</thead>';
	
		$query = $this->db->query("SELECT (
								CASE
								WHEN dcontext = 'from-internal'
								THEN DATE_ADD( calldate, INTERVAL( duration - billsec )
								SECOND )
								ELSE 0
								END
								) AS DATE, (

								CASE
								WHEN disposition = 'ANSWERED'
								AND dcontext = 'from-internal'
								THEN billsec
								ELSE 0
								END
								) AS DURATION, username AS AGENT_NAME, src AS DIALED_FROM, concat( '  ', substr( dst, 4, 20 ) ) AS DIALED_TO
								FROM cdr a
								INNER JOIN agent b ON a.src = b.sip_no
								WHERE dcontext = 'from-internal'
								AND date( calldate )
								BETWEEN '$date1' AND '$date2'");
		
		$data = $query->result_array();
		$idx = 0;
		$nr = 1;
		foreach ($data AS $val){
			$tanggal	= $val['DATE'];
			$durasi		= $val['DURATION'];
			
			$timeFormat = gmdate("H:i:s", $durasi);
			
			$agent		= $val['AGENT_NAME'];
			$dari		= $val['DIALED_FROM'];
			$ke_no		= $val['DIALED_TO'];
			
			$cRet .= '<tr>';
	      	$cRet .= '<td>' . $nr . '</td>';
	      	$cRet .= '<td>' . $tanggal . '</td>';
			$cRet .= '<td>' . $timeFormat . '</td>';
			$cRet .= '<td>' . $agent . '</td>';
			$cRet .= '<td>' . $dari . '</td>';
			$cRet .= '<td>' . $ke_no . '&nbsp;</td>';
	      	$cRet .= '</tr>';
			
			$nr++;
			$idx++;
		}
		
		$cRet .= '</tbody>';
    	$cRet .= '</table>';
		
		$addFile = date('Ymd');

		$filename = "Outbound_$addFile.xls";
		$mime_type = "application/x-msexcel;charset=utf-8";

	    header('Content-Type: ' . $mime_type);
	    header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	    header('Content-Disposition: attachment; filename="' . $filename . '"');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
    	
    	die($cRet);
    }
	
	function dir_recording(){
		$dir = "/var/spool/asterisk/monitor/2018/03/12";
		//echo $dir;
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					//echo $file."<br />";
					$line = explode("\n", $file);
					foreach($line as $ln){
						//var_dump($line);
						$recording	=	$ln;
					}
					$this->db->query("INSERT INTO table_testis VALUES('$recording')");
					echo $this->db->last_query();
				}
				closedir($dh);
			}
		}
	}
	
	function bool_monyet(){
		$query = $this->db->query("SELECT * from table_testis WHERE left( `file_recording` , 1 ) = 'q' and file_recording like '%1520816845.212%'");
		
		$data = $query->result_array();
		//var_dump($data);
		//return false;
		foreach ($data AS $val){
			
			$cols		= $val['file_recording'];
			$colslai 	= explode("-", $cols);
			$status 	= $colslai[0];
			$dst 		= $colslai[1];
			$src 		= $colslai[2];
			$tanggal 	= $colslai[3];
			$tahun		= substr($tanggal, 0, 4);
			$bulan		= substr($tanggal, 4, 2);
			$hari		= substr($tanggal, 6, 2);
			$con_tgl	= $tahun.'-'.$bulan.'-'.$hari;
			$time 		= $colslai[4];
			$jam		= substr($time, 0, 2);
			$menit		= substr($time, 3, 2);
			$detik		= substr($time, 4, 2);
			
			$con_time	= $jam.":".$menit.":".$detik;
			$dateTime	= $con_tgl.' '.$con_time;
			
			var_dump($dateTime);
			return false;
			
			$unique		= $colslai[5];
			$uniqueid	= substr($unique, 0, -4);
			
 			if($status=='out'){
				$dcontext 	= 'from-internal';
				$lastapp 	= 'Dial';
				$clid		= $src;
			}else if($status=='q'){
				$dcontext 	= 'ext-queues';
				$lastapp 	= 'Queue';
				//$clid		= "'".$src."'<".$src.">";
				$clid		= $src;
			}
			
			/* var_dump($clid);
			return false; */
			$this->db->query("INSERT INTO cdr_test(calldate,clid,src,dst,dcontext,lastapp,amaflags,uniqueid,recordingfile) VALUES('$dateTime','$clid','$src','$dst','$dcontext','$lastapp','3','$uniqueid','$cols')");
			echo $this->db->last_query();
			
		}
	}

}
