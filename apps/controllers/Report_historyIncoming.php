<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	require(APPPATH."/third_party/PHPExcel.php");
class Report_historyIncoming extends CI_Controller {

    
    function __construct() {
        parent:: __construct();
        $this->load->library(array('asmanager', 'session', 'form_validation'));
        $this->load->model(array('mphone', 'm_cstmr'));
    }

    function index() {
        
        $a = array();
         $a['html']['css'] = add_css('/assets/bootstrap/css/bootstrap.css');
        $a['html']['css'] .= add_css('/assets/bootstrap/css/font-awesome.min.css');
        $a['html']['css'] .= add_css('/assets/bootstrap/css/ionicons.min.css');
        $a['html']['css'] .= add_css('/assets/plugins/daterangepicker/daterangepicker-bs3.css');
        $a['html']['css'] .= add_css('/assets/plugins/datepicker/datepicker3.css');
        $a['html']['css'] .= add_css('/assets/plugins/colorpicker/bootstrap-colorpicker.min.css');
        $a['html']['css'] .= add_css('/assets/bootstrap/css/bootstrap-datepicker3.min.css');
        $a['html']['css'] .= add_css('/assets/plugins/datatables/dataTables.bootstrap.css');
        $a['html']['css'] .= add_css('/assets/plugins/select2/select2.min.css');
        $a['html']['css'] .= add_css('/assets/dist/css/AdminLTE.css');
        $a['html']['css'] .= add_css('/assets/dist/css/skins/_all-skins.min.css');

        $a['html']['js'] = add_js('/assets/plugins/jQuery/jQuery-2.2.0.min.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/jquery-ui.min.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/bootstrap.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/select2/select2.full.min.js');

        $a['html']['js'] .= add_js('/assets/plugins/input-mask/jquery.inputmask.js');
        $a['html']['js'] .= add_js('/assets/plugins/input-mask/jquery.inputmask.date.extensions.js');
        $a['html']['js'] .= add_js('/assets/plugins/input-mask/jquery.inputmask.extensions.js');
        $a['html']['js'] .= add_js('/assets/plugins/datatables/jquery.dataTables.min.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/moment.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/datatables/dataTables.bootstrap.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/slimScroll/jquery.slimscroll.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/fastclick/fastclick.js');
        $a['html']['js'] .= add_js('/assets/dist/js/app.min.js');
        $a['html']['js'] .= add_js('/assets/dist/js/demo.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/bootstrap-confirm-delete.js');
        $a['html']['js'] .= add_js('/assets/bootstrap/js/test.js');
        $a['html']['js'] .= add_js('/assets/plugins/datepicker/bootstrap-datepicker.js');
        $a['html']['js'] .= add_js('/assets/plugins/daterangepicker/daterangepicker.js');
        $a['html']['js'] .= add_js('/assets/plugins/colorpicker/bootstrap-colorpicker.min.js');
        $a['html']['js'] .= add_js('/assets/plugins/timepicker/bootstrap-timepicker.min.js');
        
        $x = $_GET['range'];
        $tang = explode("-",preg_replace('/\s/', '', $x));
        
        $date1 = date('Y-m-d', strtotime($tang[0]));
        $date2 = date('Y-m-d', strtotime($tang[1]));
        
        //echo $date1;
       // echo '<br>';
       // echo $date2;
        $this->load->view('report_history', $a);
    }

    function report_historyCall()
    {
		$tgl1 = $_GET['range'];
		$tang = explode("-",preg_replace('/\s/', '', $tgl1));
		$date1 = date('Y-m-d', strtotime($tang[0]));
		$date2 = date('Y-m-d', strtotime($tang[1]));
		
		$data = array();
		$cRet = '<b>REPORT History Call Incoming</b><br/><br/>';
		$cRet .= 'Date : '.date("M d, Y").'<br/>';
		$cRet .= 'Start Time : '.$date1.' - End Time '.$date2.'<br/><br/>';
		$cRet .= '<table border="1" cellpadding="2" cellspacing="4">';
    	$cRet .= '<thead>';
    	$cRet .= '<tr>';
    	$cRet .= '<th>No</th>';
		$cRet .= '<th>Date</th>';
		$cRet .= '<th>Call Entry</th>';
    	$cRet .= '<th>Call End</th>';
		$cRet .= '<th>Phone Number</th>';
    	$cRet .= '<th>Agent</th>';
    	$cRet .= '</tr>';
    	$cRet .= '</thead>';
	
		$query = $this->db->query(" SELECT date( calldate ) AS date, time( datetime_init ) AS Call_Entry, time( datetime_end ) AS Call_End, IF( left( src, 1 ) = '8', concat( '0', src ) , src ) AS Phone_Number, b.username AS Agent, duration_cc AS Duration
		FROM `cdr` a
		INNER JOIN agent b ON a.id_user = b.id_agent
		WHERE lastapp = 'Queue' AND dstchannel != '' AND date( calldate ) BETWEEN '$date1' AND '$date2'
		ORDER BY calldate ");
		
		$data = $query->result_array();
		$idx = 0;
		$nr = 1;
		foreach ($data AS $val){
			$tanggal		= $val['date'];
			$durasi			= $val['Duration'];
			
			$timeFormat 	= gmdate("H:i:s", $durasi);
			
			$Agent			= $val['Agent'];
			$Call_Entry		= $val['Call_Entry'];
			$Call_End		= $val['Call_End'];
			$Phone_Number	= $val['Phone_Number'];
			
			$cRet .= '<tr>';
	      	$cRet .= '<td>' . $nr . '</td>';
	      	$cRet .= '<td>' . $tanggal . '</td>';
			$cRet .= '<td>' . $Call_Entry . '</td>';
			$cRet .= '<td>' . $Call_End . '</td>';
			$cRet .= '<td>' . $Phone_Number . '</td>';
			$cRet .= '<td>' . $Agent . '</td>';
			$cRet .= '<td>' . $timeFormat . '&nbsp;</td>';
	      	$cRet .= '</tr>';
			
			$nr++;
			$idx++;
		}
		
		$cRet .= '</tbody>';
    	$cRet .= '</table>';
		
		$addFile = date('Ymd');

		$filename = "Outbound_$addFile.xls";
		$mime_type = "application/x-msexcel;charset=utf-8";

	    header('Content-Type: ' . $mime_type);
	    header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	    header('Content-Disposition: attachment; filename="' . $filename . '"');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
    	
    	die($cRet);
    }
}
