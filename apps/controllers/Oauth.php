<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Oauth extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->library(array('asmanager', 'session', 'form_validation'));

        $this->load->model(array('mphone', 'm_cstmr'));
    }

    function index() {

        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');

        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            //$this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('sip_no', 'sip', 'trim|required|callback_ceklogin');

            if ($this->form_validation->run() == TRUE) {
                redirect('oauth');
            }
            //echo json_encode(array("status" => TRUE));
            //$this->load->view('phone', $a, FALSE);
        } else {
            $id = $this->session->userdata('id_agent');
            $a['aux'] = $this->mphone->get_status_aux($id);
            //var_dump($a['aux']);
        } $this->load->view('phone', $a, FALSE);
    }

    private function _validate() {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('sip_no') == '') {
            $data['inputerror'][] = 'sip_no';
            $data['error_string'][] = '';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

    function ceklogin() {
        $this->_validate();
        $this->load->model('m_cstmr');
        $sip = addslashes($this->input->post('sip_no'));
        //$pass = $this->input->post('email');
        $data = $this->mphone->validate($sip);
        $jum = count($data);
        if ($jum == 0) {
            $this->form_validation->set_message('ceklogin', 'SIP not found');
            $act = array(
                'id_agent' => $this->session->userdata('id_agent'),
                'interface' => $this->session->userdata('sip_no'),
                'queue' => $this->session->userdata('queue'),
                'data' => 'By Supervisor',
                'activity' => 'AGENT LOGIN ERROR'
            );
            $this->db->insert('agent_activity', $act);
            echo json_encode(array("status" => TRUE));
            return FALSE;
        } else {
            $sip = $this->session->userdata('sip_no');
            $queue = $this->session->userdata('queue');
            $id_agent = $this->session->userdata('id_agent');
            $username = $this->session->userdata('sip_no');
            $act = array(
                'id_agent' => $this->session->userdata('id_agent'),
                'interface' => 'SIP/' . $this->session->userdata('sip_no'),
                'queue' => $this->session->userdata('queue'),
                'data' => 'By Supervisor',
                'activity' => 'AGENT LOGIN OK'
            );
            $this->db->insert('agent_activity', $act);
            $this->loginasterisk($sip, $queue, $id_agent, $username);
            echo json_encode(array("status" => TRUE));
            return TRUE;
        }
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('oauth');
        }
    }

    function loginasterisk($sip, $queue, $id_agent, $username) {
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $member_name = $id_agent . '.' . $username . '.' . $sip;
            $peer = $this->asmanager->QueueAdd($queue, $interface, 0, $member_name);
            $status = $peer['Response'];
            if ($status == "Success") {
                $activity = "AGENT LOGIN OK";
            } else {
                $activity = "AGENT LOGIN ERROR";
            }
        }
    }

    function logout() {
        $this->logoffasterisk();

        $this->session->sess_destroy();
        echo json_encode(array("status" => TRUE));
    }

    function ajax_update_aux() {
        $data = array('status_agent' => $this->input->post('aux'));
        $id = $this->session->userdata('id_agent');
        $agent = $this->mphone->get_status_aux($id);
        if ($this->input->post('aux') == 1) {
            $aux = "AGENT PAUSE MAKAN SIANG";
        } else if ($this->input->post('aux') == 2) {
            $aux = "AGENT PAUSE SHOLAT";
        } else {
            $aux = "AGENT PAUSE Dll";
        }
        $aux_act = array(
            'activity' => $aux,
            'data' => 'By Supervisor',
            'interface' => 'SIP/' . $agent[0]['sip_no'],
            'id_agent' => $agent[0]['id_agent'],
            'queue' => $agent[0]['queue']
        );
        $this->db->insert('agent_activity', $aux_act);
        $this->mphone->update(array('id_agent' => $id), $data);
        $this->asteriskagentpause();
        echo json_encode(array("status" => TRUE));
    }

    function ajax_update_unpause() {
        //$id = $this->session->userdata('id_agent');

        $agent = $this->mphone->get_status_aux($this->session->userdata('id_agent'));
        $activity = $this->m_cstmr->get_daily_aux_by_agent($this->session->userdata('id_agent'));
        //$ag = $this->m_cstmr->get_data_agent($this->session->userdata('id_agent'));
        date_default_timezone_set('asia/jakarta');
        $datetime1 = strtotime($activity[0]['time']);
        $datetime2 = strtotime(date("Y-m-d H:i:s"));
        $interval  = abs($datetime2 - $datetime1);
        $ag = $this->m_cstmr->get_data_agent($this->session->userdata('id_agent'));

        $add_time=strtotime($ag[0]['today_activity'])+$interval;
        $add_date= date('H:i:s',$add_time);
        $aux_act = array(
            'activity' => 'AGENT UNPAUSE',
            'data' => 'By Supervisor',
            'interface' => 'SIP/' . $agent[0]['sip_no'],
            'id_agent' => $agent[0]['id_agent'],
            'queue' => $agent[0]['queue']
        );
        $this->db->insert('agent_activity', $aux_act);
        $endtime = date($ag[0]['today_activity'], strtotime("+".$interval." second"));
        $data = array(
            'status_agent' => 0,
            'today_activity' => $add_date
        );
        $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $data);
        $this->asteriskagentunpause();
        echo json_encode(array("status" => TRUE));
    }

    function logoffasterisk() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue');
        $id_agent = $this->session->userdata('id_agent');

        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $peer = $this->asmanager->QueueRemove($queue, $interface);
            $status = $peer['Response'];
            if ($status == "Success") {
                $where = array('interface' => $sip);
                $this->db->delete("queue_members", $where);
                $user_update = array('status_agent' => 0);
                $w = array('sip_no' => $sip);
                $this->db->update('agent', $user_update, $w);
                $activity = "AGENT LOGOFF OK";
            } else {
                $activity = "AGENT LOGOFF ERROR";
            }
            $act = array(
                'id_agent' => $id_agent,
                'interface' => $interface,
                'queue' => $queue,
                'data' => 'By Supervisor',
                'activity' => $activity
            );
            $this->db->insert('agent_activity', $act);
        }
    }

    function asteriskagentpause() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue');
        $id_agent = $this->session->userdata('id_agent');
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $this->asmanager->QueuePause($queue, $interface, true);
            $where = array('id_agent' => $id_agent);
            $data['paused'] = '1';
            $this->db->update("queue_members", $data, $where);
        }
    }

    function asteriskagentunpause() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue');
        $id_agent = $this->session->userdata('id_agent');
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $this->asmanager->QueuePause($queue, $interface, 0);
            $where = array('id_agent' => $id_agent);
            $data['paused'] = '0';
            $this->db->update("queue_members", $data, $where);
        }
    }

    function check_sip_exists() {
        $sip = $this->input->post('sip_no');
        $exists = $this->mphone->user($sip);
        $count = count($exists);
        if ($this->input->post('sip_no') == '') {
            echo "<span class='status-not-available'> ISI SIP ! </span>";
        } else {
            if ($count > 0) {
                echo "<span class='status-not-available'> SIP sudah terdaftar</span>";
            } else {
                echo "<span class='status-available'> KTP Available</span>";
            }
        }
    }

}
