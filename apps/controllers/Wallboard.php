<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallboard extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->library(array('asmanager', 'session', 'form_validation'));
        $this->load->model(array('mphone', 'm_cstmr'));
    }

    function index() {
        //echo date("Y").'/'.date("m").'/'.date("h");
        $a = array();
        $a['html']['css'] 	= add_css('/wallboard/bootstrap/css/bootstrap.css');
        $a['html']['js'] 	= add_js('/wallboard/js/cc/wallboard.js');
        $a['html']['js'] 	.= add_js('/wallboard/bootstrap/js/bootstrap.js');
 
		//$this->load->view('phone', $a, FALSE);
		
		$this->load->view('wallboard_bigfont',$a);
    }
	
	function get_updates(){
        //echo "test";
       $agent_inbound 	= $this->get_inbound();
       $agent_outbound 	= $this->get_outbound();

       //echo ($agent_inbound.'---'.$agent_outbound);
       $this->db = $this->load->database('default', TRUE);
       $this->db->where('sip_no <>', 0);
       $this->db->where('role', 'Inbound');
       $this->db->where_in('active', array('1','2','3'));
       $this->db->where_not_in('sip_no',array('801','802','803','804','701','702','703','704'));
       $qObj = $this->db->get('agent');
       $json['staffed'] = $qObj->num_rows();
       $json['agent_inbound'] = ($agent_inbound != 0) ? $agent_inbound : 0;
       $json['agent_outbound'] = ($agent_outbound != 0) ? $agent_outbound : 0;
	 
       $json['acdin'] = 0;
       $json['aux'] = 0;
       $json['other'] = 0;
       $json['avail'] = 0;
       $json['waiting'] = 0;
       $json['availts'] = 0;
       $json['availrt'] = 0;
       
       $sip_inuse = $this->get_sipinuse();
        if($qObj->num_rows() > 0){
			$qArr = $qObj->result_array();
			foreach($qArr as $user){
			  $curstat = $sip_inuse[$user['sip_no']];
			  if($curstat == 'IDLE'){
				$json['avail']++;
			  } else if($curstat == 'INUSE'){
				$json['acdin']++;
			  } else if($curstat == 'HOLD'){
				$json['other']++;
			  } else if($curstat == 'AUX'){
				$json['aux']++;
			  }
			}
       }
	   
		$json['waiting'] = $this->get_waitingcount2();

       echo json_encode($json);  
    }
    
    function get_sipinuse(){
      if ($this->asmanager->connect()) {
 			$asresponse = $this->asmanager->command('sip show inuse');
            $asdata = $asresponse['data'];
            $lines = explode("\n", $asdata);
            $curline = 0;
            $response = array();
            $response_line = 0;
            foreach($lines as $line){
                if($curline > 1){
                  $line = preg_replace('/\s+/', ' ', $line);
                  $chunk = explode(' ',$line);
                  $stat = explode('/',@$chunk[1]);
				  
				 // var_dump($chunk);
                  $response[$chunk[0]] = $this->translate_stat($stat);
                  if($stat[0] == '0'){
					$is_pause = $this->check_pause($chunk[0]);
                    if($is_pause == 1) { $response[$chunk[0]] = 'AUX'; }
                  }
                }
                $curline++;
            }
			
		  	  //var_dump($response);
            return $response;    
	   }
    }
    
   function check_pause($sip=0){
        $is_pause = 0;
        $this->db2 = $this->load->database('default', TRUE);
        $this->db2->where('interface', 'SIP/'.$sip);
        $this->db2->order_by('id', 'DESC');
        $this->db2->limit(1);
        $qObj = $this->db2->get('agent_activity');
        if($qObj->num_rows() > 0){
            $qArr = $qObj->row_array();
            $activity = $qArr['activity'];
            if(substr($activity, 0, 11) == 'AGENT PAUSE'){
                $is_pause = 1;
            }
        }
        return $is_pause;
    }
    
    function translate_stat($stat){
        if($stat[0] == '0'){
            $stat_name = 'IDLE';
        } else if($stat[0] == '1'){
            $stat_name = 'INUSE';
            if($stat[2] == '1'){
            $stat_name = 'HOLD';  
            }
        } else {
            $stat_name = 'ERR';
        }
        return $stat_name;
    }
    
    function get_activeQueue(){
        $this->db3 = $this->load->database('asterisk', TRUE);
        
        $this->db3->select('extension, descr');
		$this->db3->where('extension LIKE "5%"', NULL, FALSE);
        //$this->db3->where_in('extension', array('5000','5002','5003','7000'));
        $qObj = $this->db3->get('queues_config');
        $qArr = $qObj->result_array();
        return $qArr;
    }
    
    function get_waitingcount(){
        $wait_count = 0;
        $queues = $this->get_activeQueue();
        foreach($queues as $queue){
            $queueInfo = $this->get_queueinfo($queue['extension']);
            //var_dump($queueInfo);
			$pattern = "!SIP\/newrock-.+!";
            $is_match = preg_match_all($pattern, $queueInfo, $matches, PREG_SET_ORDER);
            //var_dump($matches);
            if(!empty($is_match)){
               foreach($matches as $found){
                //var_dump($found);
                $wait_count++;
               } 
            }
        }
        //var_dump($wait_count);
        return $wait_count;  
    }
	
 	function get_waitingcount2(){
		$this->asmanager->connect();
         
        $line = $this->asmanager->command("core show channels");
        $a['edata'] =explode("\n", $line['data']);
		$queue = 0;
        for($i=0;$i<count($a['edata']);$i++){
            if(substr($a['edata'][$i],0,18) =="SIP/trunk-newrock-" && substr($a['edata'][$i],21,11) !='s@from-pstn' && substr($a['edata'][$i],21,3) !='777')
            {
                //$queue = count($a['edata'][$i]);
				$queue++;
				//echo $queue;
			}
			
        }
		//echo $queue;
		return $queue;  
	}
    
    function get_queueinfo($queue){
        if ($this->asmanager->connect()) {
            $asresponse = $this->asmanager->command('queue show '.$queue);
            $asdata = $asresponse['data'];
            return $asdata;
        }
    }
	
	function load_monitorqueue(){
		 $this->db2 = $this->load->database('default', TRUE);
		 $curdate = DATE('Y-m-d');
		 $where = array(
			'lastapp'=> 'Queue'
		 );
		 $this->db2->select('COUNT(uniqueid) AS queue', FALSE);
		 $this->db2->select('COUNT(CASE WHEN dstchannel = "" AND lastapp="Queue" THEN uniqueid END) AS abandon', FALSE);
		 $this->db2->select('COUNT(CASE WHEN dstchannel LIKE "SIP/%" AND lastapp ="Queue" AND duration_wait_cc > 20 THEN uniqueid END) AS answer_up20', FALSE);
		 $this->db2->select('COUNT(CASE WHEN dstchannel LIKE "SIP/%" AND lastapp ="Queue" AND duration_wait_cc <= 20 THEN uniqueid END) AS answer_btm20', FALSE);
		 $this->db2->select('COUNT(CASE WHEN dstchannel LIKE "SIP/%" AND lastapp ="Queue" THEN uniqueid END) AS answered', FALSE);
		 
		 //$this->db2->where($where);
		 //$this->db2->where('dst', '6400');
		 $this->db2->where_in('dst', array('5000','5002','5003'));
		 $this->db2->where("DATE(calldate) = curdate()", NULL, FALSE);
		 $qObj = $this->db2->get('cdr');
		 //echo $this->db->last_query();
		 $qArr = array();
		 if($qObj->num_rows() > 0){
			$qArr = $qObj->row_array();
			echo json_encode($qArr);
		 }
    }
	
	function call_center(){
		$this->db = $this->load->database('call_center', TRUE);
		$date2 = date("Y-m-d H:i:59");
		$date1 = date("Y-m-d H:i:00",mktime(date("H"),date("i")-3,0,date("m"),date("d"),date("Y")));
		//die($date1." ".$date2);
/*  	$sql1 = "delete from queue_temp where tgl != date(now())";
		$this->db->query($sql1); */
		
/* 		$sqlx = "INSERT INTO queue_temp(callerid,uniqueid,jam,tgl)
				SELECT callerid, uniqueid, TIME(datetime_entry_queue) AS jam, DATE(datetime_entry_queue) AS tgl FROM call_entry WHERE `status` = 'en-cola' AND date(`datetime_entry_queue`) = curdate( )";
		$this->db->query($sqlx); */
		
 		$sql = " SELECT *
				FROM (SELECT COUNT(CASE WHEN `status` = 'terminada' AND duration >20 THEN id END ) AS answer_up20, 
				COUNT(CASE WHEN `status` = 'terminada' AND duration <=20 THEN id END ) AS answer_btm20
				FROM call_center.call_entry ce
				WHERE DATE( ce.datetime_entry_queue ) = curDate( )
				)a";
 		/* $sql = "SELECT *, abandon - abd_queue as abandon_new,abandon + answered AS queue From(
				SELECT COUNT(*) AS queue1, COUNT(DISTINCT 
				CASE WHEN `status` = 'en-cola'
				THEN uniqueid
				END ) AS call_queue_ex, COUNT(DISTINCT 
				CASE WHEN `status` = 'terminada'
				THEN uniqueid
				END ) AS answered, COUNT(DISTINCT
				CASE WHEN `status` = 'abandonada'
				THEN uniqueid
				END ) AS abandon, COUNT(DISTINCT 
				CASE WHEN `status` = 'abandonada'
				AND duration_wait >8
				THEN uniqueid
				END ) AS abandon8s, COUNT(DISTINCT 
				CASE WHEN `status` = 'abandonada'
				AND duration_wait <=8
				THEN uniqueid
				END ) AS abandonUn8s, COUNT(DISTINCT 
				CASE WHEN `status` = 'terminada'
				AND duration >20
				THEN uniqueid
				END ) AS answer_up20, COUNT(DISTINCT 
				CASE WHEN `status` = 'terminada'
				AND duration <=20
				THEN uniqueid
				END ) AS answer_btm20, SEC_TO_TIME( floor( AVG( duration ) ) ) AS avg_old
				FROM call_center.call_entry ce
				WHERE DATE( ce.datetime_entry_queue ) = curDate( )) a,
				(SELECT SEC_TO_TIME(floor(AVG(duration))) AS aht FROM call_center.call_entry ce
				WHERE DATE(ce.datetime_entry_queue) = curDate() AND `status` = 'terminada') f,
				(SELECT COUNT(*) AS abd_queue
				FROM queue_temp a inner join call_entry b on a.uniqueid=b.uniqueid
				WHERE b.status='abandonada' AND DATE( a.tgl ) = curDate( ) AND b.duration_wait > 5 ) g
				"; */
		//die($sql);
		$q = $this->db->query($sql);
		$qArr = array();
		if($q->num_rows() > 0){
			$qArr = $q->row_array();
			echo json_encode($qArr);
		}
		
		$q->free_result();
	}
	
	function get_inbound(){
        $response = '';
        $this->db2 = $this->load->database('default', TRUE);
        $curdate = DATE('Y-m-d');
		$this->db2->select('COUNT(id_agent) AS inbound', FALSE);
        $this->db2->where('sip_no <>', 0);
        $this->db2->where('role', 'inbound');
        $this->db2->where_in('active', array('1','2','3'));
        $this->db2->where_not_in('sip_no',array('801','802','803','804','701','702','703','704'));
        $qObj = $this->db2->get('agent');
        if($qObj->num_rows() > 0){
            $rArr = $qObj->row_array();
            $response = $rArr['inbound'];
        } else {
            $response = 0;
        }
		//echo $response;
        return $response;
		$qObj->free_result();
    }
	
	function get_outbound(){
        $response = '';
        $this->db3 = $this->load->database('default', TRUE);
        $curdate = DATE('Y-m-d');
		$this->db3->select('COUNT(id_agent) AS outbound', FALSE);
        $this->db3->where('sip_no <>', 0);
        $this->db3->where('role', 'Outbound');
        $this->db3->where_in('active', array('1','2','3'));
		$this->db3->where_not_in('sip_no',array('801','802','803','804','701','702','703','704'));
        $qObj = $this->db3->get('agent');
        if($qObj->num_rows() > 0){
            $rArr = $qObj->row_array();
            $response = $rArr['outbound'];
        } else {
            $response = 0;
        }
		//echo $response;
        return $response;
        $qObj->free_result();
    }

}
