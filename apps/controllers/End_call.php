<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/Diskstatus.php';
class End_call extends CI_Controller {

    var $call_center;
    function __construct() {
        parent::__construct();
        $this->load->library(array('asmanager', 'session'));
        $this->load->helper('header');
        $this->load->library('form_validation');
        $this->call_center = $this->load->database('call_center', true);
    }
    
	
	//yg dibawah buat get token sfdc
     function key_generator()
    {
         
        $url2 = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=".$this->config->item('client_id_p')."&client_secret=".$this->config->item('client_secret_p')."&username=".$this->config->item('username_p')."&password=".$this->config->item('password_p')."";
        $data2 = array(
            'client_id' => $this->config->item('client_id_p'), 
            'client_secret' => $this->config->item('client_secret_p'), 
            'username' =>$this->config->item('username_p'), 
            'password' => $this->config->item('password_p'), 
            'grant_type' => 'password');
                    
        $options2 = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => http_build_query($data2),
            ),
        );
        $context2 = stream_context_create($options2);
        $result2 = json_decode(file_get_contents($url2, false, $context2));
        //echo $result2->access_token;
        $token= $result2->access_token;
        
       
        $time = date('Y-m-d H:i:s');
        $xmlfolder = $this->config->item('path_xml') . '/xml/';


        $xmlfile = "token.xml";
        $filePath = $xmlfolder . $xmlfile;
        if ($result2!=0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "datacontent");

            xmlwriter_start_element($wrt, "TOKEN_PRODUCTION");
            xmlwriter_text($wrt, $token);
            xmlwriter_end_element($wrt);

            
            xmlwriter_start_element($wrt, "waktu");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);


            xmlwriter_end_element($wrt);
           

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "dailydata");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
    
     function monitoring()
    {
         $this->load->model('mphone');
         $list = $this->mphone->get_datatables();
         var_dump($list);
         
         $this->load->model('m_cstmr');
        $data = $this->m_cstmr->getdurationcall2();
        $this->load->model('mxml_gen');
        $time = date('Y-m-d H:i:s');
        
       // $data = $this->mxml_gen->get_daily_data();
        $total = count($data);
        
        
        
      
        $xmlfolder = $this->config->item('path_xml') . '/xml/';

        $waktu =date('Ymd');
        $xmlfile = "monitoring.xml";
        $filePath = $xmlfolder . $xmlfile;
        if (count($data) > 0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath,777, true);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            for ($i = 0; $i < $total; $i++) {
                xmlwriter_start_element($wrt, "datacontent");

                xmlwriter_start_element($wrt, "id_agent");
                xmlwriter_text($wrt, $data[$i]['id_agent']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "username");
                xmlwriter_text($wrt, $data[$i]['username']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "sip_no");
                xmlwriter_text($wrt, $data[$i]['sip_no']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "email");
                xmlwriter_text($wrt, $data[$i]['email']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "active");
                xmlwriter_text($wrt, $data[$i]['active']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "onhold");
                xmlwriter_text($wrt, $data[$i]['onhold']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "role");
                xmlwriter_text($wrt, $data[$i]['role']);
                xmlwriter_end_element($wrt);

            

                xmlwriter_end_element($wrt);
            }

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "dailydata");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
    
    function postTerminadaInbound($url, $b) {
       
        $url2 = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=".$this->config->item('client_id_p')."&client_secret=".$this->config->item('client_secret_p')."&username=".$this->config->item('username_p')."&password=".$this->config->item('password_p')."";
        $data2 = array(
            'client_id' => $this->config->item('client_id_p'), 
            'client_secret' => $this->config->item('client_secret_p'), 
            'username' => $this->config->item('username_p'), 
            'password' => $this->config->item('password_p'), 
            'grant_type' => 'password');
                    
        $options2 = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => http_build_query($data2),
            ),
        );
        
        $context2 = stream_context_create($options2);
        $result2 = json_decode(file_get_contents($url2, false, $context2));
        var_dump($result2->access_token);
        exit();
        $header = array('Content-Type: application/json',"Authorization: OAuth ".$result2->access_token);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $b);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

        curl_exec($curl);
        curl_close($curl);
    }
    
    function test()
    {
        $this->load->model('mxml_gen');
        $vi = $this->mxml_gen->getlogcti();
         $data[] = array(
            "success" => true,
            "total" => count($vi),
            "errors" => [],
            'Data' => $vi
        );

        echo json_encode($data);
    }
	
	function updateAbandoneCall()
    {
        $this->load->model('m_cstmr');
        $x = $this->m_cstmr->getAbandonecall();
        
       // var_dump($x);
	   // return false;
		
        for($c=0;$c<count($x);$c++){
            echo $x[$c]['duration'];
            $where =array('uniqueid' => $x[$c]['uniqueid']);
            $data =array('id_user'=>$x[$c]['id_agent'] ,'duration_wait_cc' => $x[$c]['duration_wait'],'duration_cc' => $x[$c]['duration'],'date_time_entry_queue' => $x[$c]['datetime_entry_queue'],'datetime_init' => $x[$c]['datetime_init'],'datetime_end' => $x[$c]['datetime_end']);
            $data2 =array('status_log' => 2);
            $this->db->update('cdr',$data,$where);
            $this->call_center->update('call_entry',$data2,$where);
			echo $this->db->last_query();
            
        }
       // $this->updatedurs();
        
        
    }
	
	function updatedurAll()
    {
        $this->load->model('m_cstmr');
        $x = $this->m_cstmr->getdurationcallAll();
        
        
        for($c=0;$c<count($x);$c++){
            echo $x[$c]['duration'];
            $where =array('uniqueid' => $x[$c]['uniqueid']);
            $data =array('id_user'=>$x[$c]['id_agent'] ,'duration_wait_cc' => $x[$c]['duration_wait'],'duration_cc' => $x[$c]['duration'],'date_time_entry_queue' => $x[$c]['datetime_entry_queue'],'datetime_init' => $x[$c]['datetime_init'],'datetime_end' => $x[$c]['datetime_end']);
            $data2 =array('status_log' => 2);
            $this->db->update('cdr',$data,$where);
            $this->call_center->update('call_entry',$data2,$where);
            
        }
        
        
    }

   function updatedurAlls()
    {
        $this->load->model('m_cstmr');
	 echo 'tes';
        $x = $this->m_cstmr->getdurationcallmanual();
        
        
        for($c=0;$c<count($x);$c++){
            echo $x[$c]['duration'];
            $where =array('uniqueid' => $x[$c]['uniqueid']);
            $data =array('id_user'=>$x[$c]['id_agent'] ,'duration_wait_cc' => $x[$c]['duration_wait'],'duration_cc' => $x[$c]['duration'],'date_time_entry_queue' => $x[$c]['datetime_entry_queue'],'datetime_init' => $x[$c]['datetime_init'],'datetime_end' => $x[$c]['datetime_end']);
            $data2 =array('status_log' => 2);
            $this->db->update('cdr',$data,$where);
            $this->call_center->update('call_entry',$data2,$where);
            echo 't';
        }
        
        
    }
    
    function updatedur()
    {
        $this->load->model('m_cstmr');
        $x = $this->m_cstmr->getdurationcall();
        
        
        for($c=0;$c<count($x);$c++){
            echo $x[$c]['duration'];
            $where =array('uniqueid' => $x[$c]['uniqueid']);
            $data =array('id_user'=>$x[$c]['id_agent'] ,'duration_wait_cc' => $x[$c]['duration_wait'],'duration_cc' => $x[$c]['duration'],'date_time_entry_queue' => $x[$c]['datetime_entry_queue'],'datetime_init' => $x[$c]['datetime_init'],'datetime_end' => $x[$c]['datetime_end']);
            $data2 =array('status_log' => 2);
            $this->db->update('cdr',$data,$where);
            $this->call_center->update('call_entry',$data2,$where);
            
        }
        $this->updatedurs();
        
        
    }
    
    function updatedurs()
    {
        $this->load->model('m_cstmr');
        $x = $this->m_cstmr->getdurationcall2();
        
//        var_dump($x);
        
        for($c=0;$c<count($x);$c++){
         
            $this->m_cstmr->updateanu($x[$c]['id_agent'],$x[$c]['sip_no']);
        }
        
        
    }
    
    
    public function view_transfer()
    {
        $this->load->model('mxml_gen');
        $vi = $this->mxml_gen->getdailydatatransfer();
        if ($_GET['date'] == "") {
            $data = array("success" => false, "errors" => 'enter date');
            print_r(json_encode($data));
        }else{

        $data[] = array(
            "success" => true,
            "errors" => [],
            'Data' => $vi
        );

        echo json_encode($data);}
    }
    
    public function view()
    {
        $this->load->model('mxml_gen');
        $vi = $this->mxml_gen->getdailydata();
        if ($_GET['date'] == "") {
            $data = array("success" => false, "errors" => 'enter date');
            print_r(json_encode($data));
        }else{

        $data[] = array(
            "success" => true,
            "errors" => [],
            'Data' => $vi
        );

        echo json_encode($data);}
    }
            
    function xml()
    {
        $this->load->model('mxml_gen');
        $time = date('Y-m-d H:i:s');
        
        $data = $this->mxml_gen->get_daily_data();
        $total = count($data);
        
        $xmlfolder = $this->config->item('path_xml') . '/xml/dailydata/';

        $waktu =date('Ymd');
        $xmlfile = "$waktu.xml";
        $filePath = $xmlfolder . $xmlfile;
        if (count($data) > 0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath,777, true);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            for ($i = 0; $i < $total; $i++) {
                xmlwriter_start_element($wrt, "datacontent");

                xmlwriter_start_element($wrt, "ID_VALDO");
                xmlwriter_text($wrt, $data[$i]['ID_VALDO']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "SIP");
                xmlwriter_text($wrt, $data[$i]['SIP']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "PREV_DUR");
                xmlwriter_text($wrt, $data[$i]['PREV_DUR']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DESTINATION_NUMBER");
                xmlwriter_text($wrt, $data[$i]['DESTINATION_NUMBER']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DARI");
                xmlwriter_text($wrt, $data[$i]['DARI']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "START_DIALING_DATE");
                xmlwriter_text($wrt, $data[$i]['START_DIALING_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "START_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['START_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "END_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['END_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "DURATION_DIAL");
                xmlwriter_text($wrt, $data[$i]['DURATION_DIAL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_CALL");
                xmlwriter_text($wrt, $data[$i]['DURATION_CALL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_WAIT");
                xmlwriter_text($wrt, $data[$i]['DURATION_WAIT']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "RECORDING_FILE");
                xmlwriter_text($wrt, $data[$i]['RECORDING_FILE']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "STATUS");
                xmlwriter_text($wrt, $data[$i]['STATUS']);
                xmlwriter_end_element($wrt);

                xmlwriter_end_element($wrt);
            }

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "dailydata");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
    
    function xml_transfer()
    {
        $this->load->model('mxml_gen');
        $time = date('Y-m-d H:i:s');
        //$time = $_GET['date'];
        
        
        $data = $this->mxml_gen->get_daily_data_transfer();
        $total = count($data);
        
//        var_dump($data);
//        exit();
      
        $xmlfolder = $this->config->item('path_xml') . '/xml/dailydata/transfer/';
        //$waktu = str_replace('/', '', $time);
        $waktu =date('Ymd');
        $xmlfile = $waktu.".xml";
        $filePath = $xmlfolder . $xmlfile;
        if (count($data) > 0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath,777, true);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            for ($i = 0; $i < $total; $i++) {
                xmlwriter_start_element($wrt, "datacontent");

                xmlwriter_start_element($wrt, "ID_VALDO");
                xmlwriter_text($wrt, $data[$i]['ID_VALDO']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "SIP");
                xmlwriter_text($wrt, $data[$i]['SIP']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "PREV_DUR");
                xmlwriter_text($wrt, $data[$i]['PREV_DUR']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DESTINATION_NUMBER");
                xmlwriter_text($wrt, $data[$i]['DESTINATION_NUMBER']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DARI");
                xmlwriter_text($wrt, $data[$i]['DARI']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "START_DIALING_DATE");
                xmlwriter_text($wrt, $data[$i]['START_DIALING_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "START_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['START_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "END_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['END_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "DURATION_DIAL");
                xmlwriter_text($wrt, $data[$i]['DURATION_DIAL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_CALL");
                xmlwriter_text($wrt, $data[$i]['DURATION_CALL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_WAIT");
                xmlwriter_text($wrt, $data[$i]['DURATION_WAIT']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "RECORDING_FILE");
                xmlwriter_text($wrt, $data[$i]['RECORDING_FILE']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "STATUS");
                xmlwriter_text($wrt, $data[$i]['STATUS']);
                xmlwriter_end_element($wrt);

                xmlwriter_end_element($wrt);
            }

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "dailydata");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
     
    function xml_endcall_transfer()
    {
        $this->load->model('mxml_gen');
        $time = date('Y-m-d H:i:s');
        
        $data = $this->mxml_gen->get_data_call_in_terminada_t();
        $total = count($data);
        
        
        
      
        $xmlfolder = $this->config->item('path_xml') . '/xml/';

        $waktu =date('Ymd');
        $xmlfile = "log_cti_transfer.xml";
        $filePath = $xmlfolder . $xmlfile;
        if (count($data) > 0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath,777, true);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "endcalltransfer");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            for ($i = 0; $i < $total; $i++) {
                xmlwriter_start_element($wrt, "datacontent");

                xmlwriter_start_element($wrt, "ID_VALDO");
                xmlwriter_text($wrt, $data[$i]['ID_VALDO']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "SIP");
                xmlwriter_text($wrt, $data[$i]['SIP']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "PREV_DUR");
                xmlwriter_text($wrt, $data[$i]['PREV_DUR']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DESTINATION_NUMBER");
                xmlwriter_text($wrt, $data[$i]['DESTINATION_NUMBER']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DARI");
                xmlwriter_text($wrt, $data[$i]['DARI']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "START_DIALING_DATE");
                xmlwriter_text($wrt, $data[$i]['START_DIALING_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "START_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['START_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "END_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['END_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "DURATION_DIAL");
                xmlwriter_text($wrt, $data[$i]['DURATION_DIAL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_CALL");
                xmlwriter_text($wrt, $data[$i]['DURATION_CALL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_WAIT");
                xmlwriter_text($wrt, $data[$i]['DURATION_WAIT']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "RECORDING_FILE");
                xmlwriter_text($wrt, $data[$i]['RECORDING_FILE']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "STATUS");
                xmlwriter_text($wrt, $data[$i]['STATUS']);
                xmlwriter_end_element($wrt);

                xmlwriter_end_element($wrt);
            }

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "endcalltransfer");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
    //ini men buat masukin ke xml nya/ metode ini ringan krn gak nembak langsung database
	//function query xml
    function xml_endcall()
    {
        $this->load->model('mxml_gen');
        $time = date('Y-m-d H:i:s');
        
        $data = $this->mxml_gen->get_data_call_in_terminada(); //<<querynya
       
        $total = count($data);
      
        $xmlfolder = $this->config->item('path_xml') . '/xml/';

        $waktu =date('Ymd');
        $xmlfile = "log_cti.xml";
        $filePath = $xmlfolder . $xmlfile;
        if (count($data) > 0) {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath,777, true);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "endcalltransfer");

            xmlwriter_start_element($wrt, "updated");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            for ($i = 0; $i < $total; $i++) {
                xmlwriter_start_element($wrt, "datacontent");

                xmlwriter_start_element($wrt, "ID_VALDO");
                xmlwriter_text($wrt, $data[$i]['ID_VALDO']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "SIP");
                xmlwriter_text($wrt, $data[$i]['SIP']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "PREV_DUR");
                xmlwriter_text($wrt, $data[$i]['PREV_DUR']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DESTINATION_NUMBER");
                xmlwriter_text($wrt, $data[$i]['DESTINATION_NUMBER']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DARI");
                xmlwriter_text($wrt, $data[$i]['DARI']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "START_DIALING_DATE");
                xmlwriter_text($wrt, $data[$i]['START_DIALING_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "START_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['START_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "END_CALL_DATE");
                xmlwriter_text($wrt, $data[$i]['END_CALL_DATE']);
                xmlwriter_end_element($wrt);

                xmlwriter_start_element($wrt, "DURATION_DIAL");
                xmlwriter_text($wrt, $data[$i]['DURATION_DIAL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_CALL");
                xmlwriter_text($wrt, $data[$i]['DURATION_CALL']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "DURATION_WAIT");
                xmlwriter_text($wrt, $data[$i]['DURATION_WAIT']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "RECORDING_FILE");
                xmlwriter_text($wrt, $data[$i]['RECORDING_FILE']);
                xmlwriter_end_element($wrt);
                
                xmlwriter_start_element($wrt, "STATUS");
                xmlwriter_text($wrt, $data[$i]['STATUS']);
                xmlwriter_end_element($wrt);

                xmlwriter_end_element($wrt);
            }

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        } else {
            $folderPath = str_replace("//", "/", $xmlfolder);
            if (!is_dir($folderPath)) {
                mkdir_r($folderPath);
            }

            $wrt = xmlwriter_open_uri($filePath);
            xmlwriter_set_indent($wrt, true);

            xmlwriter_start_document($wrt, '1.0', 'UTF-8');
            xmlwriter_start_element($wrt, "dailydata");

            xmlwriter_start_element($wrt, "delete");
            xmlwriter_text($wrt, $time);
            xmlwriter_end_element($wrt);

            xmlwriter_start_element($wrt, "endcalltransfer");
            xmlwriter_text($wrt, "delete");
            xmlwriter_end_element($wrt);

            xmlwriter_end_element($wrt);
            xmlwriter_end_document($wrt);
        }
    }
    
    function anu(){
        try {
          $diskStatus = new DiskStatus('/');

          $freeSpace = $diskStatus->freeSpace();
          $totalSpace = $diskStatus->totalSpace();
          $barWidth = ($diskStatus->usedSpace()/100) * 560;
        } catch (Exception $e) {
              echo 'Error ('.$e->getMessage().')';
              exit();
        }
        
        $disk =$totalSpace-$freeSpace;
        

        echo 'FREE SPACE : '.$freeSpace;
        echo '<br>';

        
        echo 'Total : '.$totalSpace;
        echo '<br>';
        echo 'USED SPACE : '.$disk;
    }

    function index() {
       
        $is_logged_in = $this->session->userdata('is_logged_in');
            if (!isset($is_logged_in) || $is_logged_in != true) {
                redirect('asterisk');
            }
        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');
        //$command = $_GET['command'];
        $cc = $this->input->post('command');
//       var_dump($command.'<br>'.$cc);
          $this->form_validation->set_rules('command', 'Command', 'trim|required');
      if ($this->form_validation->run() === TRUE) {
           $this->asmanager->connect();
           
            $this->asmanager->Events('off');
            $line = $this->asmanager->command($cc);
            $a['data'] =explode("\n", $line['data']);
      }
      $a['hilang']="";
      $a['hidden']="hidden";
      $this->load->view('command',$a);
    
    }
    
   
    function command()
    {//        $total = count(array_filter($line));
//        $unique = $this->input->get('id_valdo');
//        if ($unique == "") {
//            redirect(base_url());
//        }$no = "";
//        for ($x = 0; $x < $total; $x++) {
//            $cols = explode("!", $line[$x]);
//            if ($cols[13] == $unique) {
//                $no = $cols[0];
//            }
//        }
//        $this->load->model('m_cstmr');
//        $this->asmanager->command("channel request hangup" . $no);
//        sleep(5); 
        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');
        $a['hidden']=""; $a['hilang']="hidden";
        $this->asmanager->connect();

         $this->asmanager->Events('off');
         
         $line = $this->asmanager->command("core show channels");
         $a['edata'] =explode("\n", $line['data']);
         
         //var_dump(substr($a['edata'][0],0,11));
         for($i=1;$i<count($a['edata']);$i++){
             if(substr($a['edata'][$i],0,11) =="SIP/newrock" && substr($a['edata'][$i],21,11) !='s@from-pstn')
             {
                 
                 var_dump(substr($a['edata'][$i],21,11));
                 //echo count($a['edata'][$i]);
             }
         
         }
         
         
         //$cn = explode("!", $line['data']);
        //$stwd = strlen('SIP/' . $line_number);
        // preg_match_all('/[0-9]{3}[\-][0-9]{6}|[0-9]{3}[\s][0-9]{6}|[0-9]{3}[\s][0-9]{3}[\s][0-9]{4}|[0-9]{12}|[0-9]{3}[\-][0-9]{3}[\-][0-9]{4}/', $cn[7], $matches);
        //if ($matches[0] == $_GET['to']) {
        //if (substr($cn[0], 0, $stwd) == 'SIP/' . $line_number  ) {
//        if ($cn[7] == $_GET['to']) {
//            $l = preg_split("/\\r\\n!\\r!\\n/", $cn);
//          var_dump($cn);
//       }
        //var_dump($cn);
        //$this->asmanager->command("channel request hangup" . $no);
      //  var_dump($);
     // var_dump($a['edata']);
    
     $a['hilang']="hidden";
     $a['hidden']=""; 
     $this->load->view('command',$a);
    }
}
