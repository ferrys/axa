<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Terminada_production extends REST_Controller {

    var $call_center;
//
//    public function index_get() {
//         $url = "https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG959Nd8JMmavRpXnCJkKUDI9HObySw9dy5_.jr.OWhxJZNKlcDQTcd1tOD_LcREjdx4id.5g.Uim2dk6og&client_secret=9169228210180829959&username=valdo@mii.axa.ali&password=aliaxa123";
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
//        curl_setopt($ch, CURLOPT_URL, $url);
//        $result = curl_exec($ch);
//        curl_close($ch);
//        $obj = json_decode($result);
//        $this->load->model('m_cstmr');
//        $x = $this->m_cstmr->get_data();
//        header("Content-Type: application/json");
//        if ($this->get('access_token') == "") {
//            $this->response([
//                'status' => FALSE,
//                'errors' => 'data not found',
//                    ], REST_Controller::HTTP_NOT_FOUND);
//        } else {
//            if ($x[0]['sip__c'] != "") {
//                $this->response($x, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
//            } else {
//                // Set the response and exit
//                $this->response([
//                    'status' => FALSE,
//                    'errors' => '',
//                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
//            }
//        }
//    }
//    public function index_post() {}
//    
//    public function index_patch() {
//        date_default_timezone_set('asia/jakarta');
//        $this->load->model('m_cstmr');
//        //$id ="1508309638.78";
//        $y = $this->m_cstmr->get_data_call_in_terminada_production();
//               //https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG959Nd8JMmavRnfPOYxVcw5TU0_m1Igr8xP0bZ0Yv5haAYCvhs9IhzHAEVWtETbnuCQvdI.rxBfipy8aBT&client_secret=2743732178412977155&username=valdo@mii.axa.ali.fullsb&password=aliaxa123
//        $url1 = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=".$this->config->item('client_id_p')."&client_secret=".$this->config->item('client_secret_p')."&username=".$this->config->item('username_p')."&password=".$this->config->item('password_p')."";
//        $data1 = array('client_id' => $this->config->item('client_id_p'), 'client_secret' => $this->config->item('client_secret_p'), 'username' =>$this->config->item('username_p'), 'password' => $this->config->item('password_p'), 'grant_type' => 'password');
//                    
//        $options1 = array(
//            'http' => array(
//                'header' => "Content-type: application/json\r\n",
//                'method' => 'POST',
//                'content' => http_build_query($data1),
//            ),
//        );
//        
//        $context = stream_context_create($options1);
//        $result = json_decode(file_get_contents($url1, false, $context));
//        
//        //var_dump($y);  
//        for ($i = 0; $i < count($y); $i++) {
//            $tanggal =date("Y").'/'.date("m").'/'.date("d");
//            if(strlen($y[$i]["RECORDING_FILE"])>75)
//            {$recording_file = str_replace("/var/spool/asterisk/monitor/".$tanggal,$tanggal."", $y[$i]["RECORDING_FILE"]);}
//            else{$recording_file = $tanggal."/".$y[$i]["RECORDING_FILE"];}
////            if($y[$i]["START_CALL_DATE"]=="OUTBOUND")
////            {}
////            else if($y[$i]["START_CALL_DATE"]=="OUTBOUND"){}
////            else{}
//            //$link ="https://ap4.salesforce.com/services/data/v37.0/sobjects/Log_CTI_Valdo__c/ID_Valdo__c/";
//            $start_dial= date('Y-m-d', strtotime($y[$i]['START_DIALING_DATE'])).'T'.date('H:i:s', strtotime($y[$i]['START_DIALING_DATE'])).'.000+0700';
//            $start_call= date('Y-m-d', strtotime($y[$i]["START_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["START_CALL_DATE"])).'.000+0700';
//            $end_call = date('Y-m-d', strtotime($y[$i]["END_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["END_CALL_DATE"])).'.000+0700';
//            $b = '{"Duration_Previous_Call__c":"'.$y[$i]["PREV_DUR"].'","SIP__c":"'.$y[$i]["SIP"].'","Start_Dialling__c" : "'.$start_dial .'","Transfer_From__c":"'.$y[$i]["TRANSFER_FROM"].'","Transfer_To__c":"'.$y[$i]["TRANSFER_TO"].'","From__c":"'.$y[$i]["DARI"].'","Recording_ID__c":"https://axasales.valdo-intl.com/recording/'.$recording_file.'","End_Calling__c":"'.$end_call.'","Start_Calling__c":"'.$start_call.'", "Duration_Call__c" : "' . $y[$i]['DURATION_CALL'] . '","Duration_Dialing__c" : "' . $y[$i]['DURATION_DIAL'] . '"}';
//            $valdo = "https://ap4.salesforce.com/services/data/v37.0/sobjects/Log_CTI_Valdo__c/ID_Valdo__c/".$y[$i]['ID_VALDO'];
//            $headers = array('Content-Type: application/json',"Authorization: OAuth ".$result->access_token);
//            $curl = curl_init();
//            
//            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
//            curl_setopt($curl, CURLOPT_URL, $valdo);
//            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($curl, CURLOPT_POSTFIELDS, $b);
//            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//           
//            curl_exec($curl);
//            curl_close($curl);
//            $e = array('status_log' => 1,);
//            $where = array('uniqueid' => $y[$i]['ID_VALDO']);
//            $this->db->update('cdr', $e, $where);
//       }
//        
//    }
//    
//    
//    
//
//    function postTerminadaInbound($url, $request) {
//        $url1 = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG959Nd8JMmavRpXnCJkKUDI9HObySw9dy5_.jr.OWhxJZNKlcDQTcd1tOD_LcREjdx4id.5g.Uim2dk6og&client_secret=9169228210180829959&username=valdo@mii.axa.ali&password=aliaxa123";
//        $data1 = array('client_id' => '3MVG959Nd8JMmavRpXnCJkKUDI9HObySw9dy5_.jr.OWhxJZNKlcDQTcd1tOD_LcREjdx4id.5g.Uim2dk6og', 'client_secret' => '9169228210180829959', 'username' => 'valdo@mii.axa.ali', 'password' => 'aliaxa123', 'grant_type' => 'password');
//
//        $options1 = array(
//            'http' => array(
//                'header' => "Content-type: application/json\r\n",
//                'method' => 'patch',
//                'content' => http_build_query($data1),
//            ),
//        );
//        $context = stream_context_create($options1);
//        $result = json_decode(file_get_contents($url1, false, $context));
//        $ch = curl_init($url);
//        $options = array(
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_HEADER => false,
//            CURLOPT_FOLLOWLOCATION => false,
//            CURLOPT_ENCODING => "utf-8", // handle all encodings
//            CURLOPT_AUTOREFERER => true, // set referer on redirect
//            CURLOPT_CONNECTTIMEOUT => 80, // timeout on connect
//            CURLOPT_TIMEOUT => 80, // timeout on response
//            CURLOPT_POST => 1, // i am sending post data
//            CURLOPT_POSTFIELDS => $request, // this are my post vars
//            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
//            CURLOPT_SSL_VERIFYPEER => false, //
//            CURLOPT_VERBOSE => 1,
//            CURLOPT_HTTPHEADER => array(
//                "Authorization: barier ".$result->access_token,
//                "Content-Type: application/json"
//            )
//        );
//        curl_setopt_array($ch, $options);
//        $data = curl_exec($ch);
//        curl_close($ch);
//        var_dump($data);
//        return $data;
//    }
    
    public function index_patch() {
        date_default_timezone_set('asia/jakarta');
        $this->load->model('m_cstmr');
        
        $y = $this->m_cstmr->get_data_call_in_terminada(); //langsung query
      //
        $this->load->model('mxml_gen');
        //$y = $this->mxml_gen->getlogcti();//pakai xml
        $vi = $this->mxml_gen->key_sfdc();
        $key =$vi[0]['TOKEN_PRODUCTION'];
        for ($i = 0; $i < count($y); $i++) {
            $tanggal =date("Y").'/'.date("m").'/'.date("d");
            if(strlen($y[$i]["RECORDING_FILE"])>75)
            {$recording_file = str_replace("/var/spool/asterisk/monitor/".$tanggal,$tanggal."", $y[$i]["RECORDING_FILE"]);}
            else{$recording_file = $tanggal."/".$y[$i]["RECORDING_FILE"];}
            
            $string = $y[$i]["DARI"];
            if($y[$i]["STATUS"] == 'INBOUND'){
				if (substr($string,0,2) == '62') {
					$dari = '0'.substr($string,2);
				} 
				else if (substr($string,0,1) == '8') {
					$dari = '0'.$string;
				} 
				else if (substr($string,0,3) == '+62') {
					$dari = '0'.substr($string,3);
				}
				else {
					$dari = $string;
				}
			}else{
				$dari = $string;
			}
            
            $link ="https://ap4.salesforce.com/services/data/v37.0/sobjects/Log_CTI_Valdo__c/ID_Valdo__c/".$y[$i]['ID_VALDO'];
            $start_dial= date('Y-m-d', strtotime($y[$i]['START_DIALING_DATE'])).'T'.date('H:i:s', strtotime($y[$i]['START_DIALING_DATE'])).'.000+0700';
            $start_call= date('Y-m-d', strtotime($y[$i]["START_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["START_CALL_DATE"])).'.000+0700';
            $end_call = date('Y-m-d', strtotime($y[$i]["END_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["END_CALL_DATE"])).'.000+0700';
            $b = '{"Duration_Wait__c":"'.$y[$i]["DURATION_WAIT"].'","Status__c":"'.$y[$i]["STATUS"].'","SIP__c":"'.$y[$i]["SIP"].'","Start_Dialling__c" : "'.$start_dial .'","Transfer_From__c":"'.$y[$i]["TRANSFER_FROM"].'","Transfer_To__c":"'.$y[$i]["TRANSFER_TO"].'","From__c":"'.$dari.'","Recording_ID__c":"https://axasales.valdo-intl.com/recording/'.$recording_file.'","End_Calling__c":"'.$end_call.'","Start_Calling__c":"'.$start_call.'", "Duration_Call__c" : "' . $y[$i]['DURATION_CALL'] . '","Duration_Dialing__c" : "' . $y[$i]['DURATION_DIAL'] . '"}';
            $headers = array('Content-Type: application/json',"Authorization: OAuth ".$key);
            
           // $this->postTerminadaInbound($link, $b);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $b);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_exec($curl);
            curl_close($curl);
            $e = array('status_log' => 1,);
            $where = array('uniqueid' => $y[$i]['ID_VALDO']);
            $this->db->update('cdr', $e, $where);
       }
        
    }
}