<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Phone extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->library(array('asmanager', 'session', 'form_validation'));
        $this->load->model(array('mphone', 'm_cstmr'));
       // $this->is_logged_in();
    }

    function index() {
        //echo date("Y").'/'.date("m").'/'.date("h");
        $a = array();

        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');

        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            $this->form_validation->set_rules('sip_no', 'sip', 'trim|required|callback_ceklogin');
            if ($this->form_validation->run() == TRUE) {
                redirect('asterisk');
            }
        } else {

            $usercheck = $this->mphone->user($this->session->userdata('sip_no'));
            if ($usercheck[0]['active'] == "0" OR $usercheck[0]['active'] == "NULL") {
                if ($this->asmanager->connect()) {
                    $this->asmanager->Events('off');
                    $this->asmanager->QueueRemove($queue, 'SIP/' . $usercheck[0]['sip_no']);
                }$this->logout_as();
            } 
            else if ($usercheck[0]['session_id'] == session_id()) {
                $a['aux'] = $this->mphone->get_status_aux($this->session->userdata('id_agent'));
            } 
            else {
                $this->logout_as_r();
            }
        }
//        var_dump($usercheck[0]['session_id']);
//        var_dump(session_id());
       
        $this->load->view('phone', $a, FALSE);
    }

    private function _validate() {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('sip_no') == '') {
            $data['inputerror'][] = 'sip_no';
            $data['error_string'][] = '';
            $data['status'] = FALSE;
        }
        if ($this->input->post('password') == '') {
            $data['inputerror'][] = 'password';
            $data['error_string'][] = '';
            $data['status'] = FALSE;
        }
        if ($this->input->post('role') == '') {
            $data['inputerror'][] = 'role';
            $data['error_string'][] = '';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

    function ceklogin() {
        $this->_validate();
        $this->load->model('m_cstmr');
        $exists = $this->m_cstmr->session_data($this->input->post('sip_no'), md5($this->input->post('password')));
        if ($this->input->post('role') == 1) {
            if ($exists == "" || $exists == NULL) {
                $this->form_validation->set_message('ceklogin', 'SIP not found');
                echo json_encode(array("status" => TRUE));
            } else {
                $data = array(
                    'id_agent' => $exists[0]['id_agent'],
                    'username' => $exists[0]['username'],
                    'sip_no' => $exists[0]['sip_no'],
                    'queue' => $exists[0]['queue'],
                    'queue_1' => $exists[0]['queue_1'],
                    'queue_2' => $exists[0]['queue_2'],
                    'email' => $exists[0]['email'],
                    'is_logged_in' => TRUE
                );
                $this->session->set_userdata($data);
                $sip = $this->session->userdata('sip_no');
                $queue = $this->session->userdata('queue');
                $queue_1 = $this->session->userdata('queue_1');
                $queue_2 = $this->session->userdata('queue_2');
                $id_agent = $this->session->userdata('id_agent');
                $username = $this->session->userdata('username');
                
                date_default_timezone_set('asia/jakarta');
               // $this->mphone->update(array('sip_no' => $sip), array('active' => 1,'time' =>  date('Y-m-d H:i:s') ));
                $this->asteriskagentunpause();
                //$this->load->model(array('mphone', 'm_cstmr'));
                

                $agent = array('session_id' => session_id(), 'active' => 1, 'role' => 'Inbound','time' =>  date('Y-m-d H:i:s'));
                $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $agent);
                $role = $this->m_cstmr->get_data_agent($this->session->userdata('id_agent'));
                $act = array(
                    'id_agent' => $this->session->userdata('id_agent'),
                    'interface' => 'SIP/' . $this->session->userdata('sip_no'),
                    'queue' => $this->session->userdata('queue'),
                    'data' => 'By Supervisor',
                    'activity' => 'AGENT LOGIN OK',
                    'role'=>$role[0]['role']
                );
                $this->db->insert('agent_activity', $act);
                $this->loginasterisk($sip, $queue, $id_agent, $username);
                $this->loginasterisk_dua($sip, $queue_1, $id_agent, $username);
                $this->loginasterisk_tiga($sip, $queue_2, $id_agent, $username);
            }
        } else {
            if ($exists == "" || $exists == NULL) {
                $this->form_validation->set_message('ceklogin', 'SIP not found');
                //echo json_encode(array("status" => TRUE));
            } else {
                if (strlen($exists[0]['session_id']) < 7) {

                    $data = array(
                        'id_agent' => $exists[0]['id_agent'],
                        'username' => $exists[0]['username'],
                        'sip_no' => $exists[0]['sip_no'],
                        'queue' => $exists[0]['queue'],
                        'email' => $exists[0]['email'],
                        'is_logged_in' => TRUE
                    );
                    $this->session->set_userdata($data);
                    $sip = $this->session->userdata('sip_no');
                    $queue = $this->session->userdata('queue');
                    $id_agent = $this->session->userdata('id_agent');
                    $username = $this->session->userdata('sip_no');
                  
                    
                    $this->asteriskagentunpause();
                   
                    date_default_timezone_set('asia/jakarta');
                    $agent = array('session_id' => session_id(), 'active' => 1, 'role' => 'Outbound','time' =>  date('Y-m-d H:i:s'));
                    $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $agent);
                      $role = $this->m_cstmr->get_data_agent($this->session->userdata('id_agent'));
                    $act = array(
                        'id_agent' => $this->session->userdata('id_agent'),
                        'interface' => 'SIP/' . $this->session->userdata('sip_no'),
                        'queue' => $this->session->userdata('queue'),
                        'data' => 'By Supervisor',
                        'activity' => 'AGENT LOGIN OK',
                        'role'=> $role[0]['role']
                    );
                    $this->db->insert('agent_activity', $act);
                }
                if (strlen($exists[0]['session_id']) > 7) {
                    echo '<script>';
                    echo 'function myFunction() {
                                confirm("SIP sedang login di PC lain!");
                            }';
                    echo '</script>';
                    $data = array(
                        'id_agent' => $exists[0]['id_agent'],
                        'username' => $exists[0]['username'],
                        'sip_no' => $exists[0]['sip_no'],
                        'queue' => $exists[0]['queue'],
                        'queue_1' => $exists[0]['queue_1'],
                        'email' => $exists[0]['email'],
                        
                        'is_logged_in' => TRUE
                    );
                    $this->session->set_userdata($data);
                    $sip = $this->session->userdata('sip_no');
                    $queue = $this->session->userdata('queue');
                    $id_agent = $this->session->userdata('id_agent');
                    $username = $this->session->userdata('sip_no');
                    $this->asteriskagentunpause();
                    

                    $agent = array('session_id' => session_id(), 'active' => 1, 'role' => 'Outbound');
                    $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $agent);
                    
                    $act = array(
                        'id_agent' => $this->session->userdata('id_agent'),
                        'interface' => 'SIP/' . $this->session->userdata('sip_no'),
                        'queue' => $this->session->userdata('queue'),
                        'data' => 'By Supervisor',
                        'activity' => 'AGENT LOGIN OK',
                        //'role' =>$this->session->userdata('role')
                    );
                    $this->db->insert('agent_activity', $act);
                } else {
                    redirect('asterisk');
                }
            }

            //echo json_encode(array("status" => TRUE));
        }
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('asterisk');
        }
    }

    function loginasterisk($sip, $queue, $id_agent, $username) {
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $member_name = $id_agent . '.' . $username . '.' . $sip;
            $peer = $this->asmanager->QueueAdd($queue, $interface, 0, $member_name);
            
            $status = $peer['Response'];
            if ($status == "Success") {
//                $act = array(
//                    'id_agent' => $id_agent,
//                    'member_name' => $username,
//                    'interface' => $sip,
//                    'queue_name' => $queue
//                );
//                $this->db->insert('queue_members', $act);
                $activity = "AGENT LOGIN OK";
            } else {
                $activity = "AGENT LOGIN ERROR";
            }
//            $act = array(
//                'id_agent' => $id_agent,
//                'interface' => $interface,
//                'queue' => $queue,
//                'data' => 'By Supervisor',
//                'activity' => $activity
//            );
//            $this->db->insert('agent_activity', $act);
        }
    }
    function loginasterisk_dua($sip, $queue, $id_agent, $username) {
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $member_name = $id_agent . '.' . $username . '.' . $sip;
            $peer = $this->asmanager->QueueAdd($queue, $interface, 0, $member_name);
            
            $status = $peer['Response'];
            if ($status == "Success") {
//                $act = array(
//                    'id_agent' => $id_agent,
//                    'member_name' => $username,
//                    'interface' => $sip,
//                    'queue_name' => $queue
//                );
//                $this->db->insert('queue_members', $act);
                $activity = "AGENT LOGIN OK";
            } else {
                $activity = "AGENT LOGIN ERROR";
            }
//            $act = array(
//                'id_agent' => $id_agent,
//                'interface' => $interface,
//                'queue' => $queue,
//                'data' => 'By Supervisor',
//                'activity' => $activity
//            );
//            $this->db->insert('agent_activity', $act);
        }
    }
    
    function loginasterisk_tiga($sip, $queue, $id_agent, $username) {
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $member_name = $id_agent . '.' . $username . '.' . $sip;
            $peer = $this->asmanager->QueueAdd($queue, $interface, 0, $member_name);
            
            $status = $peer['Response'];
            if ($status == "Success") {
//                $act = array(
//                    'id_agent' => $id_agent,
//                    'member_name' => $username,
//                    'interface' => $sip,
//                    'queue_name' => $queue
//                );
//                $this->db->insert('queue_members', $act);
                $activity = "AGENT LOGIN OK";
            } else {
                $activity = "AGENT LOGIN ERROR";
            }
//            $act = array(
//                'id_agent' => $id_agent,
//                'interface' => $interface,
//                'queue' => $queue,
//                'data' => 'By Supervisor',
//                'activity' => $activity
//            );
//            $this->db->insert('agent_activity', $act);
        }
    }

    function logout() {
        $this->logoffasterisk();
        $this->logoffasterisk_dua();
        $this->logoffasterisk_tiga();
        $agent = array('active' => 0, 'session_id' => 'NULL', 'role' => 'Tidak Aktif','time' =>  date('Y-m-d H:i:s'));
        date_default_timezone_set('asia/jakarta');
        $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $agent);
        $this->session->sess_destroy();
        echo json_encode(array("status" => TRUE));
    }

    function logout_as() {
        //$this->logoffasterisk();
        $agent = array('active' => 0, 'session_id' => 'NULL', 'role' => 'Tidak Aktif');
        $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $agent);
        $this->session->sess_destroy();
        redirect('asterisk');
    }
    function logout_as_r() {
        //$this->logoffasterisk();
//        $agent = array('active' => 0, 'session_id' => 'NULL', 'role' => 'Tidak Aktif');
//        $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $agent);
        $this->session->sess_destroy();
        redirect('asterisk');
    }

    function ajax_update_aux() {
        date_default_timezone_set('asia/jakarta');
        $data = array('active' => 2,'time' =>  date('Y-m-d H:i:s'));
        $id = $this->session->userdata('id_agent');
        $agent = $this->mphone->get_status_aux($id);
        if ($this->input->post('aux') == 1) {
            $aux = "AGENT PAUSE MAKAN SIANG";
        } else if ($this->input->post('aux') == 2) {
            $aux = "AGENT PAUSE SHOLAT";
        } else {
            $aux = "AGENT PAUSE MEETING";
        }
        $aux_act = array(
            'activity' => $aux,
            'data' => 'By Supervisor',
            'interface' => 'SIP/' . $agent[0]['sip_no'],
            'id_agent' => $agent[0]['id_agent'],
            'queue' => $agent[0]['queue']
        );

        $this->db->insert('agent_activity', $aux_act);
        $this->mphone->update(array('id_agent' => $id), $data);

        $this->asteriskagentpause();
        echo json_encode(array("status" => TRUE));
    }

    function ajax_update_unpause() {
        
        $agent = $this->mphone->get_status_aux($this->session->userdata('id_agent'));
        $activity = $this->m_cstmr->get_daily_aux_by_agent($this->session->userdata('id_agent'));

        date_default_timezone_set('asia/jakarta');
        $datetime1 = strtotime($activity[0]['time']);
        $datetime2 = strtotime(date("Y-m-d H:i:s"));
        $interval = abs($datetime2 - $datetime1);
        $ag = $this->m_cstmr->get_data_agent($this->session->userdata('id_agent'));

        $add_time = strtotime($ag[0]['today_activity']) + $interval;
        $add_date = date('H:i:s', $add_time);
        $aux_act = array(
            'activity' => 'AGENT UNPAUSE',
            'data' => 'By Supervisor',
            'interface' => 'SIP/' . $agent[0]['sip_no'],
            'id_agent' => $agent[0]['id_agent'],
            'queue' => $agent[0]['queue']
        );
        $this->db->insert('agent_activity', $aux_act);
        $data = array('active' => 1,'time' =>  date('Y-m-d H:i:s'));
        $this->mphone->update(array('id_agent' => $this->session->userdata('id_agent')), $data);
        $this->mphone->input_daily_aux($this->session->userdata('sip_no'), $interval);
        $this->asteriskagentunpause();
        echo json_encode(array("status" => TRUE));
    }

    function logoffasterisk() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue');
        $id_agent = $this->session->userdata('id_agent');

        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $peer = $this->asmanager->QueueRemove($queue, $interface);
            $status = $peer['Response'];
            $activity = "AGENT LOGOFF OK";
            if ($status == "Success") {
                $where = array('interface' => $sip);
                $this->db->delete("queue_members", $where);
                $user_update = array('status_agent' => 0);
                $w = array('sip_no' => $sip);
                $this->db->update('agent', $user_update, $w);
                $activity = "AGENT LOGOFF OK";
            }
//            else {
//                $activity = "AGENT LOGOFF ERROR";
//            }
            $act = array(
                'id_agent' => $id_agent,
                'interface' => $interface,
                'queue' => $queue,
                'data' => 'By Supervisor',
                'activity' => $activity
                
            );
            $this->db->insert('agent_activity', $act);
        }
    }
    function logoffasterisk_dua() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue_1');
        $id_agent = $this->session->userdata('id_agent');

        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $peer = $this->asmanager->QueueRemove($queue, $interface);
            $status = $peer['Response'];
            if ($status == "Success") {
                $where = array('interface' => $sip);
                $this->db->delete("queue_members", $where);
                $user_update = array('status_agent' => 0);
                $w = array('sip_no' => $sip);
                $this->db->update('agent', $user_update, $w);
                $activity = "AGENT LOGOFF OK";
            }
//            else {
//                $activity = "AGENT LOGOFF ERROR";
//            }
//            $act = array(
//                'id_agent' => $id_agent,
//                'interface' => $interface,
//                'queue' => $queue,
//                'data' => 'By Supervisor',
//                'activity' => $activity
//            );
//            $this->db->insert('agent_activity', $act);
        }
    }
    function logoffasterisk_tiga() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue_2');
        $id_agent = $this->session->userdata('id_agent');

        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $peer = $this->asmanager->QueueRemove($queue, $interface);
            $status = $peer['Response'];
            if ($status == "Success") {
                $where = array('interface' => $sip);
                $this->db->delete("queue_members", $where);
                $user_update = array('status_agent' => 0);
                $w = array('sip_no' => $sip);
                $this->db->update('agent', $user_update, $w);
                $activity = "AGENT LOGOFF OK";
            }
//            else {
//                $activity = "AGENT LOGOFF ERROR";
//            }
//            $act = array(
//                'id_agent' => $id_agent,
//                'interface' => $interface,
//                'queue' => $queue,
//                'data' => 'By Supervisor',
//                'activity' => $activity
//            );
//            $this->db->insert('agent_activity', $act);
        }
    }

    function asteriskagentpause() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue');
        $queue_1 = $this->session->userdata('queue_1');
        $queue_2 = $this->session->userdata('queue_2');
        $id_agent = $this->session->userdata('id_agent');
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $this->asmanager->QueuePause($queue, $interface, true);
            $this->asmanager->QueuePause($queue_1, $interface, true);
            $this->asmanager->QueuePause($queue_2, $interface, true);
            $where = array('id_agent' => $id_agent);
            $data['paused'] = '1';
            $this->db->update("queue_members", $data, $where);
        }
    }

    function asteriskagentunpause() {
        $sip = $this->session->userdata('sip_no');
        $queue = $this->session->userdata('queue');
        
        $queue_1 = $this->session->userdata('queue_1');
        $queue_2 = $this->session->userdata('queue_2');
        $id_agent = $this->session->userdata('id_agent');
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $this->asmanager->QueuePause($queue, $interface, 0);
            $this->asmanager->QueuePause($queue_1, $interface, 0);
            $this->asmanager->QueuePause($queue_2, $interface, 0);
            $where = array('id_agent' => $id_agent);
            $data['paused'] = '0';
            $this->db->update("queue_members", $data, $where);
        }
      
        $username = $this->session->userdata('username');
        
//        $this->loginasterisk($sip, $queue, $id_agent, $username);
//        $this->loginasterisk_dua($sip, $queue_1, $id_agent, $username);
//        $this->loginasterisk_tiga($sip, $queue_2, $id_agent, $username);
    }

    function check_sip_exists() {
        $sip = $this->input->post('sip_no');
        $exists = $this->mphone->user($sip);
        //$count = count($exists);
        if ($exists[0]['sip_no'] == '1') {
            echo "<span class='status-not-available'> ISI SIP ! </span>";
        } else {
            if ($count > 0) {
                echo "<span class='status-not-available'> SIP sudah terdaftar</span>";
            } else {
                echo "<span class='status-available'> KTP Available</span>";
            }
        }
    }

    function logoffallbycron() {
        $secretkey = 'Logoff';
        $stval ='208ce7bcc76f0619c86c4e33c3b8c75989';
        $k = $_GET['key_token'];

        //var_dump($stval);

	//exit();
        if ($stval == $k) {
            $data = $this->mphone->get_agent_sip();
            echo 'run';
            $queue = "7000";
            for ($i = 0; $i < count($data); $i++) {
                if ($this->asmanager->connect()) {
                    $this->asmanager->Events('off');
                    $this->asmanager->QueueRemove($queue, 'SIP/' . $data[$i]['sip_no']);
                }
            }
            $this->mphone->logoffallbycron();
        } else {
            echo 'stop';
        }
    }
//    function is_logged_in() {
//        $is_logged_in = $this->session->userdata('is_logged_in');
//        if (!isset($is_logged_in) || $is_logged_in != true) {
//            redirect('');
//        }
//    }

}
