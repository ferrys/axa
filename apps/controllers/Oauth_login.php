<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Oauth_login extends REST_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('m_cstmr');
        $this->load->library(array('asmanager', 'session'));
    }

    public function index_get() {
        
    }

    public function index_post() {
       
        
        
        
        $user = array(
            'email' => $this->input->post('email')
        );
        $login = $this->m_cstmr->get_data_user($user['email']);
        
        $sip = $login[0]['SIP'];
        $queue = $login[0]['queue'];
        $id_agent= $login[0]['id_agent'];
        $username= $login[0]['username'];
        
        if ($login == NULL) {
            $this->response('GAGAL', 404);
        } 
        else 
        {
            if($login[0]['status_agent']==1)
            {
                 $this->response('Already Login', 404);
            }
            else{if($login) {
                $user_update = array('status_agent' => 1);
                $w = array('email' => $user['email']);
                $this->db->update('agent', $user_update, $w);
                $this->loginasterisk($sip,$queue,$id_agent,$username);
                $this->response('SUCCESS', 201);
            } 
            else {
                $this->response('GAGAL', 404);
            }}
        }
    }

    function loginasterisk($sip,$queue,$id_agent,$username) {

        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $member_name = $id_agent . '.' . $username . '.' . $sip;
            $peer = $this->asmanager->QueueAdd($queue, $interface, 0, $member_name);
            $status = $peer['Response'];
            if ($status == "Success") {
                $act = array(
                    'id_agent' => $id_agent,
                    'member_name' => $username,
                    'interface' => $sip,
                    'queue_name' => $queue
                );
                $this->db->insert('queue_members', $act);
                $activity = "AGENT LOGIN OK";
            } 
            else {
                $activity = "AGENT LOGIN ERROR";
            }
            $act = array(
                'id_agent' => $id_agent,
                'interface' => $interface,
                'queue' => $queue,
                'data' => 'By Supervisor',
                'activity' => $activity
            );
            $this->db->insert('agent_activity', $act);
        }
    }
}
