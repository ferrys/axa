<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Terminada_transfer extends REST_Controller {

    var $call_center;

    public function index_get() {
     
    }
    public function index_post() {}
    
     function postTerminadaInbound($url, $b) {
       
               //https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG959Nd8JMmavRnfPOYxVcw5TU0_m1Igr8xP0bZ0Yv5haAYCvhs9IhzHAEVWtETbnuCQvdI.rxBfipy8aBT&client_secret=2743732178412977155&username=valdo@mii.axa.ali.fullsb&password=aliaxa123
        $url2 = "https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=".$this->config->item('client_id_p')."&client_secret=".$this->config->item('client_secret_p')."&username=".$this->config->item('username_p')."&password=".$this->config->item('password_p')."";
        $data2 = array(
            'client_id' => $this->config->item('client_id_p'), 
            'client_secret' => $this->config->item('client_secret_p'), 
            'username' =>$this->config->item('username_p'), 
            'password' => $this->config->item('password_p'), 
            'grant_type' => 'password');
                    
        $options2 = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => http_build_query($data2),
            ),
        );
        $context2 = stream_context_create($options2);
        $result2 = json_decode(file_get_contents($url2, false, $context2));
        $header = array('Content-Type: application/json',"Authorization: OAuth ".$result2->access_token);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $b);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_exec($curl);
        curl_close($curl);
    }
    public function index_patch() {
        date_default_timezone_set('asia/jakarta');
        $this->load->model('m_cstmr');
        
         //$y = $this->m_cstmr->get_data_call_in_terminada();
      //
        $this->load->model('mxml_gen');
        $y = $this->mxml_gen->getlogctitransfer();
        $vi = $this->mxml_gen->key_sfdc();
        $key =$vi[0]['TOKEN_PRODUCTION'];
        for ($i = 0; $i < count($y); $i++) {
            //$tanggal =date("Y").'/'.date("m").'/'.date("d");calldate
            $tanggal =  date_format($y[$i]["START_DIALING_DATE"], "Y/m/d");
            if(strlen($y[$i]["RECORDING_FILE"])>75)
            {$recording_file = str_replace("/var/spool/asterisk/monitor/".$tanggal,$tanggal."", $y[$i]["RECORDING_FILE"]);}
            else{$recording_file = $tanggal."/".$y[$i]["RECORDING_FILE"];}
            
            $string = $y[$i]["DARI"];
            if($y[$i]["STATUS"] == 'INBOUND'){
				if (substr($string,0,2) == '62') {
					$dari = '0'.substr($string,2);
				} 
				else if (substr($string,0,1) == '8') {
					$dari = '0'.$string;
				} 
				else if (substr($string,0,3) == '+62') {
					$dari = '0'.substr($string,3);
				}
				else {
					$dari = $string;
				}
			}else{
				$dari = $string;
			}
            
            $link ="https://ap4.salesforce.com/services/data/v37.0/sobjects/Log_CTI_Valdo__c/ID_Valdo__c/".$y[$i]['ID_VALDO'];
            $start_dial= date('Y-m-d', strtotime($y[$i]['START_DIALING_DATE'])).'T'.date('H:i:s', strtotime($y[$i]['START_DIALING_DATE'])).'.000+0700';
            $start_call= date('Y-m-d', strtotime($y[$i]["START_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["START_CALL_DATE"])).'.000+0700';
            $end_call = date('Y-m-d', strtotime($y[$i]["END_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["END_CALL_DATE"])).'.000+0700';
            $b = '{"Duration_Wait__c":"'.$y[$i]["DURATION_WAIT"].'","Status__c":"'.$y[$i]["STATUS"].'","SIP__c":"'.$y[$i]["SIP"].'","Start_Dialling__c" : "'.$start_dial .'","Transfer_From__c":"'.$y[$i]["TRANSFER_FROM"].'","Transfer_To__c":"'.$y[$i]["TRANSFER_TO"].'","From__c":"'.$dari.'","Recording_ID__c":"https://axasales.valdo-intl.com/recording/'.$recording_file.'","End_Calling__c":"'.$end_call.'","Start_Calling__c":"'.$start_call.'", "Duration_Call__c" : "' . $y[$i]['DURATION_CALL'] . '","Duration_Dialing__c" : "' . $y[$i]['DURATION_DIAL'] . '"}';
            $headers = array('Content-Type: application/json',"Authorization: OAuth ".$key);
            
           // $this->postTerminadaInbound($link, $b);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $b);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_exec($curl);
            curl_close($curl);
            $e = array('status_log' => 1,);
            $where = array('uniqueid' => $y[$i]['ID_VALDO']);
            $this->db->update('cdr', $e, $where);
       }
        
    }
    
}