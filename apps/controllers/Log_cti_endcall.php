<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Log_cti_endcall extends REST_Controller {

    var $call_center;

    public function index_get() {
        date_default_timezone_set('asia/jakarta');
        $this->load->model('m_cstmr');
        $y = $this->m_cstmr->log_cti();
        var_dump($y);  
        exit();
    }
    public function index_post() {}
    
    public function index_patch() {
        date_default_timezone_set('asia/jakarta');
        $this->load->model('m_cstmr');
//        //$id ="1508309638.78";
//        $y = $this->m_cstmr->log_cti();
//        print_r(date_format($y[$i]['START_DIALING_DATE'],'Y/m/d'));
//        exit();
//        
               //https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG959Nd8JMmavRnfPOYxVcw5TU0_m1Igr8xP0bZ0Yv5haAYCvhs9IhzHAEVWtETbnuCQvdI.rxBfipy8aBT&client_secret=2743732178412977155&username=valdo@mii.axa.ali.fullsb&password=aliaxa123
        $url1 = "https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=".$this->config->item('client_id')."&client_secret=".$this->config->item('client_secret')."&username=".$this->config->item('username')."&password=".$this->config->item('password')."";
        $data1 = array('client_id' => $this->config->item('client_id'), 'client_secret' => $this->config->item('client_secret'), 'username' =>$this->config->item('username'), 'password' => $this->config->item('password'), 'grant_type' => 'password');
                    
        $options1 = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => http_build_query($data1),
            ),
        );
        
        $context = stream_context_create($options1);
        $result = json_decode(file_get_contents($url1, false, $context));
        
        //
        $y = $this->m_cstmr->log_cti();
      
        for ($i = 0; $i < count($y); $i++) {
            $tanggal =date("Y").'/'.date("m").'/'.date("d");
            //$tanggal1 = date_format($y[$i]['START_DIALING_DATE'],'Y/m/d');
            
            if(strlen($y[$i]["RECORDING_FILE"])>60)
            {$recording_file = str_replace("/var/spool/asterisk/monitor/".$tanggal,$tanggal,$y[$i]["RECORDING_FILE"]);}
            else{$recording_file = $tanggal."/".$y[$i]["RECORDING_FILE"];}

            $start_dial= date('Y-m-d', strtotime($y[$i]['START_DIALING_DATE'])).'T'.date('H:i:s', strtotime($y[$i]['START_DIALING_DATE'])).'.000+0700';
            $start_call= date('Y-m-d', strtotime($y[$i]["START_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["START_CALL_DATE"])).'.000+0700';
            $end_call = date('Y-m-d', strtotime($y[$i]["END_CALL_DATE"])).'T'.date('H:i:s', strtotime($y[$i]["END_CALL_DATE"])).'.000+0700';
            $b = '{"Start_Dialling__c" : "'.$start_dial .'","Transfer_From__c":"'.$y[$i]["TRANSFER_FROM"].'","Transfer_To__c":"'.$y[$i]["TRANSFER_TO"].'","From__c":"'.$y[$i]["DARI"].'","Recording_ID__c":"https://axasales.valdo-intl.com/recording/'.$recording_file.'","End_Calling__c":"'.$end_call.'","Start_Calling__c":"'.$start_call.'", "Duration_Call__c" : "' . $y[$i]['DURATION_CALL'] . '","Duration_Dialing__c" : "' . $y[$i]['DURATION_DIAL'] . '"}';
            $valdo = "https://cs57.salesforce.com/services/data/v37.0/sobjects/Log_CTI_Valdo__c/ID_Valdo__c/".$y[$i]['ID_VALDO'];
            $headers = array('Content-Type: application/json',"Authorization: OAuth ".$result->access_token);
            $curl = curl_init();
            
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_URL, $valdo);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $b);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
           
            curl_exec($curl);
            curl_close($curl);
            $e = array('status_log' => 1,);
            $where = array('uniqueid' => $y[$i]['ID_VALDO']);
            $this->db->update('cdr', $e, $where);
       }
        
    }

    function postTerminadaInbound($url, $request) {
        $url1 = "https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG959Nd8JMmavRpXnCJkKUDI9HObySw9dy5_.jr.OWhxJZNKlcDQTcd1tOD_LcREjdx4id.5g.Uim2dk6og&client_secret=9169228210180829959&username=valdo@mii.axa.ali&password=aliaxa123";
        $data1 = array('client_id' => '3MVG959Nd8JMmavRpXnCJkKUDI9HObySw9dy5_.jr.OWhxJZNKlcDQTcd1tOD_LcREjdx4id.5g.Uim2dk6og', 'client_secret' => '9169228210180829959', 'username' => 'valdo@mii.axa.ali', 'password' => 'aliaxa123', 'grant_type' => 'password');

        $options1 = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'patch',
                'content' => http_build_query($data1),
            ),
        );
        $context = stream_context_create($options1);
        $result = json_decode(file_get_contents($url1, false, $context));
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 80, // timeout on connect
            CURLOPT_TIMEOUT => 80, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Authorization: barier ".$result->access_token,
                "Content-Type: application/json"
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        curl_close($ch);
        var_dump($data);
        return $data;
    }
}