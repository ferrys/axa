<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Welcome extends REST_Controller {

    var $call_center;

    public function index_get() {
      
    }

    public function index_post() {
        $this->load->model('m_cstmr');
        $y = $this->m_cstmr->get_data_call_in();
        $this->load->database();
        $this->call_center = $this->load->database('call_center', true);
        for ($i = 0; $i < count($y); $i++) {
			
			$string = $y[$i]["callerid"];
			if (substr($string,0,2) == '62') {
				$callerid = '0'.substr($string,2);
			} 
			else if (substr($string,0,1) == '8') {
				$callerid = '0'.$string;
			}
			else if (substr($string,0,3) == '+62') {
				$callerid = '0'.substr($string,3);
			}
			else {
				$callerid = $string;
			}
            $start_dial= date('Y-m-d', strtotime($y[$i]['datetime_entry_queue'])).'T'.date('H:i:s', strtotime($y[$i]['datetime_entry_queue'])).'.000+0700';
            $b = '{"Start_Dialling__c" : "'.$start_dial .'", "From__c" : "' . $callerid . '","sip__c" : "' . $y[$i]['number'] . '","ID_Valdo__c" : "' . $y[$i]['uniqueid'] . '"}';
            $valdo ="https://ap4.salesforce.com/services/data/v37.0/sobjects/Log_CTI_Valdo__c/";
            $this->postActivaInbound($valdo, $b);
            $e = array('status_log' => 1,);
            $where = array('uniqueid' => $y[$i]['uniqueid']);
            $this->call_center->update('call_entry', $e, $where);
        }
    }

    function postActivaInbound($url, $request) {
        $this->load->model('mxml_gen');
        $vi = $this->mxml_gen->key_sfdc();
        $key =$vi[0]['TOKEN_PRODUCTION'];
        $ch = curl_init($url);
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_ENCODING => "utf-8", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 80, // timeout on connect
            CURLOPT_TIMEOUT => 80, // timeout on response
            CURLOPT_POST => 1, // i am sending post data
            CURLOPT_POSTFIELDS => $request, // this are my post vars
            CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false, //
            CURLOPT_VERBOSE => 1,
            CURLOPT_HTTPHEADER => array(
                "Authorization: OAuth ".$key,
                "Content-Type: application/json"
            )
        );
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    
    
    function postInbound($url, $request) {
       $url1 = "https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=".$this->config->item('client_id')."&client_secret=".$this->config->item('client_secret')."&username=".$this->config->item('username')."&password=".$this->config->item('password')."";
        $data1 = array('client_id' => $this->config->item('client_id'), 'client_secret' => $this->config->item('client_secret'), 'username' =>$this->config->item('username'), 'password' => $this->config->item('password'), 'grant_type' => 'password');
            
        $options1 = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => http_build_query($data1),
            ),
        );
        $context = stream_context_create($options1);
        $result = json_decode(file_get_contents($url1, false, $context));
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: OAuth '.$result->access_token,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($request)
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }
    }
}