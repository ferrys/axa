<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Phone extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->library(array('form_validation','session'));
        $this->load->helper(array('header','url'));
        $this->load->model(array('mphone','m_cstmr'));
    }

//    function index() {
//         
//        $a = array();
//        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
//        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
//        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');
//         //$a['html']['js'] .= add_js('/bootstrap/js/webphone_api.js');
//        $id = 53;
//        $a['aux'] = $this->mphone->get_status_aux($id);
//        $this->load->view('phone', $a, FALSE);
//    }
    
    function beta_call() {
         
        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');
         //$a['html']['js'] .= add_js('/bootstrap/js/webphone_api.js');
        //$id = 53;
        //$a['aux'] = $this->mphone->get_status_aux();
        $this->load->view('phone/softphone', $a, FALSE);
    }
    
    function register()
    {
        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');
        $this->load->view('phone/register', $a, FALSE);
    }
    
    function login()
    {
        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');
        $this->load->view('phone/login', $a, FALSE);
    }
    
    
}
