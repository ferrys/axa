<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auxs extends CI_Controller {

    function __construct() {
        parent:: __construct();
        $this->load->library(array('asmanager','form_validation','session'));
        $this->load->helper(array('header','url'));
        $this->load->model(array('mphone'));
    }

    function index() {
        $a = array();
        $a['html']['css'] = add_css('/bootstrap/css/bootstrap.css');
        $a['html']['js'] = add_js('/bootstrap/js/jquery.js');
        $a['html']['js'] .= add_js('/bootstrap/js/bootstrap.js');
        $this->load->view('phone', $a, FALSE);
    }
    
    function ajax_update_aux()
    {
        $data = array('status_agent' => $this->input->post('aux'));
        $id = $this->session->userdata('id_agent');
        $agent = $this->mphone->get_status_aux($id);
        //var_dump($agent);
        if($this->input->post('aux')==1)
        {
            $aux = "AGENT PAUSE MAKAN SIANG";
        }
        else if($this->input->post('aux')==2)
        {
            $aux = "AGENT PAUSE SHOLAT";
        }
        else
        {
            $aux = "AGENT PAUSE Dll";
        }
        $aux_act =array(
            'activity' => $aux,
            'data' => 'By Supervisor',
            'interface' => 'SIP/'.$agent[0]['SIP'],
            'id_agent' => $agent[0]['id_agent'],
            'queue' => $agent[0]['queue']
        );
        $this->db->insert('agent_activity', $aux_act);
        $this->mphone->update(array('id_agent' => $id), $data);
        echo json_encode(array("status" => TRUE));
    }
    function ajax_update_unpause()
    {
        $id = $this->session->userdata('id_agent');
        $data = array('status_agent' => 0);
        $agent = $this->mphone->get_status_aux( $id);

        $aux_act =array(
            'activity' => 'AGENT UNPAUSE',
            'data' => 'By Supervisor',
            'interface' => 'SIP/'.$agent[0]['SIP'],
            'id_agent' => $agent[0]['id_agent'],
            'queue' => $agent[0]['queue']
        );
        $this->db->insert('agent_activity', $aux_act);
        $this->mphone->update(array('id_agent' =>  $id), $data);
        echo json_encode(array("status" => TRUE));
    }
}