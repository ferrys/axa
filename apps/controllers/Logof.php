<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Logof extends REST_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('m_cstmr');
        $this->load->library(array('asmanager', 'session'));
        
    }

    public function index_get() {
        
    }

    
    function index_post()
    {
        $this->logoffasterisk();
//        $par=array(
//           'email' => $this->input->post('email')
//        );
//        
//        $login = $this->m_cstmr->get_data_user($par['email']);
//        $status_agent= $login[0]['status_agent'];
//        if($status_agent==0)
//        {
//            $this->response('Already Logof', 404);  
//        }
//        else
//        {
//            if($par == NULL)
//            {
//               $this->response('GAGAL', 404);  
//            }
//            else
//            {
//                $this->logoffasterisk($par['email']);
//                $this->response('SUCCESS', 201);
//            }
//        }
    }

    function logoffasterisk() {
        //$login = $this->m_cstmr->get_data_user($email);
        $sip = 777;
        $queue =7000;
        $id_agent= 55;
        if ($this->asmanager->connect()) {
            $this->asmanager->Events('off');
            $interface = 'SIP/' . $sip;
            $peer = $this->asmanager->QueueRemove($queue, $interface);
            $status = $peer['Response'];
            if ($status == "Success") {
                $where = array('queue_name' => $queue, 'interface' => $sip);
                $this->db->delete("queue_members", $where);
                $user_update = array('status_agent' => 0);
                $w = array('email' => $email);
                $this->db->update('agent', $user_update, $w);
                $activity = "AGENT LOGOFF OK";
            } 
            else 
            {
                $activity = "AGENT LOGOFF ERROR";
            }
            $act = array(
                'id_agent' => $id_agent,
                'interface' => $interface,
                'queue' => $queue,
                'data' => 'By Supervisor',
                'activity' => $activity
            );
            $this->db->insert('agent_activity', $act);
        }
       
    }

}
