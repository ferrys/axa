<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dbslave extends CI_Controller {

    var $call_center;

    function __construct() {
        parent::__construct();
        $this->load->library(array('asmanager', 'session', 'form_validation'));
        $this->load->model(array('mxml_gen', 'm_cstmr'));
    }

    function cdr() {
        $data = $this->m_cstmr->getallcdr();
        echo json_encode($data);
    }

    function cdrupdate() {
        $where = array('uniqueid' => $this->uri->segment(3));
        $e = array(
            'sync_data' => 1
        );
        $this->db->update('cdr', $e, $where);
    }

}
