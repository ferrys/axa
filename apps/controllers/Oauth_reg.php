<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Oauth_reg extends REST_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('m_cstmr');
    }
    public function index_get() 
    {
        
    }
    public function index_post() 
     {
        $user =array(
            'username' => $this->input->post('username'),
            'SIP'=> $this->input->post('SIP'),
            'email' => $this->input->post('email'),
            'queue' => '7000'
         );
        $check = $this->m_cstmr->sip_dup($user['SIP']);
        $email = $this->m_cstmr->get_data_user($user['email']);
        if($email == NULL)
        {
            if($check == NULL)
            {
                $register = $this->db->insert('agent',$user);

                if($register)
                {
                    $this->response($register,201);
                }
                else
                {
                    $this->response(NULL,404);
                }
            }
            else 
            {
                $this->response("DUPLICATE SIP",404);
            }
        }
        else
        {
            $this->response("DUPLICATE Email",404);
        }
    }
}