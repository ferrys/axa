<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once ('/var/lib/asterisk/agi-bin/phpagi-asmanager.php');

class Outbound extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_cstmr');
        //$this->load->library(array('asmanager', 'session', 'form_validation'));
    }

    function optimasi()
    {
        $this->m_cstmr->optimasi();
        $this->m_cstmr->optimasi_agent();
    }
    function repair()
    {
        $this->m_cstmr->repair();
        $this->m_cstmr->repair_agent();
    }
    
    function index() {
        $data = array("success" => false, "errors" => 'Link Not Active!');
        print_r(json_encode($data));
//        $x = $this->m_cstmr->sip_dup(substr($_GET['dari'],-3));
//        if($x[0]['active']=="0" || $x[0]['status_agent']!="0"){
//            $data = array("success"=> false,"errors"=>'Your sip not active!');
//            
//            print_r(json_encode($data));
//        }
//        else if($x[0]['active']=="1" || $x[0]['status_agent']=="0"){ $a =array();
//        if (isset($_GET['dari']) & isset($_GET['to'])) {
//            $rec_id = "";
//            $prod_code = 'API-OUT'; // API OUTBOUND
//            $line_number =  "";
//            $originator = $_GET['dari'];
//            $numbers = $_GET['to'];
//            $number = str_replace("'", "", $numbers);
//            $nama = addslashes($_GET['custname']);
//            $userid = $_REQUEST['dari'];
//            $callsegment = 'TR';
//            $server_context = 'from-internal';
//            $rec_id .= '_' . $callsegment . "_" . $userid;
//            $var1 = 'recid=' . $rec_id . ',to=' . $number . ',line_number=' . $line_number . ',prod_code=' . $prod_code . ',nama_clean=' . $nama;
//            $asm = new AGI_AsteriskManager();
//            if ($asm->connect()) {
//               $asm->send_request('Originate', 
//               array('Channel' => $originator,
//                    'Context' => $server_context,
//                    'Exten' => $number,
//                    'Priority' => 1,
//                    'Variable' => $var1,
//                    'Callerid' => $nama));
//                $r = $asm->command("core show channels concise");
//                foreach (explode("\n", $r['data']) as $s) {
//                    $cn = explode("!", $s);
//                    $stwd = strlen('SIP/' . $line_number);
//                   // preg_match_all('/[0-9]{3}[\-][0-9]{6}|[0-9]{3}[\s][0-9]{6}|[0-9]{3}[\s][0-9]{3}[\s][0-9]{4}|[0-9]{12}|[0-9]{3}[\-][0-9]{3}[\-][0-9]{4}/', $cn[7], $matches);
//                    //if ($matches[0] == $_GET['to']) {
//                   //if (substr($cn[0], 0, $stwd) == 'SIP/' . $line_number  ) {
//                       if ($cn[7] == $_GET['to'] = substr($_GET['dari'],-3)) {
//                        $a['aster_id'] = $cn[13];
//                    }
//                }
//                $asm->disconnect();
//            }
//        }
//        date_default_timezone_set('asia/jakarta');
//       
//        $a = array(
//            'name' => $_GET['custname'],
//            'SIP' => substr($_GET['dari'],-3),
//            'to' => $_GET['to'],
//            'uniqueid' => $a['aster_id'],
//            'start_call'=>date('Y-m-d H:i:s')
//        );
//        echo json_encode($a);
//        }
    }

//    function chanspy($exten, $spyexten, $pam = '')
//    {
//        global $config, $locate;
//        $myAsterisk = new Asterisk();
//        $objResponse = new xajaxResponse();
//        $myAsterisk->config['asmanager'] = $config['asterisk'];
//        $res = $myAsterisk->connect();
//        if (!$res) {
//            return;
//        }
//        $myAsterisk->chanSpy($exten, "sip/" . $spyexten, $pam, $_SESSION['asterisk']['paramdelimiter']);
//        return $objResponse;
//    }

    function chanspy() {
        $this->asmanager->connect();
        $this->asmanager->chanSpy(555, 1234, 701);
    }

//    function get_daily_data() {
//
//        $this->load->model('m_cstmr');
//        if ($_GET['date'] == "") {
//            $data = array("success" => false, "errors" => 'enter date');
//            print_r(json_encode($data));
//        }else{
//        $vi = $this->m_cstmr->get_daily_data();
//
//        $data[] = array(
//            "success" => true,
//            "errors" => [],
//            'Data' => $vi
//        );
//
//        echo json_encode($data);}
//    }
//    
//     function get_daily_data_transfer() {
//
//        $this->load->model('m_cstmr');
//        if ($_GET['date'] == "") {
//            $data = array("success" => false, "errors" => 'enter date');
//            print_r(json_encode($data));
//        }else{
//        $vi = $this->m_cstmr->get_daily_data_transfer();
//
//        $data[] = array(
//            "success" => true,
//            "errors" => [],
//            'Data' => $vi
//        );
//
//        echo json_encode($data);}
//    }
//    
    function doc()
    {
        echo "AGENTLOGOFF(channel|logintime)
The agent logged off. The channel is recorded, along with the total time
the agent was logged in.
<br><br>
AGENTCALLBACKLOGOFF(exten@context|logintime|reason)
The callback agent logged off. The last login extension and context is
recorded, along with the total time the agent was logged in, and the
reason for the logoff if it was not a normal logoff
(e.g., Autologoff, Chanunavail)
<br><br>
COMPLETEAGENT(holdtime|calltime|origposition)
The caller was connected to an agent, and the call was terminated normally
by the *agent*. The caller's hold time and the length of the call are both
recorded. The caller's original position in the queue is recorded in
origposition.
<br><br>
COMPLETECALLER(holdtime|calltime|origposition)
The caller was connected to an agent, and the call was terminated normally
by the *caller*. The caller's hold time and the length of the call are both
recorded. The caller's original position in the queue is recorded in
origposition.
<br><br>
CONFIGRELOAD
The configuration has been reloaded (e.g. with asterisk -rx reload)
<br><br>
CONNECT(holdtime|bridgedchanneluniqueid)
The caller was connected to an agent. Hold time represents the amount
of time the caller was on hold. The bridged channel unique ID contains
the unique ID of the queue member channel that is taking the call. This
is useful when trying to link recording filenames to a particular
call in the queue.
<br><br>
ENTERQUEUE(url|callerid)
A call has entered the queue. URL (if specified) and Caller*ID are placed
in the log.
<br><br>
EXITEMPTY(position|origposition|waittime)
The caller was exited from the queue forcefully because the queue had no
reachable members and it's configured to do that to callers when there
are no reachable members. The position is the caller's position in the
queue when they hungup, the origposition is the original position the
caller was when they first entered the queue, and the waittime is how
long the call had been waiting in the queue at the time of disconnect.
<br><br>
EXITWITHKEY(key|position)
The caller elected to use a menu key to exit the queue. The key and
the caller's position in the queue are recorded.
<br><br>
EXITWITHTIMEOUT(position)
The caller was on hold too long and the timeout expired.
<br><br>
QUEUESTART
The queueing system has been started for the first time this session.
<br><br>
RINGNOANSWER(ringtime)
After trying for ringtime ms to connect to the available queue member,
the attempt ended without the member picking up the call. Bad queue
member!
<br><br>
SYSCOMPAT
A call was answered by an agent, but the call was dropped because the
channels were not compatible.
<br><br>
TRANSFER(extension|context|holdtime|calltime)
Caller was transferred to a different extension. Context and extension
are recorded. The caller's hold time and the length of the call are both
recorded. PLEASE remember that transfers performed by SIP UA's by way
of a reinvite may not always be caught by Asterisk and trigger off this
event. The only way to be 100% sure that you will get this event when
a transfer is performed by a queue member is to use the built-in transfer
functionality of Asterisk.";
    }
    function get_data_e() {

        $this->load->model('m_cstmr');
       
        $vi = $this->m_cstmr->get_data_inbound();

        $data[] = array(
            "success" => true,
            "errors" => [],
            'Data Inbound' => $vi
        );

        echo json_encode($data);
    }

    function daily_report($tgl, $user = "") {
        $tgl1 = $_GET['start'];
        $tgl2 = $_GET['end'];
        $whereFilter = "";
        $wherec = "";
        if ($user != 0) {
            $hasil = $this->db->query("select id_agent from agent")->result();
            $pWhere = "AND (";
            $whr = " AND id_agent IN (";
            $i = 0;
            foreach ($hasil as $row) {
                if ($i == 0) {
                    $pWhere .= " id_user=" . $row->id_user . "";
                    $whr .= $row->id_user;
                } else {
                    $pWhere .= " OR id_user=" . $row->id_user . " ";
                    $whr .= " ," . $row->id_user . "";
                }
                $i++;
            }
            $wherec .= $whr . ")";
            $whereFilter .= $pWhere . ")";
        }

             $where = " WHERE `id_user`!='' AND `id_user` IS NOT NULL AND date(calldate) BETWEEN '$tgl1' AND '$tgl2' AND dcontext='ext-queues' AND dstchannel !=''" . $whereFilter;
 		 
/*           $where = " WHERE `id_user`!='' AND `id_user` IS NOT NULL AND `id_user` ='99' AND date(calldate) BETWEEN '$tgl1' AND '$tgl2' AND dcontext='ext-queues' AND dstchannel !=''"; */

        //$this->db = $this->load->database('asteriskcdr', true);
        $query = $this->db->query("SELECT distinct(id_user) AS ID,date(calldate) AS calldate,(CASE 
									WHEN dcontext = 'from-internal' THEN SUBSTR(channel,5,3) 
									WHEN dcontext = 'ext-queues' THEN  SUBSTR(dstchannel,5,3) 
									ELSE SUBSTR(dstchannel,5,3)  END) AS SIP ,
										SEC_TO_TIME(floor(AVG(duration_cc))) AS AHT,
										COUNT(CASE WHEN disposition LIKE 'ANSWERED' AND lastapp LIKE 'Queue' AND id_user IS NOT NULL AND dst IN('5000','5002','5003') THEN disposition END) AS TOTAL_ACD,
										SUM(CASE WHEN disposition LIKE 'ANSWERED' AND channel LIKE 'SIP/%' AND lastapp LIKE 'Queue' AND recordingfile !='' THEN (duration_cc) END) AS talktime
										
										FROM `cdr` " . $where . "
										GROUP BY id_user");
        $data = $query->result_array();
        $idx = 0;
       
        foreach ($data AS $val) {
            $sip 		= $val['SIP'];
            $calldate 	= $val['calldate'];
            $user_id 	= $val['ID'];
            $talktime 	= $val['talktime'];
				$query = $this->db->query("SELECT sum(aux_time) as DUR
											FROM `agent_aux_counter`
											WHERE sip_no = '$sip'
											AND date(`stamp`) BETWEEN '$tgl1' AND '$tgl2'");
				
                //var_dump($query->result_array());
                //exit();               
                if (count($query) > 0){
                    $agt = $query->result_array();
                    foreach ($agt as $agtrow) {
                        //$pause += $this->Timetosecons($agtrow['DUR']);
                        $pause = $agtrow['DUR'];
                    }
                }
				//var_dump($pause);
				//return false;
               
/*                  $query = $this->db->query("SELECT IFNULL(LOGOFF, '$tgl2  17:05:00') AS XLOGOFF,
										TIME_TO_SEC(TIMEDIFF(IFNULL(LOGOFF, '$tgl2  17:05:00'), IFNULL(LOGIN, '$tgl2  07:50:00'))) AS AVALDUR,count(DISTINCT(id_agent)) AS maxstaf
										FROM
										(
										SELECT a.id_agent,
										a.interface,
										a.activity,
										a.queue,
										MIN((CASE WHEN activity LIKE 'AGENT LOGIN OK%' AND date(time) BETWEEN '$tgl1' AND '$tgl2' THEN time END)) as LOGIN,
										(SELECT MAX(time) FROM agent_activity b WHERE a.id_agent = b.id_agent AND b.activity LIKE 'AGENT LOGOFF OK%' AND b.time> a.time AND date(time) BETWEEN '$tgl1' AND '$tgl2' ORDER BY time ASC LIMIT 1) AS LOGOFF
										FROM `agent_activity` a
										WHERE
										time BETWEEN '$tgl1 00:00:00' AND '$tgl2  23:59:59' AND id_agent ='$user_id'
										AND activity LIKE 'AGENT LOGIN OK%' 
									   ) grp
										GROUP BY id_agent, DATE_FORMAT(LOGIN, '%Y-%m-%d %H:%i')"); */
					$query = $this->db->query("SELECT id_agent, sip_no, sum( login_time ) AS login_time FROM `agent_login`
												WHERE id_agent ='$user_id' AND time BETWEEN '$tgl1' AND '$tgl2' GROUP BY id_agent");
					
                $avalt = 0;
                $maxstaf = 0;
                if ($query->num_rows() > 0) {
                    $agt = $query->result_array();
                    foreach ($agt as $agtrow) {
                        
                       // $avalt += $this->Timetosecons($agtrow['AVALDUR']);
					    $avalt = $agtrow['login_time'];
                        //$maxstaf = $agtrow['maxstaf'];
                      
                    }
                }
               // $data[$idx]['pause'] = $pause;
               // $data[$idx]['avaltime_real'] = $avalt;
                $data[$idx]['avaltime'] = $avalt - $talktime - $pause;
                $data[$idx]['Total_time_Staff'] = $avalt;
                $idx++;
                
                
        }

        $dat[] = array(
            "success" => true,
            "errors" => [],
            "Summary Data" => $data
        );
                
        echo json_encode($dat);

    }
	
	/////Function Ini yg dijalanin buat update agent login
	function save_daily_login(){
		$tgl1	= "2018-09-19";
		$tgl2	= "2018-09-19";
		
		//////////Kalo udh selesai dijalanin functionnya, variabel $tgl1 & $tgl2 di atas dikomen lagi, yg bawah uncomment
		//$tgl1	= date("Y-m-d");
		//$tgl2	= date("Y-m-d");
		
		$whereFilter = "";
        $wherec = "";
        if ($user != 0) {
            $hasil = $this->db->query("select id_agent from agent")->result();
            $pWhere = "AND (";
            $whr = " AND id_agent IN (";
            $i = 0;
            foreach ($hasil as $row) {
                if ($i == 0) {
                    $pWhere .= " id_user=" . $row->id_user . "";
                    $whr .= $row->id_user;
                } else {
                    $pWhere .= " OR id_user=" . $row->id_user . " ";
                    $whr .= " ," . $row->id_user . "";
                }
                $i++;
            }
            $wherec .= $whr . ")";
            $whereFilter .= $pWhere . ")";
        }
		
		$where = " WHERE `id_user`!='' AND `id_user` IS NOT NULL AND date(calldate) BETWEEN '$tgl1' AND '$tgl2' AND dcontext='ext-queues' AND dstchannel !=''" . $whereFilter;
		
		$query = $this->db->query("SELECT distinct(id_user) AS ID,date(calldate) AS calldate,(CASE 
									WHEN dcontext = 'from-internal' THEN SUBSTR(channel,5,3) 
									WHEN dcontext = 'ext-queues' THEN  SUBSTR(dstchannel,5,3) 
									ELSE SUBSTR(dstchannel,5,3)  END) AS SIP ,
										SEC_TO_TIME(floor(AVG(duration_cc))) AS AHT,
										COUNT(CASE WHEN disposition LIKE 'ANSWERED' AND lastapp LIKE 'Queue' AND id_user IS NOT NULL AND dst IN('5000','5002','5003') THEN disposition END) AS TOTAL_ACD,
										SUM(CASE WHEN disposition LIKE 'ANSWERED' AND channel LIKE 'SIP/%' AND lastapp LIKE 'Queue' AND recordingfile !='' THEN (duration_cc) END) AS talktime
										
										FROM `cdr` " . $where . "
										GROUP BY id_user");
        $data = $query->result_array();
        $idx = 0;
       
        foreach ($data AS $val) {
            $sip 		= $val['SIP'];
            $user_id 	= $val['ID'];
		
			$query = $this->db->query("SELECT IFNULL(LOGOFF, '$tgl2  17:05:00') AS XLOGOFF,
										TIME_TO_SEC(TIMEDIFF(IFNULL(LOGOFF, '$tgl2  17:05:00'), IFNULL(LOGIN, '$tgl2  07:50:00'))) AS AVALDUR,count(DISTINCT(id_agent)) AS maxstaf
										FROM
										(
										SELECT a.id_agent,
										a.interface,
										a.activity,
										a.queue,
										MIN((CASE WHEN activity LIKE 'AGENT LOGIN OK%' AND date(time) BETWEEN '$tgl1' AND '$tgl2' THEN time END)) as LOGIN,
										(SELECT MAX(time) FROM agent_activity b WHERE a.id_agent = b.id_agent AND b.activity LIKE 'AGENT LOGOFF OK%' AND b.time> a.time AND date(time) BETWEEN '$tgl1' AND '$tgl2' ORDER BY time ASC LIMIT 1) AS LOGOFF
										FROM `agent_activity` a
										WHERE
										time BETWEEN '$tgl1 00:00:00' AND '$tgl2  23:59:59' AND id_agent ='$user_id'
										AND activity LIKE 'AGENT LOGIN OK%' 
									   ) grp
										GROUP BY id_agent, DATE_FORMAT(LOGIN, '%Y-%m-%d %H:%i')");
										
			$avalt = 0;
			$maxstaf = 0;
			if ($query->num_rows() > 0) {
				$agt = $query->result_array();
				foreach ($agt as $agtrow) {
					
					//$time = date('Y-m-d'); 
					/////// Yg ini juga komen sama uncomment kalo udh selesai
					$time = "2018-09-19"; 
					$avalt = $agtrow['AVALDUR'];
					$maxstaf = $agtrow['maxstaf'];
					
					$query = $this->db->query("INSERT INTO agent_login (`id_agent`,`sip_no` , `time` , `login_time`) VALUES ('$user_id','$sip','$time','$avalt') ");
				  
				}
			}
		}
		
	}
	
    function Timetosecons($time){
		$timeArr = array_reverse(explode(":", $time));
		$seconds = 0;
		foreach ($timeArr as $key => $value)
		{
			if ($key > 2) break;
			$seconds += pow(60, $key) * $value;
		}
		return $seconds;
	}
	function secondsToTime($inputSeconds) {
		$days = floor($inputSeconds / 86400);
		$hours = floor(($inputSeconds - $days * 86400) / 3600);
		$minutes = floor(($inputSeconds - $days * 86400 - $hours * 3600) / 60);
		$seconds = floor($inputSeconds - $days * 86400 - $hours * 3600 - $minutes * 60);
	
		$days = floor($inputSeconds / 3600);
		if($inputSeconds <= 0){
			$res = "00:00:00";
		}else{
			$res = $days.":".$minutes.":".$seconds;
		}
		return $res;
	
	}
    
    function update_cdr()
    {
        $vi = $this->m_cstmr->get_id_user();
       // var_dump($vi);
        for($i=0;$i<count($vi);$i++)
        {
            $user_update = array('id_user' => $vi[$i]['id_agent']);
            $w = array('uniqueid' => $vi[$i]['uniqueid']);
            var_dump( $vi[$i]['id_agent']);
            $this->db->update('cdr', $user_update, $w);
        }
    }
    
    
     function sumarydailyreport() {
        $this->load->model('m_cstmr');
        $vi = $this->m_cstmr->sumarydailyreport();
        $data[] = array(
            "success" => true,
            "errors" => [],
            "Summary Data" => $vi
        );
        echo json_encode($data);
    }
    
    function get_interval() {
        $this->load->model('m_cstmr');
        $vi = $this->m_cstmr->get_data_interval();
        $data[] = array(
            "success" => true,
            "errors" => [],
            "Interval Data" => $vi
        );
        echo json_encode($data);
    }
	
	function get_dataviewinterval(){
		$tgl1 = $_GET['start'];
        $tgl2 = $_GET['end'];
		$jam = 15;
		$whereFilter = "";
		$wherec = "";
		//echo $tgl1." && ".$tgl2;
		//return false;
		$xinterval = $jam;
		$curminute = 0;
		$begin = date('H:i', mktime(0, 0));
		$xbegin = "";
		$cnt = 0;
		$x = 0;
		while($xbegin != '20:00' && $cnt < 5){
			$curminute = $curminute+$xinterval;
			$xbegin == "" ? $xbegin = '07:00' : '';
			$time1 = $xbegin.":00";
			$xbegin = DATE('H:i', mktime(7, $curminute));			
			//echo $xbegin;
			//return false;
			$time2 = $xbegin;
			if($xbegin == '20:00'){ $cnt++; }
			if($time2 == '20:00'){ $time2='20:00'; }
			$time2 = $time2.":00";

			$query = $this->db->query("SELECT a.tgl, CASE WHEN a.answered IS NOT NULL THEN a.answered ELSE 0 END
					+ a.abandoned AS coff, 
					CASE WHEN a.answered IS NOT NULL THEN a.answered ELSE 0 END AS acd,acd_kurang5s,acd_5sd20,acd_kurang20s,acd_kurang30s,acd_up20s, a.abandoned,abandoned_5s,abandoned_20s,abandoned_up20s, AVG_abandone, AVG_speed,AVG_acd, 
					IF(round(acd_kurang20s/(a.answered+ a.abandoned)*100,2) = 0 ,'0.00%', concat(round(acd_kurang20s/(a.answered+ a.abandoned)*100,2),'%')) as SLA20S,
					IF(round(acd_kurang30s/(a.answered+ a.abandoned)*100,2) = 0 ,'0.00%', concat(round(acd_kurang30s/(a.answered+ a.abandoned)*100,2),'%')) as SLA30S,
					IF(round(acd_kurang20s/(a.answered)*100,2) = 0,'0.00%', concat(round(acd_kurang20s/(a.answered)*100,2),'%')) as SLA20SACD
					FROM (
					SELECT DATE(ce.date_time_entry_queue) AS tgl, date_time_entry_queue AS tgls,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' THEN uniqueid END) AS answered,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc < 5 THEN uniqueid END) AS acd_kurang5s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc >= 5 AND ce.duration_wait_cc <= 20 THEN uniqueid END) AS acd_5sd20,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc <= 20 THEN uniqueid END) AS acd_kurang20s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc <= 30 THEN uniqueid END) AS acd_kurang30s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc > 20 THEN uniqueid END) AS acd_up20s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' THEN uniqueid END) AS abandoned,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' AND ce.duration_wait_cc <= 5 THEN uniqueid END) AS abandoned_5s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' AND ce.duration_wait_cc <= 20 THEN uniqueid END) AS abandoned_20s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' AND ce.duration_wait_cc > 20 THEN uniqueid END) AS abandoned_up20s,
					(SELECT SEC_TO_TIME(floor(AVG(duration_wait_cc))) FROM cdr ce WHERE ce.date_time_entry_queue BETWEEN '".$tgl1." ".$time1."' AND '".$tgl2." ".$time2."' AND lastapp='Queue' and dstchannel='') AS AVG_abandone,
					(SELECT SEC_TO_TIME(floor(AVG(datetime_init-date_time_entry_queue))) FROM cdr ce WHERE ce.date_time_entry_queue BETWEEN '".$tgl1." ".$time1."' AND '".$tgl2." ".$time2."' AND lastapp='Queue' and dstchannel!='') AS AVG_speed,
					(SELECT SEC_TO_TIME(floor(AVG(duration_cc))) FROM cdr ce WHERE ce.date_time_entry_queue BETWEEN '".$tgl1." ".$time1."' AND '".$tgl2." ".$time2."' AND lastapp='Queue' and dstchannel!='') AS AVG_acd
					FROM cdr ce WHERE ce.date_time_entry_queue BETWEEN '".$tgl1." ".$time1."' AND '".$tgl2." ".$time2."') a");	
					
			$data[$x]['jam'] = $time1.' - '.$time2;
			$result = $query->result_array();
			//var_dump($result);
			//return false;
			foreach ($result AS $val){
				$avg_abd	= $val['AVG_abandone'];
				$avg_spd	= $val['AVG_speed'];
				$avg_accd	= $val['AVG_acd'];
				
				$SLA20S		= $val['SLA20S'];
				$SLA30S		= $val['SLA30S'];
				$SLA20SACD	= $val['SLA20SACD'];
				
				$tanggal	= $val['tgl'];
				
				$data[$x]['tgl'] 				= ($tanggal == '') ? $tgl1 : $tanggal;
				$data[$x]['coff'] 				= $val['coff'];
				$data[$x]['acd'] 				= $val['acd'];
				$data[$x]['acd_kurang5s'] 		= $val['acd_kurang5s'];
				$data[$x]['acd_5sd20'] 			= $val['acd_5sd20'];
				$data[$x]['acd_kurang20s'] 		= $val['acd_kurang20s'];
				$data[$x]['acd_kurang30s'] 		= $val['acd_kurang30s'];
				$data[$x]['acd_up20s'] 			= $val['acd_up20s'];
				$data[$x]['abandoned'] 			= $val['abandoned'];
				$data[$x]['abandoned_5s'] 		= $val['abandoned_5s'];
				$data[$x]['abandoned_20s'] 		= $val['abandoned_20s'];
				$data[$x]['abandoned_up20s'] 	= $val['abandoned_up20s'];
				$data[$x]['AVG_abandone'] 		= ($avg_abd == '') ? '00:00:00' : $avg_abd;
				$data[$x]['AVG_speed'] 			= ($avg_spd == '') ? '00:00:00' : $avg_spd;
				$data[$x]['AVG_acd'] 			= ($avg_accd == '') ? '00:00:00' : $avg_accd;
				
				$data[$x]['SLA20S'] 			= ($SLA20S == '') ? '0.00%' : $SLA20S;
				$data[$x]['SLA30S'] 			= ($SLA30S == '') ? '0.00%' : $SLA30S;
				$data[$x]['SLA20SACD'] 			= ($SLA20SACD == '') ? '0.00%' : $SLA20SACD;
			}
			$x++;
			
/* 				$data2['TotalCoff'] 			+= $val['coff'];
				$data2['TotalAcd'] 				+= $val['acd'];
				$data2['TotalAcdKurang5s'] 		+= $val['acd_kurang5s'];
				$data2['TotalAcd5sd20'] 		+= $val['acd_5sd20'];
				$data2['TotalAcdKurang20s'] 	+= $val['acd_kurang20s'];
				$data2['TotalAcdKurang30s'] 	+= $val['acd_kurang30s'];
				$data2['TotalAcdUp20s'] 		+= $val['acd_up20s'];
				$data2['TotalAbandoned'] 		+= $val['abandoned'];
				$data2['TotalAbandoned5s'] 		+= $val['abandoned_5s'];
				$data2['TotalAbandoned20s'] 	+= $val['abandoned_20s'];
				$data2['TotalAbandonedUp20s'] 	+= $val['abandoned_up20s']; */
				
/* 				$data['TotalAVGAbandone'] 		= $val['AVG_abandone'];
				$data['TotalAVGSpeed'] 			= $val['AVG_speed'];
				$data['TotalAVGAcd'] 			= $val['AVG_acd'];
				
				$data['TotalAVGSLA20S'] 		= $val['SLA20S'];
				$data['TotalAVGSLA30S'] 		= $val['SLA30S'];
				$data['TotalAVGSLA20SACD'] 		= $val['SLA20SACD']; */
		}
		
 		/* $queryx = $this->db->query("SELECT a.tgl, CASE WHEN a.answered IS NOT NULL THEN a.answered ELSE 0 END
					+ a.abandoned AS coff, 
					CASE WHEN a.answered IS NOT NULL THEN a.answered ELSE 0 END AS acd,acd_kurang5s,acd_5sd20,acd_kurang20s,acd_kurang30s,acd_up20s, a.abandoned,abandoned_5s,abandoned_20s,abandoned_up20s, AVG_abandone, AVG_speed,AVG_acd, 
					IF(round(acd_kurang20s/(a.answered+ a.abandoned)*100,2) = 0 ,'0.00%', concat(round(acd_kurang20s/(a.answered+ a.abandoned)*100,2),'%')) as SLA20S,
					IF(round(acd_kurang30s/(a.answered+ a.abandoned)*100,2) = 0 ,'0.00%', concat(round(acd_kurang30s/(a.answered+ a.abandoned)*100,2),'%')) as SLA30S,
					IF(round(acd_kurang20s/(a.answered)*100,2) = 0,'0.00%', concat(round(acd_kurang20s/(a.answered)*100,2),'%')) as SLA20SACD
					FROM (
					SELECT DATE(ce.date_time_entry_queue) AS tgl, date_time_entry_queue AS tgls,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' THEN uniqueid END) AS answered,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc < 5 THEN uniqueid END) AS acd_kurang5s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc >= 5 AND ce.duration_wait_cc <= 20 THEN uniqueid END) AS acd_5sd20,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc <= 20 THEN uniqueid END) AS acd_kurang20s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc <= 30 THEN uniqueid END) AS acd_kurang30s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel!='' AND ce.duration_wait_cc > 20 THEN uniqueid END) AS acd_up20s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' THEN uniqueid END) AS abandoned,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' AND ce.duration_wait_cc <= 5 THEN uniqueid END) AS abandoned_5s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' AND ce.duration_wait_cc <= 20 THEN uniqueid END) AS abandoned_20s,
					COUNT(CASE WHEN lastapp='Queue' and dstchannel='' AND ce.duration_wait_cc > 20 THEN uniqueid END) AS abandoned_up20s,
					(SELECT SEC_TO_TIME(floor(AVG(duration_wait_cc))) FROM cdr ce WHERE date(ce.date_time_entry_queue) BETWEEN ".$tgl1." AND ".$tgl2." AND lastapp='Queue' and dstchannel='') AS AVG_abandone,
					(SELECT SEC_TO_TIME(floor(AVG(datetime_init-date_time_entry_queue))) FROM cdr ce WHERE date(ce.date_time_entry_queue) BETWEEN ".$tgl1." AND ".$tgl2." AND lastapp='Queue' and dstchannel!='') AS AVG_speed,
					(SELECT SEC_TO_TIME(floor(AVG(duration_cc))) FROM cdr ce WHERE date(ce.date_time_entry_queue) BETWEEN ".$tgl1." AND ".$tgl2." AND lastapp='Queue' and dstchannel!='') AS AVG_acd
					FROM cdr ce WHERE date(ce.date_time_entry_queue) BETWEEN ".$tgl1." AND ".$tgl2.") a");	
			$resultx = $queryx->result_array();
			foreach ($resultx AS $valx){
				$data['Total'] 					= "Total";
				$data['TotalCoff'] 				= $valx['coff'];
				$data['TotalAcd'] 				= $valx['acd'];
				$data['TotalAcdKurang5s'] 		= $valx['acd_kurang5s'];
				$data['TotalAcd5sd20'] 			= $valx['acd_5sd20'];
				$data['TotalAcdKurang20s'] 		= $valx['acd_kurang20s'];
				$data['TotalAcdKurang30s'] 		= $valx['acd_kurang30s'];
				$data['TotalAcdUp20s'] 			= $valx['acd_up20s'];
				$data['TotalAbandoned'] 		= $valx['abandoned'];
				$data['TotalAbandoned5s'] 		= $valx['abandoned_5s'];
				$data['TotalAbandoned20s'] 		= $valx['abandoned_20s'];
				$data['TotalAbandonedUp20s'] 	= $valx['abandoned_up20s'];
				
				 $data['TotalAVGAbandone'] 		= $valx['AVG_abandone'];
				$data['TotalAVGSpeed'] 			= $valx['AVG_speed'];
				$data['TotalAVGAcd'] 			= $valx['AVG_acd'];
				
				$data['TotalAVGSLA20S'] 		= $valx['SLA20S'];
				$data['TotalAVGSLA30S'] 		= $valx['SLA30S'];
				$data['TotalAVGSLA20SACD'] 		= $valx['SLA20SACD'];
			} */
		
		$data1[] = array(
            "success" => true,
            "errors" => [],
            //"Interval Data" => array_merge($data,$data2)
            "Interval Data" => $data
        );
        echo json_encode($data1);
	}

    function get_daily_aux() {
        $this->load->model('m_cstmr');
        $vi = $this->m_cstmr->get_daily_aux();
        $data[] = array(
            "success" => true,
            "errors" => [],
            "Aux" => $vi
        );
        echo json_encode($data);
    }

    function get_all_daily_activity() {
        $this->load->model('m_cstmr');
        $vi = $this->m_cstmr->get_daily_agent_activity();
        // var_dump($vi);
        $data[] = array(
            "success" => true,
            "errors" => [],
            "Activity" => $vi
        );
        echo json_encode($data);
    }

}
