/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/wallboard.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    updateWallboard();
    updateCc();
    setTimeout(function(){ updateCc(); }, 1000);
    setInterval(function(){ location.reload(); }, 300000); // refresh browser
    
    
    function updateWallboard() {
        var response = ajak('cc/wallboard/get_updates');
        var json = $.parseJSON(response);
        $('#idle').html(json.staffed);
        $('#talking').html(json.acdin);
        $('#not_active').html(json.aux);
        $('#idle2').html(json.avail);
        $('#queueing').html(json.waiting);
        setTimeout(function(){
            updateWallboard();
        },1000);
    }
	
	function updateCc(){
     var response = ajak('cc/wallboard/call_center');
     var json = $.parseJSON(response);
		$('#total').html(json.queue);
		$('#answered').html(json.answered);
		$('#abandon8').html(json.abandon8s);
		$('#abandonUn8s').html(json.abandonUn8s);
		$('#abandon').html(json.abandon);
		$('#aht').html(json.aht);
		
		if(json.abandon8s == 0){
			$('#scr').html((100).toFixed(2));
		}else{
			$('#scr').html((json.answered/(json.queue - json.abandonUn8s) * 100).toFixed(2));
		}
		
		$('#msc').html(( (json.abandonUn8s / json.queue) * 100).toFixed(2));
		$('#abd_per').html(( (json.abandon8s / json.queue) * 100).toFixed(8));
		
      setTimeout(function(){
            updateCc();
        },2000);
    }
    
    $('#pulsate-regular').pulsate({
        color: "#bf1c56"
    });
    $('#pulsate-regular1').pulsate({
        color: "#3a87ad"
    });
	
});
