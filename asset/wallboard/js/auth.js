/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : auth.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/

if (top.location!= self.location) {
	top.location = self.location.href;
}

$(document).ready(function(){
    
    $('.inp:eq(0)').val('').focus();
	$(".inp").focus(function(e) {
        //domKey($(this).attr('name'));
	}).keypress(function (e) {
  		if (e.which == 13)
  		     $('#loginbtn').click();
    });
    $('#virkey').toggle(function() {
        $('#container').slideDown('fast');
        $('.inp:eq(0)').focus();
    },function() {
        $('#container').slideUp('fast');
    });
    $('#login > a').click(function() {
        $('.inp').val('');
        return false
    });
    $('#loginbtn').click(function() {
       if ($("#input-username").val() != "" && $("#input-password").val() != "") {
			resp = ajak("auth/login",$('form').serialize());
			//alert(resp);
			if (resp == "err") {
                $('.infonya').html("Lakukan activasi software");
                return false
			}else if (resp == "nousername") {
                $('.infonya').html("Username Tidak Terdaftar");
                return false
			} else if (resp == "wrongpass") {
                $('.infonya').html("Password Anda Salah");
                return false
			} else if (resp == "noactive") {
                $('.infonya').html("Username Tidak Aktif");
                return false
			} else if (resp == "valid") {
				//logout asterisk
				respon = ajak("cc/inbound/loginasterisk");
				window.location = ".";
                //window.parent.window.location.href = '.';
			}else{
                window.location = ".";
                //window.parent.window.location.href = '.';
            }
		} else {
            $('.infonya').html("Form Harus Diisi");
            return false
        }
    });
	
    //---- Dialog Informasi
     $('#dialog-info').dialog({autoOpen: false,width: 400,modal: true,
        buttons: {
                    "Ok": function() {
                        $(this).dialog('close');
                    }
				 }
     });
	
});


