/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : crm/contact.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
   
//---- Inisialisasi
    $("#tab-utama").tabs();
    /*
     *  --------------------- Contact -----------------------------------------
     */
      //---- Dialog Tambah group
      $('#dialog-contact').dialog({autoOpen: false,width: 500,modal: true,
            buttons: {
                        "Ok": function() {
                            hasil = validform("form_contact");
                            if (hasil['isi'] != "invalid") {
                                if ($('#dialog-contact').dialog('option', 'title') == "Tambah Contact") {
                                    respon = ajak("crm/contact/saveContact",$('#form_contact').serialize());
                                } else {
                                    respon = ajak("crm/contact/editContact",$('#form_contact').serialize() + "&id=" + jAmbil("idx"));
                                }
                                if (respon == "1") {
                                    $(this).dialog("close");
                                    $("#table_contact .reset").click();
                                } else {
                                    showinfo("Error : " + respon);
                                }
                            } else {
                                showinfo("Form dengan tanda * harus Diisi");
                                hasil['focus'].focus();
                            }
                        },
                        "Batal": function() {
                            $(this).dialog('close');
                        }
        			 }
      });
      $('#addcontact').click(function() {
          $(".infonya").hide();
          $('#form_contact .inp').val('');
          $('#form_contact .inp').removeAttr('disabled');
          //isi = ajak('crm/contact/isi_crmorganization');
          //$("#form_contact select[name='crmorganization_id']").html(isi);
          $('#dialog-contact').dialog('option', 'title',  'Tambah Contact' ).dialog('open');
          return false;
      });
      $('#dialog-hapus-contact').dialog({autoOpen: false,width: 400,modal: true,
          buttons: {
                      "Ok": function() {
                          respon = ajak("crm/contact/delContact","id=" + jAmbil("idx"));
                          if (respon == "1") {
                              $("#table_contact .reset").click();;
                              $(this).dialog('close');
                           } else {
                              showinfo("Error : " + respon);
                          }
                      },
                      "Batal": function() {
                          $(this).dialog('close');
                      }
  				 }
       });
      //---- Tabel group
	  $("#table_contact").mastertable({
		  urlGet:"crm/contact/get_contact",
		  flook:"ticket_code"
	  },
	  function(hal,juml,json) {
		  var isi		="";
		  var regedit	="";
		  var hday		="";
		  for(i = 0; i < json['alldata'].length; i++) {
			  idx = "s" + json['alldata'][i].ticket_code;
			  dtx = json['alldata'][i];
			  jSimpan(idx,dtx);
			  if (json['alldata'][i].call_status == 'PENDING'){
				 regedit = "<td nowrap=\"nowrap\" align=\"center\"><img class=\"cedtgr\" title=\"Update\" src=\"assets/images/editicon.png\"/></td>"
			  }else{
				 regedit = "<td></td>"
			  }
			  
			if(json['alldata'][i].call_status == 'CLOSED'){
				if(json['alldata'][i].age_close == 0 || json['alldata'][i].age_close == null ){
					hday = '1 Day';
				}else if(json['alldata'][i].age_close == 1){
					hday = '1 Day';
				}else{
					hday = json['alldata'][i].age_close + ' Days';
				}
			}else{
				if(json['alldata'][i].age == 0){
					hday = '1 Day';
				}else if(json['alldata'][i].age == 1){
					hday = '1 Day';
				}else{
					hday = json['alldata'][i].age + ' Days';
				}
			}
			  
			  managegrp = "<img class=\"chpsg\" title=\"Hapus\" src=\"assets/images/delicon.png\"/>&nbsp;";
			  isi += "<tr>"
				  + "<td align=\"center\">" + (((hal - 1) * juml ) + (i + 1)) + "</td>"
				  + "<td align=\"center\">" + hday + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].tgl_tiket + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].ticket_name + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].wkt_tiket + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].call_status + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].ticket_tanya + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].ticket_minta + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].ticket_complain + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].ticket_desc + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].ticket_phone + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].ticket_code + "</td>"
				  + "<td align=\"center\">" + json['alldata'][i].username + "</td>"
				  //+ "<td nowrap=\"nowrap\" align=\"center\">" + managegrp + "<img  class=\"cedtgr\" title=\"Edit\" src=\"assets/images/editicon.png\"/></td>"
				  + regedit
				  + "</tr>";
		  }
		  return isi;
	  },
	  function domIsi() {
			var obj;
			var currentDate = new Date(new Date());
			var d = currentDate.getDate()
			var mm = currentDate.getMonth() + 1
			var y = currentDate.getFullYear()
			var h = currentDate.getHours()
			var m = currentDate.getMinutes()
			var sec = currentDate.getSeconds()
			
			var waktu = y + "-" + mm + "-" + d +" " + h + ":" + m + ":" + sec
			
			//--------------------- Hapus
		  $('.chpsg').click( function() {
			  $(".infonya").hide();
			  obj = jAmbil("s" + $(this).parent().next().text());
			  $('.phps').html(obj.ticket_phone);
			  jSimpan("idx",obj.ticket_code);
			  $('#dialog-hapus-contact').dialog('option', 'title',  'Hapus Contact' ).dialog('open');
			  return false;
		  });
		  //------------------------ Edit
		  $('.cedtgr').click( function() {
			  $('#form_contact input').val('');
			  $(".infonya").hide();
			  
			  //alert($(this).parent().next().text());
			  //alert($(this).parent().prev().prev().text());
			  //alert($('#table_contact').next().ticket_name);
			  
			  //obj = jAmbil("s" + $(this).parent().next().text());
			  obj = jAmbil("s" + $(this).parent().prev().prev().text());
			  //alert(obj);
			  $('#form_contact .inp:eq(0)').val(obj.ticket_name);
			  $('#form_contact .inp:eq(1)').val(obj.ticket_nopol);
			  $('#form_contact .inp:eq(2)').val(obj.ticket_phone);
			  $('#form_contact .inp:eq(3)').val(waktu);
			  $('#form_contact .inp:eq(4)').val(waktu);
			  $('#form_contact textarea').val(obj.ticket_desc);
			  $("#form_contact option[value='PENDING']").attr('selected', true);
			  jSimpan("idx",obj.ticket_code);
			  $('#dialog-contact').dialog('option', 'title',  'Update Contact' ).dialog('open');
			  return false;
		  });
		  warnatable();
	  });
      
/*
 *  ----------------------- RESET -------------------------------
 */
    $(".reset").click();
    
});