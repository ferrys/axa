/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : crm/groups.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
   
//---- Inisialisasi
    $("#tab-utama").tabs();
    /*
     *  --------------------- GROUP -----------------------------------------
     */
      //---- Dialog Tambah group
      $('#dialog-groups').dialog({autoOpen: false,width: 450,modal: true,
            buttons: {
                        "Ok": function() {
                            hasil = validform("form_groups");
                            if (hasil['isi'] != "invalid") {
                                if ($('#dialog-groups').dialog('option', 'title') == "Tambah Groups") {
                                    respon = ajak("crm/groups/saveGroup",$('#form_groups').serialize());
                                } else {
                                    respon = ajak("crm/groups/editGroup",$('#form_groups').serialize() + "&id=" + jAmbil("idx"));
                                }
                                if (respon == "1") {
                                    $(this).dialog("close");
                                    $("#table_groups .reset").click();
                                } else if (respon == "1062") {
                                    showinfo("Error : Nama Group telah ada");
                                } else {
                                    showinfo("Error : " + respon);
                                }
                            } else {
                                showinfo("Form dengan tanda * harus Diisi");
                                hasil['focus'].focus();
                            }
                        },
                        "Batal": function() {
                            $(this).dialog('close');
                        }
        			 }
      });
      $('#addgroup').click(function() {
          $(".infonya").hide();
          $('#form_groups .inp').val('');
          $('#form_groups .inp').removeAttr('disabled');
          $('#dialog-groups').dialog('option', 'title',  'Tambah Groups' ).dialog('open');
          return false;
      });
      $('#dialog-hapus-groups').dialog({autoOpen: false,width: 450,modal: true,
          buttons: {
                      "Ok": function() {
                          respon = ajak("crm/groups/delGroup","id=" + jAmbil("idx"));
                          if (respon == "1") {
                              $("#table_groups .reset").click();;
                              $(this).dialog('close');
                           } else if (respon == "1451") {
                              showinfo("Error : Group Masih Digunakan");
                           } else {
                              showinfo("Error : " + respon);
                          }
                      },
                      "Batal": function() {
                          $(this).dialog('close');
                      }
  				 }
       });
      //---- Tabel group
      $("#table_groups").mastertable({
          urlGet:"crm/groups/get_groups",
          flook:"crmgroup_id"
      },
      function(hal,juml,json) {
          var isi="";
          for(i = 0; i < json['alldata'].length; i++) {
              idx = "g" + json['alldata'][i].crmgroup_id;
              dtx = json['alldata'][i];
              jSimpan(idx,dtx);
              managegrp = (json['alldata'][i].crmgroup_name != 'Admin') ? "<img class=\"chpsg\" title=\"Hapus\" src=\"assets/images/delicon.png\"/>&nbsp;" : "";
              isi += "<tr>"
                  + "<td align=\"center\">" + (((hal - 1) * juml ) + (i + 1)) + "</td>"
                  + "<td align=\"left\">" + json['alldata'][i].crmgroup_name + "</td>"
                  + "<td align=\"left\">" + json['alldata'][i].crmgroup_desc + "</td>"
                  + "<td nowrap=\"nowrap\" align=\"center\">" + managegrp + "<img  class=\"cedtgr\" title=\"Edit\" src=\"assets/images/editicon.png\"/></td>"
                  + "<td align=\"center\">" + json['alldata'][i].crmgroup_id + "</td>"
                  + "</tr>";
          }
          return isi;
      },
      function domIsi() {
          //---- Hapus
          $('.chpsg').click( function() {
              $(".infonya").hide();
              obj = jAmbil("g" + $(this).parent().next().text());
              $('.phps').html(obj.crmgroup_name);
              jSimpan("idx",obj.crmgroup_id);
              $('#dialog-hapus-groups').dialog('option', 'title',  'Hapus Groups' ).dialog('open');
              return false;
          });
          //---- Edit
          $('.cedtgr').click( function() {
              $(".infonya").hide();
              obj = jAmbil("g" + $(this).parent().next().text());
              $('#form_groups input:eq(0)').val(obj.crmgroup_name);
              $('#form_groups textarea:eq(0)').val(obj.crmgroup_desc);
              jSimpan("idx",obj.crmgroup_id);
              $('#dialog-groups').dialog('option', 'title',  'Edit Groups' ).dialog('open');
              return false;
          });
          warnatable();
      });
      
      /*
       *  --------------------- organization -----------------------------------------
       */
        //---- Dialog Tambah organization
        $('#dialog-organization').dialog({autoOpen: false,width: 450,modal: true,
              buttons: {
                          "Ok": function() {
                              hasil = validform("form_organization");
                              if (hasil['isi'] != "invalid") {
                                  if ($('#dialog-organization').dialog('option', 'title') == "Tambah Organization") {
                                      respon = ajak("crm/groups/saveOrganization",$('#form_organization').serialize());
                                  } else {
                                      respon = ajak("crm/groups/editOrganization",$('#form_organization').serialize() + "&id=" + jAmbil("idx"));
                                  }
                                  if (respon == "1") {
                                      $(this).dialog("close");
                                      $("#table_organization .reset").click();
                                  } else {
                                      showinfo("Error : " + respon);
                                  }
                              } else {
                                  showinfo("Form dengan tanda * harus Diisi");
                                  hasil['focus'].focus();
                              }
                          },
                          "Batal": function() {
                              $(this).dialog('close');
                          }
          			 }
        });
        $('#addorganization').click(function() {
            $(".infonya").hide();
            $('#form_organization .inp').val('');
            $('#form_organization .inp').removeAttr('disabled');
            isi = ajak('crm/groups/isi_crmgroups');
            $("#form_organization select[name='crmgroup_id']").html(isi);
            $('#dialog-organization').dialog('option', 'title',  'Tambah Organization' ).dialog('open');
            return false;
        });
        $('#dialog-hapus-organization').dialog({autoOpen: false,width: 450,modal: true,
            buttons: {
                        "Ok": function() {
                            respon = ajak("crm/groups/delOrganization","id=" + jAmbil("idx"));
                            if (respon == "1") {
                                $("#table_organization .reset").click();;
                                $(this).dialog('close');
                             } else {
                                showinfo("Error : " + respon);
                            }
                        },
                        "Batal": function() {
                            $(this).dialog('close');
                        }
    				 }
        });
      //---- Tabel organization
        $("#table_organization").mastertable({
            urlGet:"crm/groups/get_organization",
            flook:"crmorganization_id"
        },
        function(hal,juml,json) {
            var isi="";
            for(i = 0; i < json['alldata'].length; i++) {
                idx = "r" + json['alldata'][i].crmorganization_id;
                dtx = json['alldata'][i];
                jSimpan(idx,dtx);
                managegrp = "<img class=\"chpsg1\" title=\"Hapus\" src=\"assets/images/delicon.png\"/>&nbsp;";
                isi += "<tr>"
                    + "<td align=\"center\">" + (((hal - 1) * juml ) + (i + 1)) + "</td>"
                    + "<td align=\"center\">" + json['alldata'][i].crmorganization_code + "</td>"
                    + "<td align=\"left\">" + json['alldata'][i].crmorganization_name + "</td>"
                    + "<td align=\"left\">" + json['alldata'][i].crmorganization_phone + "</td>"
                    + "<td align=\"left\">" + json['alldata'][i].crmorganization_web + "</td>"
                    + "<td align=\"left\">" + json['alldata'][i].crmgroup_name + "</td>"
                    + "<td nowrap=\"nowrap\" align=\"center\">" + managegrp + "<img  class=\"cedto\" title=\"Edit\" src=\"assets/images/editicon.png\"/></td>"
                    + "<td align=\"center\">" + json['alldata'][i].crmorganization_id + "</td>"
                    + "</tr>";
            }
            return isi;
        },
        function domIsi() {
            //---- Hapus
            $('.chpsg1').click( function() {
                $(".infonya").hide();
                obj = jAmbil("r" + $(this).parent().next().text());
                $('.phps').html(obj.crmorganization_name);
                jSimpan("idx",obj.crmorganization_id);
                $('#dialog-hapus-organization').dialog('option', 'title',  'Hapus Organization' ).dialog('open');
                return false;
            });
            //---- Edit
            $('.cedto').click( function() {
                $(".infonya").hide();
                obj = jAmbil("r" + $(this).parent().next().text());
                $('#form_organization .inp:eq(0)').val(obj.crmorganization_code);
                $('#form_organization .inp:eq(1)').val(obj.crmorganization_name);
                $('#form_organization .inp:eq(2)').val(obj.crmorganization_phone);
                $('#form_organization .inp:eq(3)').val(obj.crmorganization_web);
                isi = ajak('crm/groups/isi_crmgroups');
                $("#form_organization select[name='crmgroup_id']").html(isi);
                $("#form_organization select[name='crmgroup_id']").val(obj.crmgroup_id);
                jSimpan("idx",obj.crmorganization_id);
                $('#dialog-organization').dialog('option', 'title',  'Edit Organization' ).dialog('open');
                return false;
            });
            warnatable();
        });
/*
 *  ----------------------- RESET -------------------------------
 */
    $(".reset").click();
    
});