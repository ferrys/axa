/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : crm/calendar.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
   
//---- Inisialisasi
    $("#tab-utama").tabs();
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var h = {};
    var dayL = ((d < 10) ? "0" : "")+ d;
    var blnL = ((m < 10) ? "0" : "")+ m;
    loadevent(dayL+'-'+blnL+'-'+y);
    loadall();
    function loadall(){
    	
        if ($(window).width() <= 320) {
            h = {
                left: 'title, prev,next',
                center: '',
                right: 'today,month,agendaWeek,agendaDay'
            };
        } else {
            h = {
                left: 'title',
                center: '',
                right: 'prev,next'
            };
        }
	    $('#calendar').html("");
	    $('#calendar').fullCalendar({
	        header: h,
	        editable: true,
	        droppable: true, // this allows things to be dropped onto the calendar !!!
	        drop: function (date, allDay) { // this function is called when something is dropped
	
	            // retrieve the dropped element's stored Event Object
	            var originalEventObject = $(this).data('eventObject');
	            // we need to copy it, so that multiple events don't have a reference to the same object
	            var copiedEventObject = $.extend({}, originalEventObject);
	
	            // assign it the date that was reported
	            copiedEventObject.start = date;
	            copiedEventObject.allDay = allDay;
	            copiedEventObject.className = $(this).attr("data-class");
	
	            // render the event on the calendar
	            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
	            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
	
	            // is the "remove after drop" checkbox checked?
	            if ($('#drop-remove').is(':checked')) {
	                // if so, remove the element from the "Draggable Events" list
	                $(this).remove();
	            }
	        },
	        events: "crm/calendar/eventALL",
	        eventDrop: function(event, delta) {
				//alert(event.title);
			},
			
			loading: function(bool) {
				if (bool) $('#loading').show();
				else $('#loading').hide();
			}
	    });
    }
    /// dateil event
    $(".fc-sun, .fc-mon, .fc-tue, .fc-wed, .fc-thu, .fc-fri, .fc-sat, .fc-event").live('click',function(){
    	//alert($(this).find('div.fc-day-number').text());
    	n = $(this).find('div.fc-day-number').text();
    	if(n == ""){
    		n = $(this).find('div.fc-event-inner').text();
    		n = n.split('<input type="hidden" value="');
    		n = n[1].split('">');
    		n = n[0];
    	}
    	tglklick = ((n < 10) ? "0" : "")+ n;
    	elm = $('.fc-header-title').parent();
        row = $('h2',elm).text().split(" ");
    	blnklick = bulantonumber(row[0]);
    	thnklick = row[1];
    	loadevent(tglklick+'-'+blnklick+'-'+thnklick);
		$("#titleevent").html("Detail Event for "+ tglklick +" "+ row[0] +" "+ thnklick +"");
		var add = '<button class="btn btn-warning" id="cevent"><i class="icon-plus icon-white"></i> Add Event</button>';
		$("#addbtn").html(add);
		$("#cevent").click(function(){
	    	var tgln = tglklick+'-'+blnklick+'-'+thnklick;
	    	$('#form_event input').val('');
	    	$('#form_event textarea').val('');
	    	$('#form_event input:eq(0)').val(tgln);
	    	$('#clockface_1').clockface();
	    	$('#dialog-event').dialog('option', 'title',  'Tambah Event' ).dialog('open');
	    });
	});
    $("#form_event input:eq(3)").autocomplete('admin/user/search_namapeg', {
        multiple: false,
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.nip,
                    result: row.nip
                }
            });
        },
        formatItem: function(item) {
            return item.nip + ' => ' + item.nama_pegawai;
        }
	 }).result(function(e, item) {
	        $("#form_event input:eq(3)").val(item.nama_pegawai);
	        $("#form_event input:eq(4)").val(item.pegawai_id);
	 });
    $("#form_event input:eq(2)").autocomplete('crm/calendar/search_contact', {
        multiple: false,
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.contact_code,
                    result: row.contact_code
                }
            });
        },
        formatItem: function(item) {
            return item.contact_code + ' => ' + item.contact_name;
        }
	 }).result(function(e, item) {
	        $("#form_event input:eq(2)").val(item.contact_name);
	        $("#form_event input:eq(5)").val(item.contact_id);
	 });
    $('#dialog-event').dialog({autoOpen: false,width: 500,modal: true,
        buttons: {
                    "Ok": function() {
                    	hasil = validform("form_event");
                        if (hasil['isi'] != "invalid") {
                        	if ($('#dialog-event').dialog('option', 'title') == "Tambah Event") {
                                respon = ajak("crm/calendar/saveEvent",$('#form_event').serialize());
                            }
                        	if (respon == "1") {
                                loadevent($('#form_event input:eq(0)').val());
                                loadall();
                                $(this).dialog('close');
                            } else {
                                showinfo("Error : " + respon);
                            }
                        } else {
                            showinfo("Form dengan tanda * harus Diisi");
                            hasil['focus'].focus();
                        }
                    },
                    "Batal": function() {
                        $(this).dialog('close');
                    }
    			 }
     });
    function loadevent(daten){
    	$("#event_show").html("");
    	$.post("crm/calendar/eventinfo", "tgl=" + revDate(daten,'-'),
				function(obj){
    				if(obj['alldata'].length > 0){
						var isi ="";
						for(i=0;i< obj['alldata'].length;i++){
							isi += '<button id="del_'+ obj['alldata'][i].event_id +'" class="close">x &nbsp;&nbsp;&nbsp;</button>'
								+ '<div class="alert alert-success">'+ obj['alldata'][i].contact_name +' | Jam : '+ obj['alldata'][i].event_time +' | PIC : '+ obj['alldata'][i].nama_pegawai +'<br>Desc : '+ obj['alldata'][i].event_desc +'</div>';
						}
						$("#event_show").html(isi);
					}
				$(".close").click(function(){
					id = $(this).attr('id').split("del_");
					respon = ajak("crm/calendar/delEvent","id=" + id[1]);
                    if (respon == "1") {
                    	loadevent(daten);
                    	loadall();
                    }
				});
	    }, "json");
    }
    
});