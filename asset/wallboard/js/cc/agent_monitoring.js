/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/agent_monitoring.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    
    setTimeout(function(){ updateAgentStatus(); }, 1000);
    setTimeout(function(){ updateCurrentStat(); }, 1500);
    //setTimeout(function(){ updateSummary(); }, 1500);
   // setTimeout(function(){ updateAHT(); }, 1000);
    
    setInterval(function(){ location.reload(); }, 300000); // refresh browser
    
    function updateCurrentStat() {
        var response = ajak('cc/wallboard/get_updates');
        var json = $.parseJSON(response);
        $('#staffed').html(json.staffed);
        $('#acdin').html(json.acdin);
        $('#aux').html(json.aux);
        $('#other').html(json.other);
        $('#avail').html(json.avail);
        $('#waiting').html(json.waiting);
        setTimeout(function(){
            updateCurrentStat();
        },1500);
    }
    
    /* function updateSummary(){
     var response = ajak('cc/wallboard/load_monitorqueue');
     var json = $.parseJSON(response);
      $('#coff').html(json.queue);
      $('#acd').html(json.answered);
      $('#abandon').html(json.abandon);
      //$('#aht').html('..');
      $('#src').html(( (json.answered / json.queue) * 100).toFixed(2) + '%' );
      $('#abandon_per').html(( (json.abandon / json.queue) * 100).toFixed(2) + '%');
      
      setTimeout(function(){
            updateSummary();
        },60000);
    } */
    
    function updateAgentStatus(){
        var response = ajak('cc/agent_monitoring/get_updates');
        $('#agentstat_tbl tbody').empty();
        $('#agentstat_tbl tbody').append(response);
        setTimeout(function(){
           updateAgentStatus(); 
        }, 1000);
    }
    
    /* function updateAHT(){
        var response = ajak('cc/wallboard/calc_aht');
        $('#aht').html(response);
    } */
    
    
});
