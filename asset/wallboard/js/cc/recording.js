/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/recording.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
	
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    $("#ffilter input[name='pawal']").val(isitglskrg());
    $("#ffilter input[name='pakhir']").val(isitglskrg());
    
    $("#table_vlogger").mastertable({
        urlGet:"cc/recording/get_view",
        flook:"calldate",
        order:"desc"
    },
    function(hal,juml,json) {
    	$("#recorddata").hide();
        var isi="";
        if(json != null){
        	$("#recorddata").hide();
        	for(i = 0; i < json['alldata'].length; i++) {
                idx = "j" + json['alldata'][i].uniqueid;
                dtx = json['alldata'][i];
				
                jSimpan(idx,dtx);
				
				var record = json['alldata'][i].recordingfile;
                $agent_name = json['alldata'][i].clid;
                $agent_name = $agent_name.split('<');
                $dst = json['alldata'][i].dst;
                $tgl = json['alldata'][i].calldate;
                $tgl = $tgl.split(' ');
                $extagent = json['alldata'][i].channel;
                $extagent = $extagent.split('-');
				/* $recording 	= record.substr(1,3);
				alert($recording);
				exit(); */
				if(json['alldata'][i].dst == '6400'){
					var aa = json['alldata'][i].recordingfile;
					$filename = aa.substr(39);
				}else{
					if(record.substr(1,3)=='var'){
						var aa = json['alldata'][i].recordingfile;
						$filename = aa.substr(39);
					}else{
						$filename = json['alldata'][i].recordingfile;
					}
				}
				
                $calldat = json['alldata'][i].calldate;
                $date = $calldat.split(' ');
                user = respon = ajak('cc/recording/username','id='+ json['alldata'][i].id_user);
                
                //managejab = "<a href='assets/data/recording.php?file_name="+$filename+"&date="+ $date[0] +"&play=true' target='_blank' title='Play'><i class=\"icon-play\"></i></a> | <a href='assets/data/recording.php?file_name="+$filename+"&date="+ $date[0] +"&type=wav' target='_blank' title='Download'><i class=\"icon-download\"></i></i></a>";
                managejab = "<a href='assets/data/recording.php?file_name="+$filename+"&date="+ $date[0] +"&inb="+$dst+"&type=wav' target='_blank' title='Download'><i class=\"icon-download\"></i></i></a>";
                isi += "<tr style=\"vertical-align:top;\">"
                    + "<td align=\"center\">" + (((hal - 1) * juml ) + (i + 1)) + "</td>"
                    + "<td align=\"center\">" + json['alldata'][i].uniqueid + "</td>"
                    + "<td align=\"left\">" + user + "</td>"
                    + "<td align=\"center\">" + json['alldata'][i].src + "</td>"
                    + "<td align=\"left\">" + json['alldata'][i].dst + "</td>"
                    //+ "<td align=\"center\">" + json['alldata'][i].dstchannel + "</td>"
                    + "<td align=\"center\">" + revDate($tgl[0],'-') + "</td>"
                    + "<td align=\"center\">" + $tgl[1] + "</td>"
                    + "<td align=\"center\">" + secondstotime(json['alldata'][i].billsec) + "</td>"
                    + "<td nowrap=\"nowrap\" align=\"center\">" + managejab + "</td>"
                    //+ "<td align=\"center\">" + json['alldata'][i].uniqueid + "</td>"
                    + "</tr>";
            }
        }else{
        	$("#recorddata").show();
        }
        
        return isi;
    },
    function domIsi() {
        
        warnatable();
    });
    
    $('.downloadrecording').click( function() {
    	var ff = $('.jns_filter').val();
    	var ffval = $('.isi_filter').val();
    	if(ffval == ""){
    		ffval = "null";
    	}
    	var tgl1 = $("#ffilter input[name='pawal']").val();
    	var tgl2 = $("#ffilter input[name='pakhir']").val();
    	//alert('cc/recording/downloadrecordingexel/'+ff+'/'+ffval+'/'+tgl1+'/'+tgl2+'/export');
        location.href = 'cc/recording/downloadrecordingexel/'+ff+'/'+ffval+'/'+tgl1+'/'+tgl2+'/export';
        return false;
    });
    function secondstotime(d)
    {
	    d = Number(d);
	    var h = Math.floor(d / 3600);
	    var m = Math.floor(d % 3600 / 60);
	    var s = Math.floor(d % 3600 % 60);
	    return ((h > 0 ? (h >= 10 ? h : '0' + h): '00') + ':' + (m > 0 ? (m >= 10 ? m : '0' + m): '00') + ':' + (s > 0 ? (s >= 10 ? s : '0' + s): '00')  );
    }
    /*
     *  ----------------------- RESET -------------------------------
     */
     $(".reset").click();
     
});