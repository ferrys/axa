/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/activity.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
	
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
  //---- Tabel avtivity
    $("#table_vlogger").mastertable({
        urlGet:"cc/activity/get_data",
        flook:"ticket_code",
        order: "desc"
    },
    function(hal,juml,json) {
        var isi="";
        if(json != null){
	        for(i = 0; i < json['alldata'].length; i++) {
	            idx = "g" + json['alldata'][i].ticket_id;
	            dtx = json['alldata'][i];
	            jSimpan(idx,dtx);
	            managegrp = "<img class=\"cdetail\" title=\"Detail\" src=\"assets/images/add-user.png\"/>";
	            isi += "<tr>"
	                + "<td align=\"center\">" + (((hal - 1) * juml ) + (i + 1)) + "</td>"
	                + "<td align=\"left\">" + json['alldata'][i].user_name + "</td>"
	                + "<td align=\"center\">" + json['alldata'][i].ext + "</td>"
	                + "<td align=\"center\">" + json['alldata'][i].ticket_phone + "</td>"
	                + "<td align=\"center\">" + json['alldata'][i].ticket_code + "</td>"
	                + "<td align=\"left\">" + json['alldata'][i].ticket_category + "</td>"
	                + "<td align=\"left\">" + json['alldata'][i].ticket_subcategory + "</td>"
	                + "<td align=\"left\">" + json['alldata'][i].ticket_desc + "</td>"
	                + "<td nowrap=\"nowrap\" align=\"center\">" + managegrp + "</td>"
	                + "<td align=\"center\">" + json['alldata'][i].ticket_id + "</td>"
	                + "</tr>";
	        }
        }
        return isi;
    },
    function domIsi() {
    	//---- detail
        $('.cdetail').click( function() {
        	$('#form_contact input').val('');
            $(".infonya").hide();
            obj = jAmbil("g" + $(this).parent().next().text());
            $('#form_contact .inp:eq(0)').val(obj.contact_code);
            $('#form_contact .inp:eq(1)').val(obj.contact_name);
            $('#form_contact .opt:eq(0)').val(obj.contact_phone);
            $('#form_contact .opt:eq(1)').val(obj.contact_hp);
            $('#form_contact .opt:eq(2)').val(obj.contact_fax);
            $('#form_contact .opt:eq(3)').val(obj.contact_idcard);
            $('#form_contact .opt:eq(4)').val(obj.contact_email);
            $('#form_contact .inp:eq(2)').val(obj.contact_address);
            isi = ajak('crm/contact/isi_crmorganization');
            $("#form_contact select[name='crmorganization_id']").html(isi);
            $("#form_contact select[name='crmorganization_id']").val(obj.crmorganization_id);
            jSimpan("idx",obj.contact_code);
            $('#dialog-contact').dialog('option', 'title',  'Edit Contact' ).dialog('open');
            return false;
        });
        warnatable();
    });
    $('#dialog-contact').dialog({autoOpen: false,width: 450,modal: true,
        buttons: {
                    "Ok": function() {
                        hasil = validform("form_contact");
                        if (hasil['isi'] != "invalid") {
                        	respon = ajak("cc/activity/editContact",$('#form_contact').serialize() + "&id=" + jAmbil("idx"));
                            if (respon == "1") {
                                $(this).dialog("close");
                                $("#table_vlogger .reset").click();
                            } else {
                                showinfo("Error : " + respon);
                            }
                        } else {
                            showinfo("Form dengan tanda * harus Diisi");
                            hasil['focus'].focus();
                        }
                    },
                    "Batal": function() {
                        $(this).dialog('close');
                    }
    			 }
  });
/*
*  ----------------------- RESET -------------------------------
*/
  $(".reset").click();
    
});