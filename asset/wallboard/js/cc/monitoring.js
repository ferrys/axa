/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/monitoring.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
    
    setInterval(function(){
        var content = ajak('cc/monitoring/setMonitorVer2/1');
        $('#monitor_content').html(content);
    },60000);
    
    setInterval(function(){
        updateIdleTimer();
    },1000)
	
    var xdelay = 2000;
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    $(".infonya").hide();
    $('#actspy').click(function(){
    	$txt = $(this).text();
    	$txt = $txt.split(' ');
    	$txt = $txt[0].split('#');
    	respon = ajak('cc/monitoring/agentspy','ext='+ $txt[1] +'&id='+ $txt[0]);
    	showinfo(respon);
    });
    $('#whisper').click(function(){
    	$txt = $(this).text();
    	$txt = $txt.split(' ');
    	$txt = $txt[0].split('#');
    	respon = ajak('cc/monitoring/agentwhisper','ext='+ $txt[1] +'&id='+ $txt[0]);
    	showinfo(respon);
	});
    load_status();
    var temp = null;
    function load_status() {
    	$.post("cc/monitoring/operatorstatus", "",
    			function(obj){
    				var fresh_data = obj.realData;
    				var tmp = JSON.stringify(fresh_data);
                    if (temp != tmp) {
                        temp = tmp;
                        //$("#busy").html(fresh_data.busy);
                        //$("#available").html(fresh_data.available);
                    }
    		}, "json");   
    }
    load_monitor();
    load_monitorbusy();
    load_monitorqueue();
    function load_monitor(){
    	$.post("cc/monitoring/monitorinfo", "",
    			function(obj){
		    		for(i = 0; i < obj['alldata'].length; i++) {
		    			$("#online").html(obj['alldata'][0].online);
		            }
    		}, "json"); 
            
            setTimeout(function(){
                load_monitor();
            }, xdelay);
    }
    
    function load_monitorbusy(){
    	var response = ajak('cc/monitoring/monitorinfo_status');
        var json = $.parseJSON(response);
        var busy = 0;
        var idle = 0;
        $.each(json, function(idx, key){
           var img_path = "assets/images/phone_status/";
           
           if(key == 'idle'){
            img_name = 'idle_phone.png';
            idle++;
           } else if(key == 'busy'){
            img_name = 'talking_phone.png';
            $('#idletime_'+idx).html('');
            busy++;
           }
           
           var imgObj = "<img src='"+img_path+img_name+"' width='24' height='24' />";
           $('#stat_'+idx).html(imgObj); //UPDATE Phone Status 
        });
        $('#available').html(idle);
        $('#busy').html(busy);
        setTimeout(function(){
            load_monitorbusy();
        },xdelay);
    }
    
    function load_monitorqueue(){
       var response = ajak('cc/monitoring/load_monitorqueue');
       var json = $.parseJSON(response);
       var abandon = json[0].abandon;
       var answered = json[0].answered;
       var queue = json[0].queue;
       var completed = ((answered / queue)*100).toFixed(2);
       
       $('#abandon').html(abandon);
       $('#answered').html(answered);
       $('#queue').html(queue);
       $('#answered_rate').html(completed);
       
       setTimeout(function(){
            load_monitorqueue();
        },xdelay);
    }
    
    function updateIdleTimer(){
        var idleTimerObj = $('[id^="idletime_"');
        $.each(idleTimerObj, function(idx, key){
            var curvalue = $(key).html();
            if(curvalue != ''){
               var chunk = curvalue.split(':');
               var d = new Date(0, 0, 0, chunk[0], chunk[1], chunk[2], 0);
               d.setSeconds((d.getSeconds()+1));
               $(key).html(pad('00', d.getHours(), true)+':'+pad('00',d.getMinutes(), true)+':'+pad('00', d.getSeconds(), true));  
            }
        });
    }
    
    function pad(pad, str, padLeft) {
        if (typeof str === 'undefined') 
            return pad;
        if (padLeft) {
            return (pad + str).slice(-pad.length);
        } else {
            return (str + pad).substring(0, pad.length);
        }
    }
    
    $('[data-toggle="tab"]').click(function(){
       var getid = $(this).attr('href');
       $('[data-toggle="tab"]').removeClass('active');
       $(this).prev().addClass('active');
       $('[id^="tab_2_"]').removeClass('active');
       $(getid).addClass('active');
    });
    
    /**
 * $.each($('[id^="table_agentlist_"]'), function(idx, key){
 *         var tableid = $(key).attr('data-id');
 *         $(tableid).mastertable({
 *             urlGet:"cc/monitoring/get_agentBySpv/",
 *             flook:"No"
 *         }, 
 *         function(hal,juml,json){
 *             return 'HAHAHAHA';
 *         }
 *     });
 */
    
});
