/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/inbound.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
//---- pilih 
	//$('#ticket_id_driver').val('');
	//$('#ticket_name').val('');
	//$('#ticket_nopol').val('');
	//$('#ticket_phone').val('');
	$("#ticket_tanya option[value='']").attr('selected', true);
	$("#ticket_minta option[value='']").attr('selected', true);
	$("#ticket_complain option[value='']").attr('selected', true);
	$("#call_status option[value='PENDING']").attr('selected', true);
	
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    //server status
    loadserverAsterisk();
    
    loadAsteriskAgentStatus();
    
    loadpause();
    function loadserverAsterisk(){
    	respon = ajak('cc/inbound/asteriskserver');
    	if(respon == 'Success'){
    		$('#serverConnect').html("<span class='alert-success'><b>Server Connected</b></span>");
    	}else{
    		$('#serverConnect').html("<span class='alert-error'><b>Asterisk not connection....</b></span>");
    	}
    }
    // info login asterisk
    function loadInfologinAsterisk(){
    	respon = ajak('cc/inbound/loginasterisk');
    }
    // status agent
    function loadAsteriskAgentStatus(){
    	respon = ajak('cc/inbound/asteriskagentstatus');
    	if(respon == "ege:"){
    		$('#agentStatus').html("<strong class='alert-success'>Invalid Extention</strong>");
    	}else{
    		$('#agentStatus').html("<strong class='alert-success'>"+ respon +"</strong>");
    	}
    }
    setInterval(function(){ loadAsteriskAgentStatus() }, 2000);
	
    $('#agentpause').click(function(){
    	respon = ajak('cc/inbound/asteriskagentpause');
    	loadpause();
	});
    $('#agentunpause').click(function(){
    	respon = ajak('cc/inbound/asteriskagentunpause');
    	loadpause();
	});
    $('#onpause1').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause2').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause3').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause4').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause5').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
	$('#onpause6').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
	$('#onpause7').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    function loadpause(){
    	respon = ajak('cc/inbound/agentpause');
    	if(respon == 1){
    		$('#groupagentpause').hide();
    		//$('#agentpause').hide();
    		$('#agentunpause').show();
    	}else{
    		//$('#agentpause').show();
    		$('#groupagentpause').show();
    		$('#agentunpause').hide();
    	}
    }
    // aplikasi utk notifikasi
	loadnotice();
	var answer = 0;
	function loadnotice(){
		resp = ajak('cc/inbound/getAsterisk_getChannels');
		respon = ajak('sync/notice');
		/* 	if(respon =="mati"){
			//alert("Sesi Login anda berakhir mohon login kembali");
			location.href= "https://172.25.150.20/visicall/auth/logout"; // redirectnya ke logout mas edi, biar kelogout asterisknya
		} */
    	row = respon.split("#");
    	inbound  = row[6];
    	outbound = row[7];
    	ext      = row[8];
    	channels = row[9];
    	filter 	 = row[0];
    	uniqueid = row[10];
		
		//alert(inbound);
    	if(row[0] == 1){
    		// notice call
    		if(inbound != "tdial"){
				///// Ambil nomor
    			$.post("sync/clientinfo", "id=" + inbound,
        			function(obj){
        					var unique_call;
        					var logcall = ajak("sync/logcall","id="+ inbound);
							var info = "";
    						if(obj['alldata'].length == 1){
        						var info = 'Nama : '+ obj['alldata'][0].name +'<br>No Polisi : '+ obj['alldata'][0].plat_number+
                				''+logcall+'';
        					}else{
        						var info = '<br>'+
                				''+logcall+'';
        					}
    								/*$.gritter.remove(unique_call, {
    		                        	fade: true,
    		                        	speed: 'slow'
    		                    	});
    								*/
        						unique_call = $.gritter.add({
        			            	// (string | mandatory) the heading of the notification
        			                title: '<div id="callagent"><span class="label label-success"><i class="icon-phone-sign"></i></span> '+ inbound+'</div>',
        			                // (string | mandatory) the text inside the notification
        			                text: info,
        			                // (string | optional) the image to display on the left
        			                image: './assets/img/phone-sign.jpg',
        			                // (bool | optional) if you want it to fade out on its own or just sit there
        			                sticky: true,
        			                // (int | optional) the time you want it to be alive for before fading out
        			                time: '',
        			                // (string | optional) the class name you want to apply to that specific message
        			                class_name: 'my-sticky-class'
        			            });
        							
        			            $('#callagent').pulsate({
        			                color: "#dd5131",
        			                repeat: 10000
        			            });
								/////////////////////////////////////////////////commennt
        			           /*  setTimeout(function () {
        			            	resp = ajak('cc/inbound/noticeclose');
            			            if(resp == 1){
            			            	$.gritter.remove(unique_call, {
            		                        fade: true,
            		                        speed: 'slow'
            		                    });
            			            }
        		                }, 2000); */
								/////////////////////////////////////////////////
        			            //loadinbound(inbound,uniqueid);
        			            /*setTimeout(function () {
        		                    $.gritter.remove(unique_call, {
        		                        fade: true,
        		                        speed: 'slow'
        		                    });
        		                }, 60000);
        			            */
        			            $('#callanswerdid').click(function(){
        			            	$.gritter.remove(unique_call, {
        			                    fade: true,
        			                    speed: 'slow'
        			                });
        			            	//send to asterisk answerd / Hangup
        			        		respon = ajak('cc/inbound/callanswerd','channels='+channels);
        			        		//alert(respon);
        			        		// duration
        			        		$({countNum: $('#countduration').text()}).animate({countNum: 1000}, {
        			        			  duration: 1000000,
        			        			  easing:'linear',
        			        			  step: function() {
        			        			    $('#countduration').text(Math.floor(this.countNum));
        			        			  },
        			        			  complete: function() {
        			        			    $('#countduration').text(this.countNum);
        			        			  }
        			        		});
        			        		
        			        		//loadinbound(inbound,uniqueid);
        			    	    });
        			            $('#callrejectid').click(function(){
        			            	$.gritter.remove(unique_call, {
        			                    fade: true,
        			                    speed: 'slow'
        			                });
        			            	// send to asterisk reject call
        			            	respon = ajak('cc/inbound/callreject','channels='+channels);
        			    	    });
        			            
					}, "json");
    		}
    		
    		respon = ajak('sync/callupdate','ext='+ ext +'&callnumber='+ inbound +'&set=0');
    	}
    	
    	//respon email
    	if(row[1] == 1){
    		
    	}
    	//respon chat
    	if(row[2] == 1){
    		responn = ajak('cc/chat/chatnew');
            var unique_id3 = $.gritter.add({
                // (string | mandatory) the heading of the notification
                //title: '<i class="icon-bullhorn"></i> '+row[0],
                // (string | mandatory) the text inside the notification
                text: responn,
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: true,
                // (int | optional) the time you want it to be alive for before fading out
                time: '',
                // (string | optional) the class name you want to apply to that specific message
                class_name: 'my-sticky-class'
            });

            // You can have it return a unique id, this can be used to manually remove it later using
            
            setTimeout(function () {
                $.gritter.remove(unique_id3, {
                    fade: true,
                    speed: 'slow'
                });
            }, 10000);
            respon = ajak('cc/chat/chatupdate','ext='+ ext +'&set=0');
    	}
    	//respon videocall
    	if(row[3] == 1){
    		
    	}
    	//respon fax
    	if(row[4] == 1){
    		
    	}
    	//respon sms
    	if(row[5] == 1){
    		
    	}
    	
	}
    //setInterval(function(){ loadnotice() }, 2000);
    
    //setInterval(function(){ loadAsteriskAgentStatus() }, 2000);
    
	$('#form_clientcall textarea').val('');
	
/* 	$('#collerid').click(function(){
		ext = $(this).text();
		loadinbound(ext);
	}); */
	
	//loadinbound('085711255143');
	function loadinbound(phone,uniqueid){
		loadAsteriskAgentStatus();
		loadticket(phone);
		$('#form_clientcall input').val('');
		$('#form_clientcall textarea').val('');
		$("#collerid").html(phone);
		$('#collerid').pulsate({
	        color: "#51a351",
	        repeat: 10000
	    });
		//ticket info
		ticketcount = ajak('cc/inbound/ticketcount');
		$.post("cc/inbound/ticket", "id=" + phone,
				function(obj){
					var ticketcode;
					if(obj['alldata'].length < 1){
						ticketcode = ticketcount;
					}else{
						ticketcode = obj['alldata'][0].last_ticket_no;
						/* $("#form_clientcall input:eq(0)").val(obj['alldata'][0].primary_1);
						$("#form_clientcall input:eq(1)").val(obj['alldata'][0].name);
						$("#form_clientcall input:eq(2)").val(obj['alldata'][0].plat_number);
						$("#form_clientcall input:eq(4)").val(obj['alldata'][0].bill_phone_fix);
						$("#form_clientcall input:eq(5)").val(uniqueid); */
					}
					$('#codeticket').html(ticketcode);
					//$('#form_clientcall input:eq(0)').val(primary_1);
					
		}, "json");
		
    }
	// crm amtrust load url
	function CRMamtrust(CallerID){
		ajak("https://crm.secondcrm.com/safuantest/index.php?callerid="+CallerID);
	}
	//save data close ticket
	$('#saveclose').click(function(){
		//CRMamtrust('11111');
		hasil = validform("form_clientcall");
		if (hasil['isi'] != "invalid") {
			var CallerID = $('#ticket_phone').val();
			respon = ajak("cc/inbound/saveTicket",$('#form_clientcall').serialize() + "&id=close");
			if (respon == "1") {
				CRMamtrust(CallerID);
				loadticket($('#form_clientcall input:eq(0)').val());
				$('#form_clientcall input').val('');
				$('#form_clientcall textarea').val('');
				$('#codeticket').html('');
			} else {
                showinfo("Error : " + respon);
            }
        } else {
            showinfo("Form dengan tanda * harus Diisi");
            hasil['focus'].focus();
        }
    });
	//save data create ticket
	$('#savecreate').click(function(){
		hasil = validform("form_clientcall");
		if (hasil['isi'] != "invalid") {
			respon = ajak("cc/inbound/saveTicket",$('#form_clientcall').serialize() + "&id=create");
			if (respon == "1") {
				loadticket($('#form_clientcall input:eq(5)').val());
				//$('#ticket_id_driver').val('');
				//$('#ticket_name').val('');
				//$('#ticket_nopol').val('');
				//$('#ticket_phone').val('');
				$('#form_clientcall textarea').val('');
				$("#ticket_tanya option[value='']").attr('selected', true);
				$("#ticket_minta option[value='']").attr('selected', true);
				$("#ticket_complain option[value='']").attr('selected', true);
				$("#call_status option[value='PENDING']").attr('selected', true);
				$('#codeticket').html('');
				location.reload();
            } else {
                showinfo("Error : " + respon);
            }
        } else {
            showinfo("Form dengan tanda * harus Diisi");
            hasil['focus'].focus();
        }
    });
	//load ticket history
	function loadticket(phone){
		$("#tb_view").html("");
        $.post("cc/inbound/tickethistory", "id=" + phone,
        		function(obj){
		        	var isi ="";
		            for(i = 0; i < obj['alldata'].length; i++) {
		            	if(obj['alldata'][i].status == 0){
		            		$status = "<span class=\"label label-important\">Open</span>";
		            	}else{
		            		$status = "<span class=\"label label-success\">Close</span>";
		            	}
		            	tgltime = obj['alldata'][i].ticket_timestamp;
		            	row = tgltime.split(" ");
		                isi += "<tr>"
		                    + "<td align=\"center\">"+ obj['alldata'][i].ticket_code +"</td>" 
		                    + "<td>"+ obj['alldata'][i].ticket_phone +"</td>" 
		                    + "<td>"+ obj['alldata'][i].ticket_category +"</td>" 
		                    + "<td>"+ obj['alldata'][i].ticket_subcategory +"</td>" 
		                    + "<td>"+ obj['alldata'][i].ticket_desc +"</td>" 
		                    + "<td>"+ row[1] +' '+ revDate(row[0],'-') +"</td>" 
		                    + "<td>"+ $status +"</td>" 
		                    + "</tr>";
		            }  
		            $("#tb_view").html(isi);
		}, "json");
        return false;
	}
	loadhistoryagent();
	function loadhistoryagent(){
		$("#tb_view1").html("");
        $.post("cc/inbound/historyagent", "id=",
        		function(obj){
		        	var isi ="";
		            for(i = 0; i < obj['alldata'].length; i++) {
		            	var res = obj['alldata'][i].dstchannel;
		            	res = res.split('-');
		            	isi += "<tr>"
		                    + "<td align=\"center\">"+ res[0] +"</td>" 
		                    + "<td align=\"center\">"+ obj['alldata'][i].cnum +"</td>" 
		                    + "<td align=\"center\">"+ obj['alldata'][i].billsec +" sec</td>" 
		                    + "<td align=\"center\">"+ obj['alldata'][i].calldate +" sec</td>" 
		                    //+ "<td align=\"center\">"+ obj['alldata'][i].disposition +"</td>" 
		                    + "</tr>";
		            }  
		            $("#tb_view1").html(isi);
		}, "json");
        return false;
	}
	//acw
	$('#takebreack2').click(function(){
		respon = ajak("cc/inbound/logoffasterisk");
	});
	//aux
	$('#takebreack3').click(function(){
		respon = ajak("cc/inbound/loginasterisk");
	});
	
	// transfer call
	$("#transfercall").live('click',function(){
    	ext = $(this).text();
    	//alert('transfer to : '+ ext);
    	respon = ajak("cc/inbound/transferCall","&ext="+ext);
    	
	});
	// callstop
	$("#callstop").live('click',function(){
		loadAsteriskAgentStatus();
		$('#countduration').html('0');
		$({countNum: $('#countduration').text()}).animate({countNum: 1000}, {
			  duration: 1000000,
			  easing:'linear',
			  step: function() {
			    $('#countduration').text(0);
			  },
			  complete: function() {
			    $('#countduration').text(0);
			  }
		});
		$('#collerid').pulsate({
	        color: "#51a351",
	        repeat: 1
	    });
		$('#form_clientcall input').val('');
		$('#form_clientcall textarea').val('');
		$('#codeticket').html('');
		$("#collerid").html('<i class="icon-phone-sign"></i>');
		
	});
	loadhold(0);
	// acttotone On
	$("#acttoneOn").live('click',function(){
		respon = ajak("cc/inbound/toholdON");
		loadhold(1);
	});
	// acttotone Off
	$("#acttoneOff").live('click',function(){
		respon = ajak("cc/inbound/toholdOFF");
		loadhold(0);
	});
	function loadhold($s){
		if($s == 0){
			$('#acttoneOff').hide();
			$('#acttoneOn').show();
		}else{
			$('#acttoneOn').hide();
			$('#acttoneOff').show();
		}
	}
	// chat
	//loadchatuserol();
	function loadchatuserol(){
		respon = ajak("cc/inbound/chatuserall");
		$('#idonline').html(respon);
		respon1 = ajak("cc/inbound/chatgroupall");
		$('#idonline1').html(respon1);
	}
	$("#chatact").live('click',function(){
		user = $(this).text();
		$("#titleevent").html(" to "+ user);
		loadchatto(0,user);
	});
	$("#chatactgroup").live('click',function(){
		groupname = $(this).text();
		$("#titleevent").html(" to "+ groupname);
		loadchatto(1,groupname);
	});
	function loadchatto(type,id){
		respon = ajak("cc/inbound/chatto",'id='+id+'&type='+type);
		from = ajak("cc/inbound/chatfrom");
		$("#chatslog").html(respon);
		$("#from").val(from);
		$("#to").val(id);
		$("#type").val(type);
	}
	$("#message").val('');
	$("#btnchats").live('click',function(){
		$from = $("#from").val();
		$to = $("#to").val();
		$type = $("#type").val();
		$message = $("#message").val();
		respon = ajak("cc/inbound/chats",'from='+$from+'&to='+$to+'&type='+$type+'&msg='+$message);
		if(respon == 1){
			$("#message").val('');
			loadchatto($type,$to);
			// sent notice
		}
	});
	$("#message").keypress(function (e) {
		if (e.which == 13) {
			$from = $("#from").val();
			$to = $("#to").val();
			$type = $("#type").val();
			$message = $("#message").val();
			respon = ajak("cc/inbound/chats",'from='+$from+'&to='+$to+'&type='+$type+'&msg='+$message);
			if(respon == 1){
				$("#message").val('');
				loadchatto($type,$to);
				// sent notice
			}
        }
    });
	
	//---- Autocomplete Nopol ----//
    $("#form_clientcall input:eq(2)").autocomplete('cc/inbound/search_nopol', {
            multiple: false,
            parse: function(data) {
                return $.map(eval(data), function(row) {
                    return {
                        data: row,
                        value: row.plat_number,
                        result: row.plat_number
                    }
                });
            },
            formatItem: function(item) {
                return item.plat_number + '<br />' + item.name;
            }
     }).result(function(e, item) {
            $("#form_clientcall input:eq(0)").val(item.primary_1);
            $("#form_clientcall input:eq(1)").val(item.name);
            $("#form_clientcall input:eq(2)").val(item.plat_number);
            $("#form_clientcall input:eq(4)").val(item.bill_phone_fix);
     });
	 
	 //---- Autocomplete Nama Driver ----//
    $("#form_clientcall input:eq(1)").autocomplete('cc/inbound/search_driver', {
            multiple: false,
            parse: function(data) {
                return $.map(eval(data), function(row) {
                    return {
                        data: row,
                        value: row.name,
                        result: row.name
                    }
                });
            },
            formatItem: function(item) {
                return item.name + '<br />' + item.plat_number;
            }
     }).result(function(e, item) {
            $("#form_clientcall input:eq(0)").val(item.primary_1);
            $("#form_clientcall input:eq(1)").val(item.name);
            $("#form_clientcall input:eq(2)").val(item.plat_number);
            $("#form_clientcall input:eq(4)").val(item.bill_phone_fix);
     });
	 
	 	 //---- Autocomplete Nomor Telepon ----//
    $("#form_clientcall input:eq(4)").autocomplete('cc/inbound/search_cellular', {
            multiple: false,
            parse: function(data) {
                return $.map(eval(data), function(row) {
                    return {
                        data: row,
                        value: row.bill_phone_fix,
                        result: row.bill_phone_fix
                    }
                });
            },
            formatItem: function(item) {
                return item.bill_phone_fix + '<br />' + item.name;
            }
     }).result(function(e, item) {
            $("#form_clientcall input:eq(0)").val(item.primary_1);
            $("#form_clientcall input:eq(1)").val(item.name);
            $("#form_clientcall input:eq(2)").val(item.plat_number);
            $("#form_clientcall input:eq(4)").val(item.bill_phone_fix);
     });
	
/*
 *  ----------------------- RESET -------------------------------
 */
    $(".reset").click();
    $.ajaxSetup({ cache: false });
});