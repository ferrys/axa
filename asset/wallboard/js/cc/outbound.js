/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/outbound.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
	
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
  //server status
    loadserverAsterisk();
    //loadInfologinAsterisk();
    loadAsteriskAgentStatus();
    loadpause();
    function loadserverAsterisk(){
    	respon = ajak('cc/outbound/asteriskserver');
    	if(respon == 'Success'){
    		$('#serverConnect').html("<span class='alert-success'><b>Server Connected</b></span>");
    	}else{
    		$('#serverConnect').html("<span class='alert-error'><b>Asterisk not connection....</b></span>");
    	}
    }
    // info login asterisk
    function loadInfologinAsterisk(){
    	respon = ajak('cc/outbound/loginasterisk');
    }
    // status agent
    function loadAsteriskAgentStatus(){
    	respon = ajak('cc/outbound/asteriskagentstatus');
    	if(respon == "ege:"){
    		$('#agentStatus').html("<strong class='alert-success'>Invalid Extention</strong>");
    	}else{
    		$('#agentStatus').html("<strong class='alert-success'>"+ respon +"</strong>");
    	}
    }
    $('#agentpause').click(function(){
    	respon = ajak('cc/inbound/asteriskagentpause');
    	loadpause();
	});
    $('#agentunpause').click(function(){
    	respon = ajak('cc/inbound/asteriskagentunpause');
    	loadpause();
	});
    $('#onpause1').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause2').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause3').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause4').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    $('#onpause5').click(function(){
    	ff = $(this).text();
    	respon = ajak('cc/inbound/asteriskagentpause','ff='+ ff);
    	loadpause();
	});
    function loadpause(){
    	respon = ajak('cc/inbound/agentpause');
    	if(respon == 1){
    		$('#groupagentpause').hide();
    		//$('#agentpause').hide();
    		$('#agentunpause').show();
    	}else{
    		//$('#agentpause').show();
    		$('#groupagentpause').show();
    		$('#agentunpause').hide();
    	}
    }
    //acw
	$('#takebreack2').click(function(){
		respon = ajak("cc/outbound/logoffasterisk");
	});
	//aux
	$('#takebreack3').click(function(){
		respon = ajak("cc/outbound/loginasterisk");
	});
	
	// transfer call
	$("#transfercall").live('click',function(){
    	ext = $(this).text();
    	//alert('transfer to : '+ ext);
    	respon = ajak("cc/outbound/transferCall","&ext="+ext);
    	
	});
	loadhold(0);
	// acttotone On
	$("#acttoneOn").live('click',function(){
		respon = ajak("cc/outbound/toholdON");
		loadhold(1);
	});
	// acttotone Off
	$("#acttoneOff").live('click',function(){
		respon = ajak("cc/outbound/toholdOFF");
		loadhold(0);
	});
	function loadhold($s){
		if($s == 0){
			$('#acttoneOff').hide();
			$('#acttoneOn').show();
		}else{
			$('#acttoneOn').hide();
			$('#acttoneOff').show();
		}
	}
	$('#form_clientcall input').val('');
	$('#form_clientcall textarea').val('');
	$('#form_clientcall select').val('');
	
	$('#callphone').click(function(){
		phone = $('#form_clientcall input:eq(0)').val();
		if(phone == ""){
			showinfo("Call number not found");
			$('#form_clientcall input:eq(0)').focus();
		}else{
			$("#collerid").html(phone);
			$('#collerid').pulsate({
		        color: "#51a351",
		        repeat: 10000
		    });
			//loadoutbound(phone);
			loadnotice(phone);
		}
	});
	$('#callhp').click(function(){
		phone = $('#form_clientcall input:eq(1)').val();
		if(phone == ""){
			showinfo("Call number not found");
			$('#form_clientcall input:eq(1)').focus();
		}else{
			$("#collerid").html(phone);
			$('#collerid').pulsate({
		        color: "#51a351",
		        repeat: 10000
		    });
			//loadoutbound(phone);
			loadnotice(phone);
		}
	});
	function loadnotice(phone){
		//send to asterisk answerd
		respon = ajak('cc/outbound/click2call','phone='+phone);
		showinfo(respon);
		// duration
		$({countNum: $('#countduration').text()}).animate({countNum: 1000}, {
			  duration: 1000000,
			  easing:'linear',
			  step: function() {
			    $('#countduration').text(Math.floor(this.countNum));
			  },
			  complete: function() {
			    $('#countduration').text(this.countNum);
			  }
		});
		$.post("sync/clientinfo", "id=" + phone,
				function(obj){
					var logcall = ajak("sync/logcall","id="+ phone);
					if(obj['alldata'].length == 1){
						var info = 'Code : '+ obj['alldata'][0].contact_code+'<br>Name : '+ obj['alldata'][0].contact_name +
			        				''+logcall+'<br><button id="callstop" class="btn btn-small btn-danger ccall"><i class="icon-remove icon-white"></i> Stop</button> ';
					}else{
						var info = 'Data Not Found<br>'+
						''+logcall+'<br> <button id="callstop" class="btn btn-small btn-danger ccall"><i class="icon-remove icon-white"></i> Stop</button> ';
					}
					var unique_call = $.gritter.add({
			        	// (string | mandatory) the heading of the notification
			            title: '<div id="callagent"><span class="label label-success"><i class="icon-phone-sign"></i></span> '+ phone+'</div>',
			            // (string | mandatory) the text inside the notification
			            text: info,
			            // (string | optional) the image to display on the left
			            image: './assets/img/phone-sign.jpg',
			            // (bool | optional) if you want it to fade out on its own or just sit there
			            sticky: true,
			            // (int | optional) the time you want it to be alive for before fading out
			            time: '',
			            // (string | optional) the class name you want to apply to that specific message
			            class_name: 'my-sticky-class'
			        });
			        $('#callagent').pulsate({
			            color: "#dd5131",
			            repeat: 10000
			        });
			        $('.ccall').click(function(){
		            	$.gritter.remove(unique_call, {
		                    fade: true,
		                    speed: 'slow'
		                });
			        });
			     // callstop
			    	$("#callstop").live('click',function(){
			    		$('#countduration').html('0');
			    		$({countNum: $('#countduration').text()}).animate({countNum: 1000}, {
			    			  duration: 1000000,
			    			  easing:'linear',
			    			  step: function() {
			    			    $('#countduration').text(0);
			    			  },
			    			  complete: function() {
			    			    $('#countduration').text(0);
			    			  }
			    		});
			    		$("#collerid").html('<i class="icon-phone-sign"></i>');
			    		//$('#form_clientcall input').val('');
			    		//$('#form_clientcall textarea').val('');
			    		$('#collerid').pulsate({
			    	        color: "#51a351",
			    	        repeat: 1
			    	    });
			    		//$('#codeticket').html('');
			    		respon = ajak('cc/outbound/callreject');
			    	});
		}, "json");
	}
	$('#collerid').click(function(){
		ext = $(this).text();
		loadoutbound(ext);
	});
	$("#form_clientcall input:eq(0)").autocomplete('cc/outbound/search_contactphone', {
        multiple: false,
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.contact_code,
                    result: row.contact_code
                }
            });
        },
        formatItem: function(item) {
            return item.contact_phone;
        }
	 }).result(function(e, item) {
	        $('#form_clientcall input:eq(0)').val(item.contact_phone);
	        $('#form_clientcall input:eq(1)').val(item.contact_code);
			$('#form_clientcall input:eq(2)').val(item.contact_idcard);
			//$('#form_clientcall input:eq(1)').val(item.contact_hp);
			$('#form_clientcall input:eq(3)').val(item.contact_name);
			$('#form_clientcall textarea:eq(0)').val(item.contact_address);
			//$('#form_clientcall input:eq(8)').val(item.crmorganization_id);
			$('#form_clientcall select:eq(0)').val(item.contact_type);
	 });
	$("#form_clientcall input:eq(1)").autocomplete('cc/outbound/search_contactcell', {
        multiple: false,
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.contact_code,
                    result: row.contact_code
                }
            });
        },
        formatItem: function(item) {
            return item.contact_code;
        }
	 }).result(function(e, item) {
		 	$('#form_clientcall input:eq(0)').val(item.contact_phone);
	        $('#form_clientcall input:eq(1)').val(item.contact_code);
			$('#form_clientcall input:eq(2)').val(item.contact_idcard);
			//$('#form_clientcall input:eq(1)').val(item.contact_hp);
			$('#form_clientcall input:eq(3)').val(item.contact_name);
			$('#form_clientcall textarea:eq(0)').val(item.contact_address);
			//$('#form_clientcall input:eq(8)').val(item.crmorganization_id);
			$('#form_clientcall select:eq(0)').val(item.contact_type);
	 });
	$("#form_clientcall input:eq(2)").autocomplete('cc/outbound/search_contactid', {
        multiple: false,
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.contact_code,
                    result: row.contact_code
                }
            });
        },
        formatItem: function(item) {
            return item.contact_code;
        }
	 }).result(function(e, item) {
		 $('#form_clientcall input:eq(0)').val(item.contact_phone);
	        $('#form_clientcall input:eq(1)').val(item.contact_code);
			$('#form_clientcall input:eq(2)').val(item.contact_idcard);
			//$('#form_clientcall input:eq(1)').val(item.contact_hp);
			$('#form_clientcall input:eq(3)').val(item.contact_name);
			$('#form_clientcall textarea:eq(0)').val(item.contact_address);
			//$('#form_clientcall input:eq(8)').val(item.crmorganization_id);
			$('#form_clientcall select:eq(0)').val(item.contact_type);
	 });
	$("#form_clientcall input:eq(3)").autocomplete('cc/outbound/search_contactname', {
        multiple: false,
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.contact_code,
                    result: row.contact_code
                }
            });
        },
        formatItem: function(item) {
            return item.contact_name;
        }
	 }).result(function(e, item) {
		 $('#form_clientcall input:eq(0)').val(item.contact_phone);
	        $('#form_clientcall input:eq(1)').val(item.contact_code);
			$('#form_clientcall input:eq(2)').val(item.contact_idcard);
			//$('#form_clientcall input:eq(1)').val(item.contact_hp);
			$('#form_clientcall input:eq(3)').val(item.contact_name);
			$('#form_clientcall textarea:eq(0)').val(item.contact_address);
			//$('#form_clientcall input:eq(8)').val(item.crmorganization_id);
			$('#form_clientcall select:eq(0)').val(item.contact_type);
	 });
	//loadticket('');
	function loadoutbound(phone){
		loadticket(phone);
		$('#form_clientcall input:eq(4)').val('');
		$('#form_clientcall input:eq(5)').val('');
		$('#form_clientcall input:eq(6)').val('');
		$('#form_clientcall input:eq(7)').val('');
		$('#form_clientcall textarea:eq(1)').val('');
		//ticket info
		ticketcount = ajak('cc/outbound/ticketcount');
		$.post("cc/outbound/ticket", "id=" + phone,
				function(obj){
					var ticketcode;
					if(obj['alldata'].length < 1){
						ticketcode = ticketcount;
					}else{
						ticketcode = obj['alldata'][0].ticket_code;
						$('#form_clientcall select:eq(0)').val(obj['alldata'][0].ticket_ocategory);
						isi = ajak("cc/outbound/isi_activity","group="+obj['alldata'][0].ticket_ocategory);
						$("#ticket_subcategory").html(isi);
						if(obj['alldata'][0].ticket_ocategory == "Connect"){
							$("#ticket_sub").show();
							$("#ticket_asub").show();
						}else{
							$("#ticket_sub").hide();
							$("#ticket_asub").hide();
						}
						
						if(obj['alldata'][0].ticket_oasub == "Contacted"){
							$("#ticket_asub").show();
							$("#ticket_sub").show();
							$("#ticket_subcategory").hide();
						}else{
							$("#ticket_asub").show();
							$("#ticket_sub").hide();
							$("#ticket_subcategory").show();
						}
						$('#form_clientcall select:eq(1)').val(obj['alldata'][0].ticket_oasub);
						
						$('#form_clientcall select:eq(2)').val(obj['alldata'][0].ticket_osub);
						$('#form_clientcall select:eq(3)').val(obj['alldata'][0].ticket_osubcategory);
						
						//$('#form_clientcall input:eq(6)').val(obj['alldata'][0].ticket_category);
						//$('#form_clientcall input:eq(7)').val(obj['alldata'][0].ticket_subcategory);
						$('#form_clientcall textarea:eq(1)').val(obj['alldata'][0].ticket_odesc);
					}
					$('#codeticket').html(ticketcode);
					$('#form_clientcall input:eq(4)').val(ticketcode);
					$('#form_clientcall input:eq(5)').val(phone);
					
		}, "json");
		
    }
	//close
	$('#saveclose').click(function(){
		loadticket($('#form_clientcall input:eq(5)').val());
		$('#form_clientcall input').val('');
		$('#form_clientcall select').val('');
		$('#form_clientcall textarea').val('');
		$('#codeticket').html('');
		
		$('#countduration').html('0');
		$({countNum: $('#countduration').text()}).animate({countNum: 1000}, {
			  duration: 1000000,
			  easing:'linear',
			  step: function() {
			    $('#countduration').text(0);
			  },
			  complete: function() {
			    $('#countduration').text(0);
			  }
		});
		$("#collerid").html('<i class="icon-phone-sign"></i>');
		$('#collerid').pulsate({
	        color: "#51a351",
	        repeat: 1
	    });
    });
	//save data done ticket
	$('#savedone').click(function(){
		hasil = validform("form_clientcall");
		if (hasil['isi'] != "invalid") {
			respon = ajak("cc/outbound/saveTicket",$('#form_clientcall').serialize() + "&id=close");
			if (respon == "1") {
				//loadticket($('#form_clientcall input:eq(5)').val());
				loadticket('');
				$('#form_clientcall input').val('');
				$('#form_clientcall select').val('');
				$('#form_clientcall textarea').val('');
				$('#codeticket').html('');
			} else {
                showinfo("Error : " + respon);
            }
        } else {
            showinfo("Form dengan tanda * harus Diisi");
            hasil['focus'].focus();
        }
    });
	//save data create ticket
	$('#savecreate').click(function(){
		hasil = validform("form_clientcall");
		if (hasil['isi'] != "invalid") {
			respon = ajak("cc/outbound/saveTicket",$('#form_clientcall').serialize() + "&id=create");
			if (respon == "1") {
				//loadticket($('#form_clientcall input:eq(5)').val());
				loadticket('');
				$('#form_clientcall input').val('');
				$('#form_clientcall textarea').val('');
				$('#form_clientcall select').val('');
				$('#codeticket').html('');
				
            } else {
                showinfo("Error : " + respon);
            }
        } else {
            showinfo("Form dengan tanda * harus Diisi");
            hasil['focus'].focus();
        }
    });
	//load ticket history
	function loadticket(phone){
		$("#tb_view").html("");
        $.post("cc/outbound/tickethistory", "id=" + phone,
        		function(obj){
		        	var isi ="";
		            for(i = 0; i < obj['alldata'].length; i++) {
		            	if(obj['alldata'][i].status == 0){
		            		//$status = "<span class=\"label label-important\" id='tickopen'>Open</span>";
		            		$status = '<button class="btn btn-mini btn-danger tickopen" type="button" id="tickopen"><span style="display:none">'+ obj['alldata'][i].ticket_id +'</span>Open</button>';
		            	}else{
		            		$status = "<span class=\"label label-success\">Done</span>";
		            	}
		            	tgltime = obj['alldata'][i].ticket_timestamp;
		            	ticket_code = obj['alldata'][i].ticket_code;
		            	row = tgltime.split(" ");
		            	rowr = ticket_code.split("#");
		                isi += "<tr>"
		                    + "<td align=\"center\">"+ obj['alldata'][i].ticket_code +"</td>" 
		                    + "<td>"+ obj['alldata'][i].ticket_phone +"</td>" 
		                    + "<td>"+ obj['alldata'][i].ticket_name +"</td>";
		                if(rowr[0] == "IN"){
		                	isi +="<td>"+ obj['alldata'][i].ticket_category +"</td>" 
			                    + "<td>"+ obj['alldata'][i].ticket_subcategory +"</td>" 
			                    + "<td>"+ obj['alldata'][i].ticket_ocategory +"</td>";
		                	if(obj['alldata'][i].ticket_oasub == ""){
		                		isi += "<td>"+ obj['alldata'][i].ticket_osubcategory +"</td>";
		                	}else{
		                		isi += "<td>"+ obj['alldata'][i].ticket_oasub +"</td>";
		                	}
		                	if((obj['alldata'][i].ticket_osub == "")&&( obj['alldata'][i].ticket_ocategory != 'Unconnect')){
		                		isi += "<td>"+ obj['alldata'][i].ticket_osubcategory +"</td>";
		                	}else{
		                		isi += "<td>"+ obj['alldata'][i].ticket_osub +"</td>";
		                	}
		                	    //+ "<td>"+ obj['alldata'][i].ticket_desc +"</td>";
		                }else{
		                	isi +="<td></td><td></td>" 
		                		+ "<td>"+ obj['alldata'][i].ticket_ocategory +"</td>" 
			                    + "<td>"+ obj['alldata'][i].ticket_oasub +"</td>" 
			                    + "<td>"+ obj['alldata'][i].ticket_osub +"</td>" 
			                    //+ "<td>"+ obj['alldata'][i].ticket_odesc +"</td>";
		                }
		                isi +="<td>"+ $status +"</td>" 
		                    + "</tr>";
		            }  
		            $("#tb_view").html(isi);
		            $('.tickopen').click(function(){
		        		var id = $(this).text();
		        		id = id.split("Open");
		        		//alert(id[0]);
		        		loadticketopen(id[0]);
		            });
		}, "json");
        
        return false;
	}
	function loadticketopen(code){
		$('.ver-inline-menu li:eq(0)').removeClass('').addClass('active');
        $('.ver-inline-menu li:eq(1)').removeClass('active').addClass('');
        
        $('#tab_1-1').removeClass('').addClass('active');
        $('#tab_2-2').removeClass('active').addClass('');
        
		
		$('#form_clientcall select:eq(1)').val('');
		$('#form_clientcall select:eq(2)').val('');
		$('#form_clientcall select:eq(3)').val('');
		$.post("cc/outbound/ticketsearch", "code=" + code,
				function(obj){
					if(obj['alldata'].length == 1){
						$('#form_clientcall input:eq(0)').val(obj['alldata'][0].ticket_phone);
						$('#form_clientcall input:eq(1)').val(obj['alldata'][0].ticket_name);
						$('#form_clientcall input:eq(2)').val(obj['alldata'][0].ticket_imei);
						$('#form_clientcall input:eq(3)').val(obj['alldata'][0].ticket_polis);
						$('#form_clientcall textarea:eq(0)').val(obj['alldata'][0].ticket_desc);
						$('#form_clientcall input:eq(4)').val(obj['alldata'][0].ticket_code);
						$('#form_clientcall input:eq(5)').val(obj['alldata'][0].ticket_phone);
						$('#codeticket').html(obj['alldata'][0].ticket_code);
						
						//$('#form_clientcall select:eq(0)').val(obj['alldata'][0].ticket_ocategory);
						isi = ajak("cc/outbound/isi_activity","group="+obj['alldata'][0].ticket_ocategory);
						$("#ticket_subcategory").html(isi);
						if(obj['alldata'][0].ticket_ocategory == "Connect"){
							$("#ticket_sub").show();
							$("#ticket_asub").show();
						}else{
							$("#ticket_sub").hide();
							$("#ticket_asub").hide();
						}
						
						if(obj['alldata'][0].ticket_oasub == "Contacted"){
							$("#ticket_asub").show();
							$("#ticket_sub").show();
							$("#ticket_subcategory").hide();
						}else{
							$("#ticket_asub").show();
							$("#ticket_sub").hide();
							$("#ticket_subcategory").show();
						}
						//$('#form_clientcall select:eq(1)').val(obj['alldata'][0].ticket_oasub);
						
						//$('#form_clientcall select:eq(2)').val(obj['alldata'][0].ticket_osub);
						//$('#form_clientcall select:eq(3)').val(obj['alldata'][0].ticket_osubcategory);
						
						$('#form_clientcall textarea:eq(1)').val(obj['alldata'][0].ticket_odesc);
					}
					
		}, "json");
		
	}
	//loadhistoryagent();
	function loadhistoryagent(){
		$("#tb_view1").html("");
        $.post("cc/outbound/historyagent", "id=",
        		function(obj){
		        	var isi ="";
		            for(i = 0; i < obj['alldata'].length; i++) {
		            	isi += "<tr>"
		                    + "<td align=\"center\">"+ obj['alldata'][i].src +"</td>" 
		                    + "<td align=\"center\">"+ obj['alldata'][i].dst +"</td>" 
		                    + "<td align=\"center\">"+ obj['alldata'][i].billsec +" sec</td>" 
		                    + "<td align=\"center\">"+ obj['alldata'][i].calldate +" sec</td>" 
		                    + "<td align=\"center\">"+ obj['alldata'][i].disposition +"</td>" 
		                    + "</tr>";
		            }  
		            $("#tb_view1").html(isi);
		}, "json");
        return false;
	}
	
	// script
	//load_script();
	function load_script(){
		respon = ajak("cc/outbound/viewscript");
		var isi = respon.split("#####");
		$('#form_script textarea:eq(0)').val(isi[1]);
		$('#form_script input:eq(0)').val(isi[0]);
	}
	$("#actscript").live('click',function(){
		respon = ajak("cc/outbound/editscript",$('#form_script').serialize());
		//load_script();
	});
	$("#ticket_category").val('');
	$("#ticket_asub").hide();
	$("#ticket_sub").hide();
	//loadactivity('Connect');
	$("#ticket_category").live('change',function(){
		ff = $(this).val();
		loadactivity(ff);
	});
	$("#ticket_asub").live('change',function(){
		f = $(this).val();
		if(f == "Contacted"){
			$("#ticket_asub").show();
			$("#ticket_sub").show();
			$("#ticket_subcategory").hide();
			$('#form_clientcall select:eq(3)').val('');
		}else{
			$("#ticket_asub").show();
			$("#ticket_sub").hide();
			$("#ticket_subcategory").show();
			
			$('#form_clientcall select:eq(2)').val('');
		}
	});
	function loadactivity(g){
		isi = ajak("cc/outbound/isi_activity","group="+g);
		$("#ticket_subcategory").html(isi);
		if(g == "Connect"){
			$("#ticket_asub").show();
			$("#ticket_sub").hide();
			$("#ticket_subcategory").hide();
			$('#form_clientcall select:eq(2)').val('');
			$('#form_clientcall select:eq(3)').val('');
		}else{
			$("#ticket_asub").hide();
			$("#ticket_sub").hide();
			$("#ticket_subcategory").show();
			$('#form_clientcall select:eq(1)').val('');
			$('#form_clientcall select:eq(2)').val('');
		}
	}
	
	$("#table_ticket").mastertable({
        urlGet:"cc/outbound/get_callhistory",
        flook:"calldate"
    },
    function(hal,juml,json) {
        var isi="",info="";
        var nr = 1;
        for(i = 0; i < json['alldata'].length; i++) {
            idx = "j" + json['alldata'][i].uniqueid;
            dtx = json['alldata'][i];
            jSimpan(idx,dtx);
            //
            $status = '<button class="tickopen btn btn-mini btn-danger tickopen" type="button"><span style="display:none">'+ json['alldata'][i].uniqueid +'</span>Call</button>';
        	if(json['alldata'][i].dstchannel == ""){
        		info = "ABANDON";
        	}else{
        		info = "";
        	}
            isi += "<tr style=\"vertical-align:top;\">"
                + "<td align=\"center\">" + nr + "</td>"
                + "<td align=\"center\">" + json['alldata'][i].calldate + "</td>"
                + "<td align=\"center\">" + json['alldata'][i].src + "</td>"
                + "<td align=\"left\">" + json['alldata'][i].name + "</td>"
                + "<td align=\"center\">" + json['alldata'][i].cabang + "</td>"
                + "<td align=\"center\">" + json['alldata'][i].duration + "</td>"
                + "<td align=\"center\">" + json['alldata'][i].disposition + "</td>"
                + "<td align=\"center\">" + info + "</td>"
                + "<td align=\"center\">" + $status + "</td>"
                + "<td align=\"center\">" + json['alldata'][i].uniqueid + "</td>"
                + "</tr>";
            nr++;
        }
        return isi;
    },
    function domIsi() {
        $('.tickopen').click( function() {
            $(".infonya").hide();
            obj = jAmbil("j" + $(this).parent().next().text());
            $('.nav-tabs li:eq(1)').removeClass('active').addClass('');
            $('.nav-tabs li:eq(0)').removeClass('').addClass('active');
            $('#tab_1_2').removeClass('active').addClass('');
            $('#tab_1_1').removeClass('').addClass('active');
            $('#contact_phone').val(obj.src);
            return false;
        });
        warnatable();
    });
/*
 *  ----------------------- RESET -------------------------------
 */
    $(".reset").click();
    
});