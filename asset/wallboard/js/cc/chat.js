/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/chat.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
	
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    
	// aplikasi utk notifikasi
	loadnotice();
	var answer = 0;
	function loadnotice(){
		respon = ajak('sync/noticechat');
		row = respon.split("#");
    	inbound  = row[6];
    	outbound = row[7];
    	ext      = row[8];
    	channels = row[9];
    	user_id = row[10];
    	filter = row[0];
    	
    	//respon chat
    	if(row[2] == 1){
    		if(user_id != 1){
    			responn = ajak('cc/chat/chatnew');
    		}else{
    			responn = ajak('cc/chat/chatnewadmin');
    		}
    		
                var unique_id3 = $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    //title: '<i class="icon-bullhorn"></i> '+row[0],
                    // (string | mandatory) the text inside the notification
                    text: responn,
                    // (bool | optional) if you want it to fade out on its own or just sit there
                    sticky: true,
                    // (int | optional) the time you want it to be alive for before fading out
                    time: '',
                    // (string | optional) the class name you want to apply to that specific message
                    class_name: 'my-sticky-class'
                });

                // You can have it return a unique id, this can be used to manually remove it later using
                if(user_id != 1){
                setTimeout(function () {
                    $.gritter.remove(unique_id3, {
                        fade: true,
                        speed: 'slow'
                    });
                }, 300000);
                }
            respon = ajak('cc/chat/chatupdate','ext='+ ext +'&set=0');
    		
    	}
	}
    setInterval(function(){ loadnotice() }, 3000);
    
    
	
	// chat
	loadchatuserol();
	function loadchatuserol(){
		respon = ajak("cc/chat/chatuserall");
		$('#idonline').html(respon);
		respon1 = ajak("cc/chat/chatgroupall");
		$('#idonline1').html(respon1);
	}
	$("#chatact").live('click',function(){
		user = $(this).text();
		$("#titleevent").html(" to "+ user);
		loadchatto(0,user);
	});
	$("#chatactgroup").live('click',function(){
		groupname = $(this).text();
		$("#titleevent").html(" to "+ groupname);
		//groupname = groupname.split('Team SPV ');
		loadchatto(1,groupname);
	});
	function loadchatto(type,id){
		//alert(type+'##'+id);
		//exit();
		respon = ajak("cc/chat/chatto",'id='+id+'&type='+type);
		from = ajak("cc/chat/chatfrom");
		$("#chatslog").html(respon);
		$("#from").val(from);
		$("#to").val(id);
		$("#type").val(type);
	}
	$("#message").val('');
	$("#btnchats").live('click',function(){
		$from = $("#from").val();
		$to = $("#to").val();
		$type = $("#type").val();
		$message = $("#message").val();
		respon = ajak("cc/chat/chats",'from='+$from+'&to='+$to+'&type='+$type+'&msg='+$message);
		if(respon == 1){
			$("#message").val('');
			loadchatto($type,$to);
			// sent notice
		}
	});
	$("#message").keypress(function (e) {
		if (e.which == 13) {
			$from = $("#from").val();
			$to = $("#to").val();
			$type = $("#type").val();
			$message = $("#message").val();
			respon = ajak("cc/chat/chats",'from='+$from+'&to='+$to+'&type='+$type+'&msg='+$message);
			if(respon == 1){
				$("#message").val('');
				loadchatto($type,$to);
				// sent notice
			}
        }
    });
	
/*
 *  ----------------------- RESET -------------------------------
 */
    $(".reset").click();
    $.ajaxSetup({ cache: false });
});