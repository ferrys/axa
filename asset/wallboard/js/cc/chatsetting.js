/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/chatsetting.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
	
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    loadgroups();
    function loadgroups(){
    	respon = ajak("cc/chatsetting/groups");
		$('#idgroups').html(respon);
    }
    setInterval(function(){ loadgroups() }, 100000);
    $("#chatactroom").live('click',function(){
		group = $(this).text();
		$("#group_name").val(group);
		$("#titlegroup").html(group);
		$("#idact").html(' <a href="javascript:;" id="btngroupdel" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Delete</a> <a href="javascript:;" id="btngroupclose" class="btn btn-mini btn-primary"><i class="icon-minus-sign icon-white"></i> Close</a>');
		loadroomgroups(group);
    });
    $("#btngroupchats").live('click',function(){
		$name = $("#chatgroups_name").val();
		resp = addgroups($name);
		if(resp == "1062"){
			alert('Maaf nama groups '+ $name +' sudah ada');
		}else if(resp == "1"){
			$("#chatgroups_name").val('');
			loadgroups();
		}
	});
    $("#chatgroups_name").keypress(function (e) {
		if (e.which == 13) {
			$name = $("#chatgroups_name").val();
			resp = addgroups($name);
			if(resp == "1062"){
				alert('Maaf nama groups '+ $name +' sudah ada');
			}else if(resp == "1"){
				$("#chatgroups_name").val('');
				loadgroups();
			}
        }
    });
    function addgroups($name){
    	respon = ajak("cc/chatsetting/groups_add",'id='+$name);
    	return respon;
    }
    $("#btngroupdel").live('click',function(){
    	groupname = $('#titlegroup').text();
    	respon = ajak("cc/chatsetting/groups_del",'id='+groupname);
    	if(respon == "1"){
    		$("#titlegroup").html('');
    		$("#idact").html('');
    		$("#group_name").val('');
    		$("#name").val('');
			loadgroups();
		}else{
			alert('maaf group masih di gunakan');
			loadgroups();
		}
	});
    $("#btngroupclose").live('click',function(){
    	$("#titlegroup").html('');
		$("#idact").html('');
		$("#chatroom").html('');
		$("#group_name").val('');
		$("#name").val('');
    	loadgroups();
	});
    
    $("#name").autocomplete('cc/chatsetting/search_user', {
        multiple: false,
        parse: function(data) {
            return $.map(eval(data), function(row) {
                return {
                    data: row,
                    value: row.username,
                    result: row.username
                }
            });
        },
        formatItem: function(item) {
            return item.username + ' - ' + item.nama_pegawai;
        }
	 }).result(function(e, item) {
	        $("#user_name").val(item.username);
	 });
    $("#btnroomchats").live('click',function(){
		$group = $("#group_name").val();
		$user_name = $("#user_name").val();
		if($group == ""){
			alert('Maaf anda belum memilih groups');
		}else if($user_name == ""){
			alert('Silahkan pilih user dulu');
		}else{
			resp = addroomgroups($group,$user_name);
			if(resp == 1){
				$("#name").val('');
				$("#user_name").val('');
				loadroomgroups($group);
			}
		}
	});
    $("#name").keypress(function (e) {
		if (e.which == 13) {
			$group = $("#group_name").val();
			$user_name = $("#user_name").val();
			if($group == ""){
				alert('Maaf anda belum memilih groups');
			}else if($user_name == ""){
				alert('Silahkan pilih user dulu');
			}else{
				resp = addroomgroups($group,$user_name);
				if(resp == 1){
					$("#name").val('');
					$("#user_name").val('');
					loadroomgroups($group);
				}
			}
        }
    });
    function addroomgroups($group,$user_name){
    	respon = ajak("cc/chatsetting/room_add",'group='+$group +'&user='+ $user_name);
    	return respon;
    }
    function loadroomgroups($group){
    	respon = ajak("cc/chatsetting/chatuserroom",'group='+$group);
		$('#chatroom').html(respon);
    }
    $("#userroomdel").live('click',function(){
    	$group = $("#group_name").val();
    	$user = $(this).text();
    	$user = $user.split('delete - ');
    	respon = ajak("cc/chatsetting/room_del",'group='+$group +'&user='+ $user[1]);
    	
    	loadroomgroups($group);
	});
    
/*
 *  ----------------------- RESET -------------------------------
 */
    $(".reset").click();
    $.ajaxSetup({ cache: false });
});