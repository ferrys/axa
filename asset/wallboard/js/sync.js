/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : sync.php
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
    //---- Inisialisasi
	$('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
	// aplikasi utk notifikasi
	//loadnotice();
	var answer = 0;
	function loadnotice(){
    	respon = ajak('sync/notice');
    	row = respon.split("#");
    	inbound  = row[6];
    	outbound = row[7];
    	ext      = row[8];
    	//respon call
    	if(row[0] == 1){
    		
    		// notice call
    		$.post("sync/clientinfo", "id=" + inbound,
    				function(obj){
    					if(obj['alldata'].length == 1){
    						var logcall = ajak("sync/logcall","id="+ inbound);
    						var unique_call = $.gritter.add({
    			            	// (string | mandatory) the heading of the notification
    			                title: '<div id="callagent"><span class="label label-success"><i class="icon-phone-sign"></i></span> '+ obj['alldata'][0].contact_name+'</div>',
    			                // (string | mandatory) the text inside the notification
    			                text: 'Code : '+ obj['alldata'][0].contact_code+'<br>Branch Code : '+ obj['alldata'][0].crmorganization_code+'<br>Dealer Nama : '+ obj['alldata'][0].crmorganization_name+'<br>Client Type : '+ obj['alldata'][0].contact_type+''+
    			                	''+logcall+'<br><button id="callanswerdid" class="btn btn-small"><strong class="alert-success">Answerd</strong></button> ',
    			                // (string | optional) the image to display on the left
    			                image: './assets/img/avatar3.jpg',
    			                // (bool | optional) if you want it to fade out on its own or just sit there
    			                sticky: true,
    			                // (int | optional) the time you want it to be alive for before fading out
    			                time: '',
    			                // (string | optional) the class name you want to apply to that specific message
    			                class_name: 'my-sticky-class'
    			            });
    			            $('#callagent').pulsate({
    			                color: "#dd5131",
    			                repeat: 10000
    			            });
    			            /*
    			            setTimeout(function () {
    			                $.gritter.remove(unique_call, {
    			                    fade: true,
    			                    speed: 'slow'
    			                });
    			            }, 3000);
    			            */
    			            $('#callanswerdid').click(function(){
    			            	/*$.gritter.remove(unique_call, {
    			                    fade: true,
    			                    speed: 'slow'
    			                });*/
    			            	loadinbound(inbound);
    			    	    });
    					}
    	    }, "json");
    		
    		//respon = ajak('sync/callupdate','ext='+ ext +'&callnumber='+ inbound +'&set=0');
    		
    	}
    	//respon email
    	if(row[1] == 1){
    		
    	}
    	//respon chat
    	if(row[2] == 1){
    		
    	}
    	//respon videocall
    	if(row[3] == 1){
    		
    	}
    	//respon fax
    	if(row[4] == 1){
    		
    	}
    	//respon sms
    	if(row[5] == 1){
    		
    	}
	}
    //setInterval(function(){ loadnotice() }, 2000);
	
	function loadinbound(phone){
		
    }
    
});
