/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/reportticket.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
	
//---- Inisialisasi

    $("#tab-utama").tabs();
    $('.infoproses3').hide();
    $('.infoproses4').hide();
    $('.infoproses5').hide();
    $('.infoproses6').hide();
    $('.infoproses8').hide();
    $('.infoproses9').hide();
    $('.infoprosesa').hide();
    $('.infoprosesabd').hide();
	
	isi = ajak('report/reportinbound/isi_user');
    $("#useractivity").html(isi);
    isi = ajak('report/reportinbound/isi_user1');
    $("#useractivity1").html(isi);
	isi = ajak('report/reportinbound/isi_user');
    $("#useraht").html(isi);
    isi = ajak('report/reportinbound/isi_user1');
    $("#useraht1").html(isi);
	isi = ajak('report/reportinbound/isi_user');
    $("#user_a").html(isi);
	
    $("#useractivity").live('change',function(){
		ff = $(this).val();
		isi = ajak('report/reportticket/isi_user1','id='+ ff);
	    $("#useractivity1").html(isi);
	});
	
	$("#useraht").live('change',function(){
		ff = $(this).val();
		isi = ajak('report/reportticket/isi_user1','id='+ ff);
	    $("#useraht1").html(isi);
	});
	
    function secondstotime(d)
    {
	    d = Number(d);
	    var h = Math.floor(d / 3600);
	    var m = Math.floor(d % 3600 / 60);
	    var s = Math.floor(d % 3600 % 60);
	    return ((h > 0 ? (h >= 10 ? h : '0' + h): '00') + ':' + (m > 0 ? (m >= 10 ? m : '0' + m): '00') + ':' + (s > 0 ? (s >= 10 ? s : '0' + s): '00')  );
    }
    function hmsToSecondsOnly(str) {
        var p = str.split(':'),
            s = 0, m = 1;
        while (p.length > 0) {
            s += m * parseInt(p.pop(), 10);
            m *= 60;
        }
        return s;
    }
	
/////////////////////////////////////LIST TICKETING///////////////////////////////////
    $('#showlap3').click(function(){
    	var isitgl1 =  $('#tgllap31').val();
    	var isitgl2 =  $('#tgllap32').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Selected to date");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tgllap32').val().substr(6, 4) - $('#tgllap31').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllap32').val().substr(3, 2)*1) + 12) - ($('#tgllap31').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
        isitglperiode = ($('#tgllap32').val() == $('#tgllap32').val()) ? cbulan($('#tgllap31').val()) : cbulan($('#tgllap31').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllap32').val());
        $('#isitgl3').html(isitglperiode);
    	loaddata3(isitgl1,isitgl2);
    });
    function loaddata3(tgl1,tgl2){
    	$('.infoproses3').show();
    	$("#tb_view3").html("");
    	$.post("report/reportticket/get_dataview2","tgl1="+ tgl1 +"&tgl2="+ tgl2 ,
            function(json){
				var isi ="",nr=1,pickup =0;
				for(i = 0; i < json['alldata'].length; i++) {
					
					isi += "<tr>"
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].tgl_tiket +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].wkt_tiket +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].hday +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_code +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].ticket_nopol +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].ticket_phone +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].ticket_name +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].call_type1 +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].call_type2 +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_desc +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].call_status +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].username +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].type +"</td>" 
						+ "</tr>";
					nr++;
				}  
				isi += "<tr>"
					+ "<td></td>" 
					+ "</tr>";
				
				$("#tb_view3").html(isi);
				$('.infoproses3').hide();
            }, "json");
        return false;
    }
    $('#pdownload3').click( function() {
    	var isitgl1 =  $('#tgllap31').val();
    	var isitgl2 =  $('#tgllap32').val();
    	location.href = 'report/reportticket/get_dataviewexel2/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
	
/////////////////////////////////////ANSEWERED CALL DURATION///////////////////////////////////

	$('#showlap4').click(function(){
    	var isitgl1 =  $('#tgllap41').val();
    	//var isitgl2 =  $('#tgllap42').val();
    	if(isitgl1 == ""){
    		alert("Selected to date");
    		return false;
    	}
    	
    	/* if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        } */
        /* if(($('#tgllap42').val().substr(6, 4) - $('#tgllap41').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllap42').val().substr(3, 2)*1) + 12) - ($('#tgllap41').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        } */
        isitglperiode = $('#tgllap41').val();
        $('#isitgl3').html(isitglperiode);
    	//loaddata4(isitgl1,isitgl2);
    	loaddata4(isitgl1);
    });
    function loaddata4(tgl1){
    	$('.infoproses4').show();
    	$("#tb_view4").html("");
    	$.post("report/reportticket/get_dataview4","tgl1="+ tgl1 ,
                function(json){
                    var isi ="",nr=1,pickup =0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].tanggal +"</td>" 
                   	 		//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].waktu +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].call_entry +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].call_end +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].callerid +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].agent +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].dur_cc +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view4").html(isi);
                    $('.infoproses4').hide();
            }, "json");
        return false;
    }
    $('#pdownload4').click( function() {
    	var isitgl1 =  $('#tgllap41').val();
    	//var isitgl2 =  $('#tgllap42').val();
    	location.href = 'report/reportticket/get_dataviewexel4/'+isitgl1+'/export';
        return false;
    });
	
////////////////////////////////////// REPORT INTERVAL ////////////////////////////////////	
	$('#showlap1').click( function() {
    	var tgl3 =  $('#tgl3').val();
    	var tgl3z =  $('#tgl3z').val();
		//alert(tgl3+'###'+tgl3z);
    	if((tgl3 == "")||(tgl3z =="")){
    		alert("Selected to date");
    		return false;
    	}
		
		if (Date.parse(tgl3) > Date.parse(tgl3z)) {
            alert("Start date < End date");
            return false;
        }
		
		if(($('#tgl3z').val().substr(6, 4) - $('#tgl3').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgl3z').val().substr(3, 2)*1) + 12) - ($('#tgl3').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
		isitglperiode = ($('#tgl3z').val() == $('#tgl3z').val()) ? cbulan($('#tgl3').val()) : cbulan($('#tgl3').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgl3z').val());
        $('#isitgl2').html(isitglperiode);
    	loaddatainterval(tgl3,tgl3z);
    });
    function loaddatainterval(tgl3,tgl3z){
    	$('.infoproses6').show();
    	$("#tb_view1").html("");
		//alert(tgl3+'###'+tgl3z);
    	$.post("report/reportticket/get_dataviewinterval","tgl3="+ tgl3 +"&tgl3z="+ tgl3z ,
            function(json){
				var isi ="";
				var ivrtosales=0, coff=0, acd=0, abd=0,pacd=0,pabd=0,acdtalktime=0,tothold=0,totalcoff=0,totalacd=0,totalabdivr=0,totalabd=0,totacdtalktime = 0,totaux=0,totavaltimetoH=0,totavg=0,totstaftime=0,totpaccu=0,tottarget=0,totacdtalktime1=0,totmax=0,abd5sec=0,abd4sec=0,abd3sec=0,abdUp5sec=0,abdUn3sec=0,abdUp30sec=0,abdUn30sec=0,abdUp8sec=0,abdUn8sec=0,inQueue=0,wm=0,abdDistinct=0,totalabd5=0,totalabd4=0,totalabd3=0,totalabdUp5=0,totalabdUn3=0,totalabdUp30=0,totalabdUn30=0,totalabdUp8=0,totalabdUn8=0,totalinQueue=0,totalwm=0,totalabdDistinct=0;
				var maxstaftot = 1,totaltosales=0;
				for(i = 0; i < json['alldata'].length; i++) {
					acd = json['alldata'][i].answered;
					abd = json['alldata'][i].abandon;
					
					abd5sec = json['alldata'][i].abd5s;
					abd4sec = json['alldata'][i].abd4s;
					abd3sec = json['alldata'][i].abd3s;
					abdUp5sec = json['alldata'][i].abdUp5s;
					abdUn3sec = json['alldata'][i].abdUn3s;
					abdUp30sec = json['alldata'][i].abdUp30s;
					abdUn30sec = json['alldata'][i].abdUn30s;
					abdUp8sec = json['alldata'][i].abdUp8s;
					abdUn8sec = json['alldata'][i].abdUn8s;
					inQueues = json['alldata'][i].inqueue;
					wm = json['alldata'][i].withtout_monitoring;
					abdDistinct = json['alldata'][i].abdDistinct;
					
					abdivr = json['alldata'][i].abandonivr;
					ivrtosales = json['alldata'][i].ivrtosales;
					
					coff = eval(acd) + eval(abd);
					
					isi += "<tr>"
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].calldate +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].time +"</td>" 
						//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].agent +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ coff +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ acd +"</td>" 
						//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ abdivr +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ abd +"</td>" 
						//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].inqueue +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].withtout_monitoring +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abd5s +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abd4s +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abd3s +"</td>"
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abdUp5s +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abdUn3s +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abdUp30s +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abdUn30s +"</td>"
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abdUp8s +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abdUn8s +"</td>" 
						+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abdDistinct +"</td>"  
						+ "</tr>";
						
					totalcoff += eval(coff);
					totalacd += eval(acd);
					totalabdivr += eval(abdivr);
					totaltosales += eval(ivrtosales);
					totalabd += eval(abd);
					totalabd5 += eval(abd5sec);
					totalabd4 += eval(abd4sec);
					totalabd3 += eval(abd3sec);
					totalabdUp5 += eval(abdUp5sec);
					totalabdUn3 += eval(abdUn3sec);
					totalabdUp30 += eval(abdUp30sec);
					totalabdUn30 += eval(abdUn30sec);
					totalabdUp8 += eval(abdUp8sec);
					totalabdUn8 += eval(abdUn8sec);
					//totalinQueue += eval(inQueues);
					totalwm += eval(wm);
					totalabdDistinct += eval(abdDistinct);
				}
				
				
				isi += "<tr>"
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap colspan=\"2\">Total</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalcoff +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalacd +"</td>" 
					//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdivr +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabd +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabd5 +"</td>" 
					//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalinQueue +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalwm +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabd4 +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabd3 +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdUp5 +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdUn3 +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdUp30 +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdUn30 +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdUp8 +"</td>" 
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdUn8 +"</td>"
					+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ totalabdDistinct +"</td>"  
					+ "</tr>";
				$("#tb_view1").html(isi);
				$('.infoproses6').hide();
            }, "json");
    	
            return false;
    }
	
	$('#pdownload2').click( function() {
    	var tgl3 	= $('#tgl3').val();
    	var tgl3z 	= $('#tgl3z').val();
    	
        location.href = 'report/reportticket/generate_intervalexels/'+tgl3+'/'+tgl3z+'/export';
    	
        return false;
    });

/////////////////////////////////// REPORT ACTIVITY AGENT ////////////////////////////////////////
    $('#showlap5').click(function(){
    	var isitgl1 =  $('#tglactivity').val();
    	var user2 = $('#useractivity').val();
    	var user3 = $('#useractivity1').val();
		//alert(user2+'-'+user3);
    	if(user3 == 0){
    		alert("Maaf anda belum memilih agent");
    		return false;
    	}
    	var jam = $('#jam1').val();
    	isitglperiode = cbulan($('#tglactivity').val());
        $('#isitglactivity').html(isitglperiode);
    	loaddata5(isitgl1,user2,user3,jam);
    });
    function loaddata5(tgl1,user1,user2,jam){
    	$('.infoproses5').show();
    	$("#tb_view5").html("");
    	$.post("report/reportticket/get_dataviewactivity","tgl1="+ tgl1 +"&jam="+ jam +"&user="+ user1 +"&user1="+ user2 ,
                function(json){
                    var isi ="";
                    var totsholat = 0, totistirahat=0, totbriefing=0, tottoilet=0, toteskalasi=0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].time +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].agent +"</td>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].login +"</td>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].logout +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].sholat +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].istirahat +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].briefing +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].toilet +"</td>"
							+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].eskalasi +"</td>"
                   	 		+ "</tr>";
                    	totsholat += eval(hmsToSecondsOnly(json['alldata'][i].sholat));
                    	totistirahat += eval(hmsToSecondsOnly(json['alldata'][i].istirahat));
                    	totbriefing += eval(hmsToSecondsOnly(json['alldata'][i].briefing));
                    	tottoilet += eval(hmsToSecondsOnly(json['alldata'][i].toilet));
                    	toteskalasi += eval(hmsToSecondsOnly(json['alldata'][i].eskalasi));
                    } 
                    isi += "<tr>"
               	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap colspan='4'>TOTAL</td>" 
               	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(totsholat) +"</td>"
        	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(totistirahat) +"</td>"
        	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(totbriefing) +"</td>"
        	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(tottoilet) +"</td>"
        	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(toteskalasi) +"</td>"
               	 		+ "</tr>";
                    $("#tb_view5").html(isi);
                    $('.infoproses5').hide();
            }, "json");
    	
    	return false;
    }
    $('#pdownload5').click( function() {
    	var isitgl1 =  $('#tglactivity').val();
    	var user2 = $('#useractivity').val();
    	var user3 = $('#useractivity1').val();
    	var jam = $('#jam1').val();
    	location.href = 'report/reportticket/get_dataviewexel5/'+isitgl1+'/'+user2+'/'+user3+'/'+jam+'/export';
        return false;
    });
	
/////////////////////////////////// REPORT AHT AGENT ////////////////////////////////////////
 	$('#showlap8').click(function(){
    	var isitgl1 =  $('#tglaht1').val();
    	var isitgl2 =  $('#tglaht2').val();
    	if(isitgl1 == ""){
    		alert("Selected to date");
    		return false;
    	}
    	
    	 if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tglaht2').val().substr(6, 4) - $('#tglaht1').val().substr(6, 4)) > 0) {
            selisihx = (($('#tglaht2').val().substr(3, 2)*1) + 12) - ($('#tglaht1').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
		isitglperiode = ($('#tglaht2').val() == $('#tglaht2').val()) ? cbulan($('#tglaht1').val()) : cbulan($('#tglaht1').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tglaht2').val());
        $('#isitglaht').html(isitglperiode);
    	loaddataAht(isitgl1,isitgl2);
    });
    function loaddataAht(tgl1,tgl2){
    	$('.infoproses8').show();
    	$("#tb_view8").html("");
    	$.post("report/reportticket/get_dataviewAht","tgl1="+ tgl1 +"&tgl2="+ tgl2,
            function(json){
                    var isi ="",nr=1;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].tgl +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].agent +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].aht +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view8").html(isi);
                    $('.infoproses8').hide();
            }, "json");
        return false;
    }
    $('#pdownload8').click( function() {
    	var isitgl1 =  $('#tglaht1').val();
    	var isitgl2 =  $('#tglaht2').val();
    	location.href = 'report/reportticket/get_dataviewexel8/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
	
	/////////////////////////////////// REPORT DAILY AHT ////////////////////////////////////////
 	$('#showlap9').click(function(){
    	var isitgl1 =  $('#tglahtd1').val();
    	var isitgl2 =  $('#tglahtd2').val();
    	if(isitgl1 == ""){
    		alert("Selected to date");
    		return false;
    	}
    	
    	 if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tglahtd2').val().substr(6, 4) - $('#tglahtd1').val().substr(6, 4)) > 0) {
            selisihx = (($('#tglahtd2').val().substr(3, 2)*1) + 12) - ($('#tglahtd1').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
		isitglperiode = ($('#tglahtd2').val() == $('#tglahtd2').val()) ? cbulan($('#tglahtd1').val()) : cbulan($('#tglahtd1').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tglahtd2').val());
        $('#isitglahdt').html(isitglperiode);
    	loaddataAhtd(isitgl1,isitgl2);
    });
    function loaddataAhtd(tgl1,tgl2){
    	$('.infoproses9').show();
    	$("#tb_view9").html("");
    	$.post("report/reportticket/get_dataviewAhtd","tgl1="+ tgl1 +"&tgl2="+ tgl2,
            function(json){
                    var isi ="",nr=1;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].tanggal +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].avg +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view9").html(isi);
                    $('.infoproses9').hide();
            }, "json");
        return false;
    }
    $('#pdownload9').click( function() {
    	var isitgl1 =  $('#tglahtd1').val();
    	var isitgl2 =  $('#tglahtd2').val();
    	location.href = 'report/reportticket/get_dataviewexel9/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
	
	////////////////////////////////////SUMMARY ACTIVITY AGENT////////////////////////////////////
	$('#showlapa').click(function(){
		var isitgl1 =  $('#tgla').val();
    	var isitgl2 =  $('#tgla1').val();
    	if(isitgl1 == ""){
    		alert("Selected to date");
    		return false;
    	}
    	
    	 if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tgla1').val().substr(6, 4) - $('#tgla').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgla1').val().substr(3, 2)*1) + 12) - ($('#tgla').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
		isitglperiode = ($('#tgla1').val() == $('#tgla1').val()) ? cbulan($('#tgla').val()) : cbulan($('#tgla').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgla1').val());
        $('#isitgla').html(isitglperiode);
    	loaddataa(isitgl1,isitgl2);
		
    });
    function loaddataa(tgl1,tgl2){
    	$('.infoprosesa').show();
    	$("#tb_viewa").html("");
    	$.post("report/reportticket/get_dataviewa","tgl1="+ tgl1 +"&tgl2="+ tgl2,
            function(json){
                    var isi ="",nr=1;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].tgl +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].agent +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].callall +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_viewa").html(isi);
                    $('.infoprosesa').hide();
            }, "json");
        return false;
    }
    $('#pdownloada').click( function() {
    	var isitgl1 =  $('#tgla').val();
    	var isitgl2 =  $('#tgla1').val();
    	location.href = 'report/reportticket/get_dataviewaexel/'+isitgl1+'/'+isitgl2+'';
        return false;
    });
	
	////////////////////////////////////Abandon Detail////////////////////////////////////
	$('#showlapabd').click(function(){
		var isitgl1 =  $('#tglabd1').val();
    	var isitgl2 =  $('#tglabd2').val();
    	if(isitgl1 == ""){
    		alert("Selected to date");
    		return false;
    	}
    	
    	 if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tglabd2').val().substr(6, 4) - $('#tglabd1').val().substr(6, 4)) > 0) {
            selisihx = (($('#tglabd2').val().substr(3, 2)*1) + 12) - ($('#tglabd1').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
		isitglperiode = ($('#tglabd2').val() == $('#tglabd2').val()) ? cbulan($('#tglabd1').val()) : cbulan($('#tglabd1').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tglabd2').val());
        $('#isitglabd').html(isitglperiode);
    	loaddatabd(isitgl1,isitgl2);
		
    });
    function loaddatabd(tgl1,tgl2){
    	$('.infoprosesabd').show();
    	$("#tb_viewabd").html("");
    	$.post("report/reportticket/get_dataviewabd","tgl1="+ tgl1 +"&tgl2="+ tgl2,
            function(json){
                    var isi ="",nr=1;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].tanggal +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].jam +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].telp +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].dur +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_viewabd").html(isi);
                    $('.infoprosesabd').hide();
            }, "json");
        return false;
    }
    $('#pdownloabd').click( function() {
    	var isitgl1 =  $('#tglabd1').val();
    	var isitgl2 =  $('#tglabd2').val();
    	location.href = 'report/reportticket/get_dataviewaexelabd/'+isitgl1+'/'+isitgl2+'';
        return false;
    });
	
});