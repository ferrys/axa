/*
 * Aplikasi
 * Copyright (c) 2015
 *
 * file   : cc/reportinbound.js
 * author : Edi Suwoto S.Komp
 * email  : edi.suwoto@gmail.com
 */
/*----------------------------------------------------------*/
$(document).ready(function(){
	
//---- Inisialisasi
    $("#tab-utama").tabs();
    $('#jclock').jclock({format: '%A, %d %B %Y - %H:%M:%S %P'});
    $('.infoproses1').hide();
    $('.infoproses2').hide();
    $('.infoproses3').hide();
    $('.infoproses4').hide();
    $('.infoproses5').hide();
    $('.infoproses6').hide();
    $('.infoproses6t').hide();
    $('.infoproses7').hide();
    $('.infoprosesa').hide();
    $('.infoproseso').hide();
    $('.infoprosesabd').hide();
    isi = ajak('report/reportinbound/isi_user');
    $("#user").html(isi);
    isi = ajak('report/reportinbound/isi_user1');
    $("#user1").html(isi);
    isi = ajak('report/reportinbound/isi_user');
    $("#useractivity").html(isi);
    isi = ajak('report/reportinbound/isi_user1');
    $("#useractivity1").html(isi);
    isi = ajak('report/reportinbound/isi_user');
    $("#user_a").html(isi);
    isi = ajak('report/reportinbound/isi_user');
    $("#user_a10").html(isi);
    isi = ajak('report/reportinbound/isi_user');
    $("#user_o").html(isi);
    $("#user").live('change',function(){
		ff = $(this).val();
		isi = ajak('report/reportinbound/isi_user1','id='+ ff);
	    $("#user1").html(isi);
	});
    $("#useractivity").live('change',function(){
		ff = $(this).val();
		isi = ajak('report/reportinbound/isi_user1','id='+ ff);
	    $("#useractivity1").html(isi);
	});
    function secondstotime(d)
    {
	    d = Number(d);
	    var h = Math.floor(d / 3600);
	    var m = Math.floor(d % 3600 / 60);
	    var s = Math.floor(d % 3600 % 60);
	    return ((h > 0 ? (h >= 10 ? h : '0' + h): '00') + ':' + (m > 0 ? (m >= 10 ? m : '0' + m): '00') + ':' + (s > 0 ? (s >= 10 ? s : '0' + s): '00')  );
    }
    function hmsToSecondsOnly(str) {
        var p = str.split(':'),
            s = 0, m = 1;
        while (p.length > 0) {
            s += m * parseInt(p.pop(), 10);
            m *= 60;
        }
        return s;
    }
    $('#showlap1').click(function(){
    	var isitgl1 =  $('#tgllap1').val();
    	var isitgl2 =  $('#tgllap2').val();
    	var user = $('#user').val();
    	var user1 = $('#user1').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Selected to date");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tgllap2').val().substr(6, 4) - $('#tgllap1').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllap2').val().substr(3, 2)*1) + 12) - ($('#tgllap1').val().substr(3, 2)*1)
            if(selisihx > 11) {
            alert("Range Periode max 12 month");
            return false;
            }
        }
        isitglperiode = ($('#tgllap1').val() == $('#tgllap2').val()) ? cbulan($('#tgllap1').val()) : cbulan($('#tgllap1').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllap2').val());
        $('#isitgl').html(isitglperiode);
    	loaddata1(isitgl1,isitgl2,user,user1);
    });
    function loaddata1(tgl1,tgl2,user,user1){
    	$('.infoproses1').show();
    	$("#tb_view1").html("");
    	$.post("report/reportinbound/get_dataview","tgl1="+ tgl1 +"&tgl2="+ tgl2 +"&user="+ user +"&user1="+ user1 ,
                function(json){
                    var isi ="",abdrate=0,coff=0,acd=0,abd=0,abdivr=0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	acd = json['alldata'][i].answered;
                    	abd = json['alldata'][i].abandon;
                    	abdivr = json['alldata'][i].abandonivr;
                    	coff = eval(acd) + eval(abd) + eval(abdivr) + eval(json['alldata'][i].outbound);
                    	
                    	abdrate = (json['alldata'][i].abandon / json['alldata'][i].callall * 100).toFixed(2);
                    	if (isNaN(abdrate)){abdrate = 0;}
                    	acd = json['alldata'][i].answered;
                    	acdtalktime = json['alldata'][i].talktime ? json['alldata'][i].talktime : 0;
                    	
                    	AvgHandlingTime = (eval(acdtalktime) + eval(hmsToSecondsOnly(json['alldata'][i].hold))) / acd;
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].calldate +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ coff +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].answered +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abandonivr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].abandon +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].outbound +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ abdrate +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ secondstotime(AvgHandlingTime) +"</td>" 
               	 			+ "</tr>";
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view1").html(isi);
                    $('.infoproses1').hide();
            }, "json");
        return false;
    }
    $('#pdownload1').click( function() {
    	var isitgl1 =  $('#tgllap1').val();
    	var isitgl2 =  $('#tgllap2').val();
    	var user = $('#user').val();
    	var user1 = $('#user1').val();
        location.href = 'report/reportinbound/get_dataviewexel/'+isitgl1+'/'+isitgl2+'/'+user+'/'+user1+'/export';
        return false;
    });
    $('#showlapabd').click(function(){
    	var isitgl1 =  $('#tgllapabd1').val();
    	var isitgl2 =  $('#tgllapabd2').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Selected to date");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tgllapabd2').val().substr(6, 4) - $('#tgllapabd1').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllapabd2').val().substr(3, 2)*1) + 12) - ($('#tgllapabd1').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
        isitglperiode = ($('#tgllapabd2').val() == $('#tgllapabd2').val()) ? cbulan($('#tgllapabd1').val()) : cbulan($('#tgllapabd1').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllapabd2').val());
        $('#isitglabd').html(isitglperiode);
    	loaddataabd(isitgl1,isitgl2);
    });
    $('#pdownloadabd').click( function() {
    	var isitgl1 =  $('#tgllapabd1').val();
    	var isitgl2 =  $('#tgllapabd2').val();
    	location.href = 'report/reportinbound/get_dataviewabdexel/'+isitgl1+'/'+isitgl2+'';
        return false;
    });
    function loaddataabd(tgl1,tgl2){
    	$('.infoprosesabd').show();
    	$("#tb_view2").html("");
    	$.post("report/reportinbound/get_dataviewabd","tgl1="+ tgl1 +"&tgl2="+ tgl2 ,
                function(json){
                    var isi ="",nr=1;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].alldate +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].src +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].name +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].cb +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].duration +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_viewabd").html(isi);
                    $('.infoprosesabd').hide();
            }, "json");
        return false;
    }
    $('#showlap2').click(function(){
    	var isitgl1 =  $('#tgllap21').val();
    	var isitgl2 =  $('#tgllap22').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Selected to date");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tgllap22').val().substr(6, 4) - $('#tgllap21').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllap22').val().substr(3, 2)*1) + 12) - ($('#tgllap21').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
        isitglperiode = ($('#tgllap22').val() == $('#tgllap22').val()) ? cbulan($('#tgllap21').val()) : cbulan($('#tgllap21').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllap22').val());
        $('#isitgl2').html(isitglperiode);
    	loaddata2(isitgl1,isitgl2);
    });
    function loaddata2(tgl1,tgl2){
    	$('.infoproses2').show();
    	$("#tb_view2").html("");
    	$.post("report/reportinbound/get_dataview1","tgl1="+ tgl1 +"&tgl2="+ tgl2 ,
                function(json){
                    var isi ="",nr=1;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].alldate +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].agent +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].phone +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].name +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].cb +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].duration +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].callfrom +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view2").html(isi);
                    $('.infoproses2').hide();
            }, "json");
        return false;
    }
    $('#pdownload2').click( function() {
    	var isitgl1 =  $('#tgllap21').val();
    	var isitgl2 =  $('#tgllap22').val();
    	location.href = 'report/reportinbound/get_dataviewexel1/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
    $('#showlap3').click(function(){
    	var isitgl1 =  $('#tgllap31').val();
    	var isitgl2 =  $('#tgllap32').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Selected to date");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tgllap32').val().substr(6, 4) - $('#tgllap31').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllap32').val().substr(3, 2)*1) + 12) - ($('#tgllap31').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
        isitglperiode = ($('#tgllap32').val() == $('#tgllap32').val()) ? cbulan($('#tgllap31').val()) : cbulan($('#tgllap31').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllap32').val());
        $('#isitgl3').html(isitglperiode);
    	loaddata3(isitgl1,isitgl2);
    });
    function loaddata3(tgl1,tgl2){
    	$('.infoproses3').show();
    	$("#tb_view3").html("");
    	$.post("report/reportinbound/get_dataview2","tgl1="+ tgl1 +"&tgl2="+ tgl2 ,
                function(json){
                    var isi ="",nr=1,pickup =0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].agent +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].src +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].name +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].cb +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].onqueue_datetime +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].pickup_datetime +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].hangup_datetime +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].pickup_time +"</td>" 
           	 				+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ secondstotime(json['alldata'][i].talk_time) +"</td>" 
           	 				+ "</tr>";
                    	nr++;
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view3").html(isi);
                    $('.infoproses3').hide();
            }, "json");
        return false;
    }
    $('#pdownload3').click( function() {
    	var isitgl1 =  $('#tgllap31').val();
    	var isitgl2 =  $('#tgllap32').val();
    	location.href = 'report/reportinbound/get_dataviewexel2/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
    $('#showlap4').click(function(){
    	var isitgl1 =  $('#tgllap41').val();
    	var isitgl2 =  $('#tgllap42').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Selected to date");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Start date < End date");
            return false;
        }
        if(($('#tgllap42').val().substr(6, 4) - $('#tgllap41').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllap42').val().substr(3, 2)*1) + 12) - ($('#tgllap41').val().substr(3, 2)*1)
            if(selisihx > 11) {
            	alert("Range Periode max 12 month");
            	return false;
            }
        }
        isitglperiode = ($('#tgllap42').val() == $('#tgllap42').val()) ? cbulan($('#tgllap41').val()) : cbulan($('#tgllap41').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllap42').val());
        $('#isitgl4').html(isitglperiode);
    	loaddata4(isitgl1,isitgl2);
    });
    function loaddata4(tgl1,tgl2){
    	$('.infoproses4').show();
    	$("#tb_view4").html("");
    	$.post("report/reportinbound/get_dataview3","tgl1="+ tgl1 +"&tgl2="+ tgl2 ,
                function(json){
                    var isi ="",nr=1,status="";
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	if(json['alldata'][i].status == 0){
                    		status = "open";
                    	}else{
                    		status = "close";
                    	}
                    	isi += "<tr>"
                    		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ nr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].ticket_code +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].ticket_phone +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].ticket_name +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].ticket_imei +"</td>" 
		               	 	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\">"+ json['alldata'][i].ticket_polis +"</td>" 
		               	 	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_category +"</td>" 
		               	 	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_subcategory +"</td>" 
		               	 	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_desc +"</td>" 
		               	 	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_ocategory +"</td>" 
		               		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_oasub +"</td>" 
		               		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_osub +"</td>" 
		               		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_osubcategory +"</td>" 
		               		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\">"+ json['alldata'][i].ticket_odesc +"</td>" 
		               		+ "</tr>";
                    	nr++;
                    }  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view4").html(isi);
                    $('.infoproses4').hide();
            }, "json");
        return false;
    }
    $('#pdownload4').click( function() {
    	var isitgl1 =  $('#tgllap41').val();
    	var isitgl2 =  $('#tgllap42').val();
    	location.href = 'report/reportinbound/get_dataviewexel3/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
    $('#showlap5').click(function(){
    	var isitgl1 =  $('#tglactivity').val();
    	var user2 = $('#useractivity').val();
    	var user3 = $('#useractivity1').val();
    	if(user3 == 0){
    		alert("Maaf anda belum memilih agent");
    		return false;
    	}
    	var jam = $('#jam1').val();
    	isitglperiode = cbulan($('#tglactivity').val());
        $('#isitglactivity').html(isitglperiode);
    	loaddata5(isitgl1,user2,user3,jam);
    });
    function loaddata5(tgl1,user1,user2,jam){
    	$('.infoproses5').show();
    	$("#tb_view9").html("");
    	$.post("report/reportinbound/get_dataviewactivity","tgl1="+ tgl1 +"&jam="+ jam +"&user="+ user1 +"&user1="+ user2 ,
                function(json){
                    var isi ="";
                    var totsholat = 0, totistirahat=0, totbriefing=0,tottoilet=0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].time +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].agent +"</td>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].login +"</td>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].logout +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].sholat +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].istirahat +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].briefing +"</td>"
            	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].toilet +"</td>"
                   	 		+ "</tr>";
                    	totsholat += eval(hmsToSecondsOnly(json['alldata'][i].sholat));
                    	totistirahat += eval(hmsToSecondsOnly(json['alldata'][i].istirahat));
                    	totbriefing += eval(hmsToSecondsOnly(json['alldata'][i].briefing));
                    	tottoilet += eval(hmsToSecondsOnly(json['alldata'][i].toilet));
                    } 
                    isi += "<tr>"
               	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap colspan='4'>TOTAL</td>" 
               	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(totsholat) +"</td>"
        	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(totistirahat) +"</td>"
        	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(totbriefing) +"</td>"
        	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(tottoilet) +"</td>"
               	 		+ "</tr>";
                    $("#tb_view5").html(isi);
                    $('.infoproses5').hide();
            }, "json");
    	
    	return false;
    }
    $('#pdownload5').click( function() {
    	var isitgl1 =  $('#tgllap51').val();
    	var isitgl2 =  $('#tgllap52').val();
    	location.href = 'report/reportinbound/get_dataviewexel4/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
    $('#showlap6').click(function(){
    	var isitgl1 =  $('#tgllap61').val();
    	var isitgl2 =  $('#tgllap62').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Anda belum memilih periode tanggal");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Tanggal Awal Harus lebih kecil dari Tanggal Akhir");
            return false;
        }
        if(($('#tgllap62').val().substr(6, 4) - $('#tgllap61').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllapPickup2').val().substr(3, 2)*1) + 12) - ($('#tgllap61').val().substr(3, 2)*1)
            if(selisihx > 11) {
            alert("Selisih Bulan Periode max 12 bulan");
            return false;
            }
        }
        isitglperiode = ($('#tgllap61').val() == $('#tgllap62').val()) ? cbulan($('#tgllap61').val()) : cbulan($('#tgllap61').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllap62').val());
        $('#isitgl6').html(isitglperiode);
        loaddata6(isitgl1,isitgl2);
    });
    function loaddata6(tgl1,tgl2){
    	$('.infoproses6').show();
    	$("#tb_view6").html("");
    	$.post("report/reportinbound/get_dataview5","tgl1="+ tgl1 +"&tgl2="+ tgl2 ,
                function(json){
                    var isi ="";
                    var coff=0, acd=0, abd=0,pacd=0,pabd=0,acdtalktime=0,ivrtosales=0,talktime0=0,talktime5=0,talktime10=0,talktime15=0,talktime20=0,talktime25=0,talktime30=0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	acd = json['alldata'][i].answered;
                    	abd = json['alldata'][i].abandon;
                    	
                    	abdivr = json['alldata'][i].abandonivr;
                    	coff = eval(acd) + eval(abd) + eval(abdivr);
                    	
                    	talktime0 = json['alldata'][i].talktime0;
                    	talktime5 = json['alldata'][i].talktime5;
                    	talktime10 = json['alldata'][i].talktime10;
                    	talktime15 = json['alldata'][i].talktime15;
                    	talktime20 = json['alldata'][i].talktime20;
                    	talktime25 = json['alldata'][i].talktime25;
                    	talktime30 = json['alldata'][i].talktime30;
                    	
                    	ptalktime0 = (talktime0 / acd * 100).toFixed(2);
                    	ptalktime5 = (talktime5 / acd * 100).toFixed(2);
                    	ptalktime10 = (talktime10 / acd * 100).toFixed(2);
                    	ptalktime15 = (talktime15 / acd * 100).toFixed(2);
                    	ptalktime20 = (talktime20 / acd * 100).toFixed(2);
                    	ptalktime25 = (talktime25 / acd * 100).toFixed(2);
                    	ptalktime30 = (talktime30 / acd * 100).toFixed(2);
                    	//sl = (eval(talktime0) + eval(talktime5) + eval(talktime10) + eval(talktime15) / coff * 100).toFixed(2);
                    	//scr = (eval(talktime0) + eval(talktime5) + eval(talktime10) + eval(talktime15) + eval(talktime20) + eval(talktime25) + eval(talktime30) / coff * 100).toFixed(2);
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ revDate(json['alldata'][i].calldate, "-") +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ coff +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ abd +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ abdivr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ acd +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime0 +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime5 +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime10 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime15 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime20 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime25 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime30 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime0 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime5 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime10 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime15 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime20 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime25 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime30 +"%</td>" 
		                   	//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ sl +"%</td>" 
		                   //	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ scr +"%</td>" 
		   	 				+ "</tr>";
                   	 	
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view6").html(isi);
                    $('.infoproses6').hide();
            }, "json");
    	
    	return false;
    }
    $('#pdownload6').click( function() {
    	var isitgl1 =  $('#tgllap61').val();
    	var isitgl2 =  $('#tgllap62').val();
    	location.href = 'report/reportinbound/get_dataviewexel5/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
    $('#showlap6t').click(function(){
    	var isitgl1 =  $('#tgllap61t').val();
    	var isitgl2 =  $('#tgllap62t').val();
    	if((isitgl1 == "")||(isitgl2 =="")){
    		alert("Anda belum memilih periode tanggal");
    		return false;
    	}
    	
    	if (Date.parse(isitgl1) > Date.parse(isitgl2)) {
            alert("Tanggal Awal Harus lebih kecil dari Tanggal Akhir");
            return false;
        }
        if(($('#tgllap62t').val().substr(6, 4) - $('#tgllap61t').val().substr(6, 4)) > 0) {
            selisihx = (($('#tgllap62t').val().substr(3, 2)*1) + 12) - ($('#tgllap61t').val().substr(3, 2)*1)
            if(selisihx > 11) {
            alert("Selisih Bulan Periode max 12 bulan");
            return false;
            }
        }
        isitglperiode = ($('#tgllap61t').val() == $('#tgllap62t').val()) ? cbulan($('#tgllap61t').val()) : cbulan($('#tgllap61t').val()) + "&nbsp;&nbsp;s/d&nbsp;&nbsp;" + cbulan($('#tgllap62t').val());
        $('#isitgl6').html(isitglperiode);
        loaddata6t(isitgl1,isitgl2);
    });
    function loaddata6t(tgl1,tgl2){
    	$('.infoproses6t').show();
    	$("#tb_view6t").html("");
    	$.post("report/reportinbound/get_dataviewtalk","tgl1="+ tgl1 +"&tgl2="+ tgl2 ,
                function(json){
                    var isi ="";
                    var coff=0, acd=0, abd=0,pacd=0,pabd=0,acdtalktime=0,ivrtosales=0,talktime0=0,talktime5=0,talktime10=0,talktime15=0,talktime20=0,talktime25=0,talktime30=0,talktime30up=0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	acd = json['alldata'][i].answered;
                    	abd = json['alldata'][i].abandon;
                    	
                    	abdivr = json['alldata'][i].abandonivr;
                    	coff = eval(acd) + eval(abd) + eval(abdivr);
                    	
                    	talktime10 = json['alldata'][i].talktime10;
                    	talktime20 = json['alldata'][i].talktime20;
                    	talktime30 = json['alldata'][i].talktime30;
                    	talktime30up = json['alldata'][i].talktime30up;
                    	
                    	ptalktime10 = (talktime10 / acd * 100).toFixed(2);
                    	ptalktime20 = (talktime20 / acd * 100).toFixed(2);
                    	ptalktime30 = (talktime30 / acd * 100).toFixed(2);
                    	ptalktime30up = (talktime30up / acd * 100).toFixed(2);
                    	isi += "<tr>"
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ revDate(json['alldata'][i].calldate, "-") +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ coff +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ abd +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ abdivr +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ acd +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime10 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime20 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime30 +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ talktime30up +"</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime10 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime20 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime30 +"%</td>" 
		                   	+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ ptalktime30up +"%</td>" 
			                   + "</tr>";
                   	 	
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_view6t").html(isi);
                    $('.infoproses6t').hide();
            }, "json");
    	
    	return false;
    }
    $('#pdownload6t').click( function() {
    	var isitgl1 =  $('#tgllap61t').val();
    	var isitgl2 =  $('#tgllap62t').val();
    	location.href = 'report/reportinbound/get_dataviewtalkexel/'+isitgl1+'/'+isitgl2+'/export';
        return false;
    });
    $('#showlapa').click(function(){
    	var isitgl1 =  $('#tgla').val();
    	var user = $('#user_a').val();
    	if((isitgl1 == "")){
    		alert("Anda belum memilih periode tanggal");
    		return false;
    	}
        isitglperiode = cbulan($('#tgla').val());
        $('#isitgla').html(isitglperiode);
    	loaddataa(isitgl1,user);
    });
    function loaddataa(tgl1,user){
    	$('.infoprosesa').show();
    	$("#tb_viewa").html("");
    	$.post("report/reportinbound/get_dataviewa","tgl1="+ tgl1 +"&user="+ user,
                function(json){
                    var isi ="";
                    var coff=0, acd=0, abd=0,pacd=0,pabd=0,acdtalktime=0;
                    for(i = 0; i < json['alldata'].length; i++) {
                    	//if((json['alldata'][i].id_group == 30){
                    		acd = json['alldata'][i].callallout;
                    		acdtalktime1 = json['alldata'][i].outcall ? json['alldata'][i].outcall : 0;
                        	acdtalktime = secondstotime(acdtalktime1);
                    	/*}else{
                    		acd = json['alldata'][i].answered;
                    		acdtalktime1 = json['alldata'][i].talktime ? json['alldata'][i].talktime : 0;
                        	acdtalktime = secondstotime(acdtalktime1);
                        }*/
                    	aux = json['alldata'][i].pause;
                    	staftime = json['alldata'][i].avaltime;
                    	
                    	avaltime = staftime - hmsToSecondsOnly(acdtalktime) - hmsToSecondsOnly(aux) - hmsToSecondsOnly(json['alldata'][i].hold);
                    	avaltimetoH = secondstotime(avaltime);
                    	AvgHandlingTime = (eval(acdtalktime1) + eval(hmsToSecondsOnly(json['alldata'][i].hold))) / acd;
                    	
                    	if((json['alldata'][i].id_group == 30)||(json['alldata'][i].id_group == 29)){
                    		isi += "<tr>"
                       	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].agent +"</td>";
                        }else{
                    		isi += "<tr>"
                       	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].agent +"</td>"; 
                       	 		//+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ acd +"</td>";
                        }
                    	
                    	isi += "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].callallout +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ acdtalktime +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].hold +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ aux +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ avaltimetoH +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(AvgHandlingTime) +"</td>" 
               	 			+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(staftime) +"</td>" 
                   	 		+ "</tr>";
                   	 	
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_viewa").html(isi);
                    $('.infoprosesa').hide();
            }, "json");
    	
            return false;
    }
    $('#pdownloada').click( function() {
    	var isitgl1 =  $('#tgla').val();
    	var user = $('#user_a').val();
    	location.href = 'report/reportinbound/get_dataviewaexel/'+isitgl1+'/'+user+'';
        return false;
    });
    $('#showlapo').click(function(){
    	var isitgl1 =  $('#tglo').val();
    	var user = $('#user_a10').val();
    	if((isitgl1 == "")){
    		alert("Anda belum memilih periode tanggal");
    		return false;
    	}
        isitglperiode = cbulan($('#tglo').val());
        $('#isitglo').html(isitglperiode);
    	loaddatao(isitgl1,user);
    });
    function loaddatao(tgl1,user){
    	$('.infoproseso').show();
    	$("#tb_viewo").html("");
    	$.post("report/reportinbound/get_dataviewo","tgl1="+ tgl1 +"&user="+ user,
                function(json){
                    var isi ="";
                    for(i = 0; i < json['alldata'].length; i++) {
                    	isi += "<tr>"
                    		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].uniqueid +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"left\" nowrap>"+ json['alldata'][i].calldate +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].agent +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].dst +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ secondstotime(json['alldata'][i].duration) +"</td>" 
                   	 		+ "<td style=\"border-top:1px solid #000;border-right:0px solid #000\" align=\"center\" nowrap>"+ json['alldata'][i].disposition +"</td>" 
                   	 		+ "</tr>";
                   	 	
                   	}  
                    isi += "<tr>"
                   	 	+ "<td></td>" 
                   	 	+ "</tr>";
                    
                    $("#tb_viewo").html(isi);
                    $('.infoproseso').hide();
            }, "json");
    	
            return false;
    }
    $('#pdownloado').click( function() {
    	var isitgl1 =  $('#tglo').val();
    	var user = $('#user_o').val();
    	location.href = 'report/reportinbound/get_dataviewoexel/'+isitgl1+'/'+user+'';
        return false;
    });
});